/**
 * @file
 * Dieses Modul dient der Kapselung von globalen Flags, die Programmstati
 * anzeigen.
 *
 * @author Ellen Schwartau (Minf9888) und Malte Jörgens (Minf9640)
 */

/* ---- Eigene Header einbinden ---- */
#include "flag.hpp"
#include "debugGL.hpp"
#include <commonTypes.hpp>

/* ---- Konstanten ---- */
#ifdef DEBUG
/** Gibt an, ob Debug-Ausgaben dieses Moduls angezeigt werden sollen */
#define FLAG_PRINT GL_TRUE
#endif

/* ---- Typen ---- */
/** Flag-Array, das für jede Flag beinhaltet, ob sie gesetzt ist */
typedef GLboolean CGFlags[flag_count];

/* ---- globale Variablen ---- */
/** Flags */
CGFlags g_flags;

/** Flag-Callbacks */
FlagCallback g_callbacks[flag_count];

/* ---- Makros ---- */
#ifdef DEBUG
/** Makro zur Ausgabe von Debug-Informationen */
#define FLAG_INFO(ARGS) if (FLAG_PRINT) {INFO(ARGS);}
#else
#define FLAG_INFO(ARGS)
#endif

/* ---- Funktionen ---- */

/**
 * Ruft je nach gesetzter Flag eine Funktion auf, die auf diese Änderung
 * reagiert.
 * @param flag      Flag, die verändert wurde
 */
static void handleFlag(Flag flag) {
    FLAG_INFO(("Flag %u: %u\n", flag, g_flags[flag]));
    if (FlagCallback callback = g_callbacks[flag]) {
        callback(g_flags[flag]);
    }
    glutPostRedisplay();
}

/**
 * Flag setzen.
 * @param flag  Flag, die gesetzt werden soll.
 */
void setFlag(Flag flag) {
    g_flags[flag] = GL_TRUE;
    handleFlag(flag);
}

/**
 * Flag zurücksetzen.
 * @param flag  Flag, die zurückgesetzt werden soll.
 */
void resetFlag(Flag flag) {
    g_flags[flag] = GL_FALSE;
    handleFlag(flag);
}

/**
 * Flag umschalten.
 * @param flag  Flag, die umgeschaltet werden soll.
 * @return      Neuer Status der Flag
 */
GLboolean toggleFlag(Flag flag) {
    g_flags[flag] = !g_flags[flag];
    handleFlag(flag);
    return g_flags[flag];
}

/**
 * Flag auslesen
 * @param flag  Flag, die ausgelesen werden soll.
 */
GLboolean getFlag(Flag flag) {
    return g_flags[flag];
}

/**
 * Setze eine Callback-Funktion für eine bestimmte Flag, der bei Änderung jener Flag aufgerufen wird
 * @param flag              Flag, für die die Callback-Funktion registriert wird
 * @param callbackFunction  Aufzurufende Callback-Funktion
 */
void setCallback(Flag flag, FlagCallback callbackFunction) {
    g_callbacks[flag] = callbackFunction;
}

/**
 * Initialisiert das Flag-Modul.
 */
void initFlags(void) {
    GLuint i;
    for (i = 0; i < flag_count; i++) {
        g_flags[i] = GL_FALSE;
        g_callbacks[i] = NULL;
    }
    g_flags[flag_render_direct_illu] = GL_TRUE;
    g_flags[flag_render_photons] = GL_TRUE;
    g_flags[flag_show_indirect_photons] = GL_TRUE;
    g_flags[flag_show_caustic_photons] = GL_TRUE;

#ifdef DEBUG
    g_flags[flag_coordinateSystem] = GL_TRUE;
#endif
}


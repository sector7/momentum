/**
 * @file
 * Debug-Modul fuer OpenGL-Programme.
 * Genutzt werden sollten die Makros #CG_ERROR, #INFO, #GLSTATE und #GLGETERROR.
 *
 * @author copyright (C) Fachhochschule Wedel 1999-2011. All rights reserved.
 */

/* ---- Eigene Header einbinden ---- */
#include "debugGL.hpp"
#include <sstream>
#include <iostream>
#include <stdio.h>

/* ---- Funktionen ---- */

/**
 * Hilfsfunktion: nicht direkt verwenden.
 * Ausgabe wie mit fprintf auf stderr.
 * @return Rueckgabewert der Ausgabefunktion (vfprintf).
 */
int dbgPrint(const char *format, ...) {
    va_list ap;
    int result;

    va_start(ap, format);
    result = vfprintf(stderr, format, ap);
    va_end(ap);
    return result;
}

/**
 * Hilfsfunktion: nicht direkt verwenden.
 * Ausgabe eines GL-Fehlercode als String auf stderr.
 * @param state GL-Fehlercode (In).
 * @return Rueckgabewert der Ausgabefunktion (fprintf).
 */
int dbgPrintGLState(GLenum state) {
    if (state == GL_NO_ERROR) {
        return fprintf(stderr, "OK: %s\n", gluErrorString(state));
    } else {
        return fprintf(stderr, "NOT OK: %s\n", gluErrorString(state));
    }
}

/**
 * Hilfsfunktion: nicht direkt verwenden.
 * GL-Fehlercode ermitteln und als String auf stderr ausgeben.
 * @return GL-Fehlercode (von glGetError()).
 */
int dbgGetGLError(void) {
    GLenum errorCode = glGetError();

    dbgPrintGLState(errorCode);

    return errorCode;
}

/**
 * Hilfsfunktion: nicht direkt verwenden.
 * GL-Fehlercode ermitteln und als String auf stderr ausgeben.
 * @return Rueckgabewert der Ausgabe-Funktion.
 */
int dbgGL(void) {
    /* GLStatuswert ermitteln */
    GLenum errorCode = glGetError();
    /* In String umwandeln und zurueckgeben */
    return dbgPrintGLState(errorCode);
}

/**
 * Gibt eine Matrix auf der Console aus.
 * @param matrix
 * @return Rueckgabewert der Ausgabe-Funktion.
 */
std::string prettyPrintMatrix(GLfloat matrix[16]) {
    std::ostringstream stream;
    for (int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            stream << matrix[i * 4 + j];
            if (j == 3) {
                stream << "\n";
            }
        }
    }
    return stream.str();
}

/**
 * Gibt einen Fortschrittsbalken auf der Konsole aus.
 * @param x aktuelle Runde
 * @param n maximale Runde
 * @param r Auflösung
 * @param w Breite
 */
void printProgressbar(int x, int n, int r, int w) {
    // Only update r times.
    if (x % (n / r + 1) != 0)
        return;

    // Calculuate the ratio of complete-to-incomplete.
    float ratio = x / (float) n;
    int c = ratio * w;

    // Show the percentage complete.
    printf("%3d%% [", (int) (ratio * 100));

    // Show the load bar.
    for (int x = 0; x < c; x++)
        printf("=");

    for (int x = c; x < w; x++)
        printf(" ");

    printf("]\n");
}

/**
 * Liefert eine lesbare Darstellung des übergebenen Vektors
 * @param v Darzustellender Vektor
 * @return lesbare Darstellung
 */
std::string prettyPrintVector(momentum::Vector3f v) {
    std::ostringstream stream;
    stream << "(" << v[0] << ", " << v[1] << ", " << v[2] << ")";
    return stream.str();
}

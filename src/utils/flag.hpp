#ifndef __FLAG_HPP__
#define __FLAG_HPP__
/**
 * @file
 * Dieses Modul dient der Kapselung von globalen Flags, die Programmstati
 * anzeigen.
 * Mögliche Operationen sind das auslesen und setzen von Flags.
 *
 * @author Ellen Schwartau (Minf9888) und Malte Jörgens (Minf9640)
 */

/* ---- System-Header einbinden ---- */
#include <GL/glut.h>

/* ----- Eigene Header ---- */
#include <commonTypes.hpp>

using namespace momentum;

/* ---- Typen ---- */
/** Aufzählung der Flags */
typedef enum e_flag {
    flag_wireframe,
    flag_normals,
    flag_coordinateSystem,
    flag_fullscreen,
    flag_usage,
    flag_light,
    flag_textures,
    flag_raytracer,
    flag_show_photons,
    flag_show_direct_photons,
    flag_show_indirect_photons,
    flag_show_caustic_photons,
    flag_render_photons,
    flag_render_direct_illu,

    flag_count // Diese immer als letztes belassen und nicht verwenden!
} Flag;

/**
 * Flag setzen.
 * @param flag  Flag, die gesetzt werden soll.
 */
void setFlag(Flag flag);

/**
 * Flag zurücksetzen.
 * @param flag  Flag, die zurückgesetzt werden soll.
 */
void resetFlag(Flag flag);

/**
 * Flag umschalten.
 * @param flag  Flag, die umgeschaltet werden soll.
 * @return      Neuer Status der Flag
 */
GLboolean toggleFlag(Flag flag);

/**
 * Flag auslesen
 * @param flag  Flag, die ausgelesen werden soll.
 */
GLboolean getFlag(Flag flag);

/**
 * Setze eine Callback-Funktion für eine bestimmte Flag, der bei Änderung jener Flag aufgerufen wird
 * @param flag              Flag, für die die Callback-Funktion registriert wird
 * @param callbackFunction  Aufzurufende Callback-Funktion
 */
void setCallback(Flag flag, FlagCallback callbackFunction);

/**
 * Initialisiert das Flag-Modul.
 */
void initFlags(void);

#endif

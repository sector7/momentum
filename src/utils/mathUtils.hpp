#ifndef __MATH_UTILS_HPP__
#define __MATH_UTILS_HPP__

/* ---- System-Header einbinden ---- */
#include <math.h>
#include <time.h>
#include <commonTypes.hpp>

namespace momentum {

/* ---- Eigene Header einbinden ---- */
#include "commonConstants.hpp"

/** Winkelumrechnung von Grad nach Radiant */
#define DEG2RAD(x) ((x)/180.0*M_PI)
/** Winkelumrechnung von Radiant nach Grad */
#define RAD2DEG(x) ((x)/M_PI*180.0)

/** Runden von Fließkommazahlen */
#define ROUND(x) ((GLint)((x)+0.5f))

/** Berechnung einer zufälligen Fließkommazahl
 *  zwischen 0 und 1 */
#define RANDOM_FLOAT() ((GLfloat)rand()/RAND_MAX)

/** Berechnung einer zufälligen Fließkommazahl zwischen min und max */
#define RANDOM_FLOAT_BETWEEN(min,max) RANDOM_FLOAT()*((max)-(min))+(min)

/** Maximum- und Minimum-Makros */
#define MAX(x,y) ((x) > (y)) ? (x) : (y)
#define MIN(x,y) ((x) < (y)) ? (x) : (y)
/** Clipping eines Wertes in einen Zielbereich */
#define CLIP(min,max,v) MIN((max), MAX((min), (v)))

/** Gerade/ungerade Zahlen bestimmen */
#define IS_EVEN(x) !IS_ODD(x)
#define IS_ODD(x) ((x) & 1)

/** Winkelberechnung */
#define CALC_ANGLE_BETWEEN(x, y) acosf(gte::Dot(x, y))

/**
 * Initialisiert die Zufallszahlen.
 */
void initRand();

/**
 * Berechnet den Richtungsvektor ausgehend von zwei Kugelkoordinaten theta und phi.
 * @param theta vertikaler Winkel
 * @param phi horizontaler Winkel
 * @return
 */
Vector3f calcDirection(GLfloat theta, GLfloat phi);

/**
 * Berechnet die Gaussfunktion für ein x mit bestimmtem Erwartungswert und Varianz.
 * Der Default ist die Normalverteilung.
 * @param x X-Wert
 * @param expectedValue Erwartungswert
 * @param variance Standardabweichung
 * @return y
 */
GLfloat gaussFunction(GLfloat x, GLfloat expectedValue = 0.0, GLfloat variance = 1.0);

/**
 * Berechnet die quadratische Abschwächung für ein x innerhalb eines Radius.
 * @param x X-Wert
 * @param width Bereich der Abschwächung
 * @return y
 */
GLfloat quadraticWeight(const GLfloat x, const GLfloat width);

/**
 * Berechnet die lineare Abschwächung für ein x innerhalb eines Radius.
 * @param x X-Wert
 * @param width Bereich der Abschwächung
 * @return y
 */
GLfloat linearWeight(const GLfloat x, const GLfloat width);

}
#endif

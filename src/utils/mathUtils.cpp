#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <cmath>

#include <mathUtils.hpp>

namespace momentum {

/**
 * Initialisiert die Zufallszahlen.
 */
void initRand() {
    time_t t;
    time(&t);
    srand((unsigned int) t);
}

/**
 * Berechnet den Richtungsvektor ausgehend von zwei Kugelkoordinaten theta und phi.
 * @param theta vertikaler Winkel
 * @param phi horizontaler Winkel
 * @return
 */
Vector3f calcDirection(GLfloat theta, GLfloat phi) {
    Vector3f randomThing = Vector3f( {
            sinf(DEG2RAD(phi)) * cosf(DEG2RAD(theta)),
            sinf(DEG2RAD(theta)),
            cosf(DEG2RAD(theta)) * cosf(DEG2RAD(phi))
    });
    gte::Normalize(randomThing);
    return randomThing;
}

/**
 * Berechnet die Gaussfunktion für ein x mit bestimmtem Erwartungswert und Varianz.
 * Der Default ist die Normalverteilung.
 * @param x X-Wert
 * @param expectedValue Erwartungswert
 * @param variance Standardabweichung
 * @return y
 */
GLfloat gaussFunction(const GLfloat x, const GLfloat expectedValue, const GLfloat variance) {
    //GLfloat sigma = sqrtf(variance);
    GLfloat sigma = variance;
    return (1 / (sigma * sqrtf(2 * M_PI))) * expf(-0.5 * powf((x - expectedValue) / sigma, 2.0));
}

/**
 * Berechnet die quadratische Abschwächung für ein x innerhalb eines Radius.
 * @param x X-Wert
 * @param width Bereich der Abschwächung
 * @return y
 */
GLfloat quadraticWeight(const GLfloat x, const GLfloat width) {
    return -x * x / (width * width) + 1.0f;
}

/**
 * Berechnet die lineare Abschwächung für ein x innerhalb eines Radius.
 * @param x X-Wert
 * @param width Bereich der Abschwächung
 * @return y
 */
GLfloat linearWeight(const GLfloat x, const GLfloat width) {
    return -x / width + 1.0f;
}

}

/**
 * @file
 * Hauptprogramm. Initialisierung und Starten der Ergeignisverarbeitung.
 *
 * @author Ellen Schwartau (Minf9888) und Malte Jörgens (Minf9640)
 */

/* ---- System Header einbinden ---- */
#include <stdio.h>
#include <GL/glut.h>
#include <mathUtils.hpp>

/* ---- Eigene Header einbinden ---- */
#include <io.hpp>
#include <commonConstants.hpp>
#include <PixelRenderer.hpp>
#include <Config.hpp>

using namespace momentum;

/**
 * Hauptprogramm.
 * Initialisierung und Starten der Ereignisbehandlung.
 * @param argc Anzahl der Kommandozeilenparameter (In).
 * @param argv Kommandozeilenparameter (In).
 * @return Rueckgabewert im Fehlerfall ungleich Null.
 */
int main(int argc, char **argv) {
    Config config = Config(argc, argv);
    /* Initialisierung des I/O-Sytems
     (inkl. Erzeugung des Fensters und Starten der Ereignisbehandlung). */
    if (!initAndStartIO("Momentum", config.WINDOW_WIDTH, config.WINDOW_HEIGHT, config)) {
        fprintf(stderr, "Initialisierung fehlgeschlagen!\n");
        return 1;
    } else {
        return 0;
    }
}

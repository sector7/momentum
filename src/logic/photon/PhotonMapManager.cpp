/*
 * PhotonMapManager.cpp
 *
 *  Created on: 11.08.2015
 *      Author: malte
 */

#include "PhotonMapManager.hpp"

#include <sceneTypes.hpp>
#include <flag.hpp>

/**
 * Klasse zur Handhabung der verschiedenen Photon Maps für direkte, indirekte und spekular reflektierte Photonen.
 */
namespace momentum {

/**
 * Konsturktorfunktion - initialisiert die verschiedenen Photon Maps.
 */
PhotonMapManager::PhotonMapManager() :
        photonMap( { PhotonMap(MAX_LEAF_SIZE, MAX_LEVEL, 1.0f), // Direkt
        PhotonMap(MAX_LEAF_SIZE, MAX_LEVEL, 1.5f),      // Indirekt
        PhotonMap(MAX_LEAF_SIZE, MAX_LEVEL, 0.3f) }) {  // Spekular
}

/**
 * Destruktorfunktion.
 */
PhotonMapManager::~PhotonMapManager() {
}

/**
 * Berechnet die Farbe an einem bestimmten Punkt.
 * @param pos Punkt
 * @param object geschnittenes Objekt
 * @return Farbe
 */
Color3f PhotonMapManager::calcColor(const Vector3f& pos, SceneObject* object) const {
    Color3f color;
    color.MakeZero();
    if (getFlag (flag_show_indirect_photons))
        color += photonMap[INDIRECT_ILLU].calcColor(pos, object);
    if (getFlag (flag_show_caustic_photons))
        color += photonMap[CAUSTIC_ILLU].calcColor(pos, object);
    return color;
}

/**
 * Liefert eine bestimmte Photon Map zurück.
 * @param which Typ der gewünschten Photon Map
 * @return Photon Map
 */
const PhotonMap& PhotonMapManager::getPhotonMap(const PhotonMapType which) const {
    return photonMap[which];
}

/**
 * Fügt einer bestimmten Photon Map ein Photon hinzu.
 * @param which Typ der betreffenden Photon Map
 * @param p Photon
 */
void PhotonMapManager::put(const PhotonMapType which, Photon p) {
    this->photonMap[which].put(p);
}

/**
 * Verschießt die Photonen der Szene und erstellt die verschiedenen Photon Maps.
 */
void PhotonMapManager::createMaps() {
    for (int type = INDIRECT_ILLU; type <= CAUSTIC_ILLU; type++) {
        photonMap[type].createKDTree();
    }
    // Kein KD-Tree für die direkten Photonen, aber Anzeige im OpenGL Modus ermöglichen
    photonMap[DIRECT_ILLU].createVertexBufferObject();
}

} /* namespace momentum */


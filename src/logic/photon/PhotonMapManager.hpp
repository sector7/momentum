/*
 * PhotonMapManager.hpp
 *
 *  Created on: 11.08.2015
 *      Author: malte
 */

#ifndef __PHOTONMAPMANAGER_HPP__
#define __PHOTONMAPMANAGER_HPP__

#include <PhotonMap.hpp>
#include <SceneObject.hpp>
#include <Light.hpp>
#include <photonAndRayTypes.hpp>

#include <list>

namespace momentum {

/**
 * Klasse zur Handhabung der verschiedenen Photon Maps für direkte, indirekte und spekular reflektierte Photonen.
 */
class PhotonMapManager {
public:
    /**
     * Konsturktorfunktion - initialisiert die verschiedenen Photon Maps.
     */
    PhotonMapManager();

    /**
     * Destruktorfunktion.
     */
    virtual ~PhotonMapManager();

    /**
     * Berechnet die Farbe an einem bestimmten Punkt.
     * @param pos Punkt
     * @param object geschnittenes Objekt
     * @return Farbe
     */
    Color3f calcColor(const Vector3f& pos, SceneObject* object) const;

    /**
     * Liefert eine bestimmte Photon Map zurück.
     * @param which Typ der gewünschten Photon Map
     * @return Photon Map
     */
    const PhotonMap& getPhotonMap(const PhotonMapType which) const;

    /**
     * Fügt einer bestimmten Photon Map ein Photon hinzu.
     * @param which Typ der betreffenden Photon Map
     * @param p Photon
     */
    void put(const PhotonMapType which, Photon p);

    /**
     * Verschießt die Photonen der Szene und erstellt die verschiedenen Photon Maps.
     */
    void createMaps();

private:
    /**
     * Photon Maps für direkte, indirekte und spekular reflektierte Photonen.
     */
    PhotonMap photonMap[CAUSTIC_ILLU + 1];
};

} /* namespace momentum */

#endif /* __PHOTONMAPMANAGER_HPP__ */

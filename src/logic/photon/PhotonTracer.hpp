#ifndef PHOTONTRACER_HPP_
#define PHOTONTRACER_HPP_

#include <commonTypes.hpp>
#include <Scene.hpp>
#include <PhotonMap.hpp>
#include <PhotonMapManager.hpp>
#include <Tracer.hpp>

namespace momentum {

/**
 * Klasse zur Berechnung der Photonen ausgehend von den vorhandenen Lichtquellen und Szenenobjekten.
 */
class PhotonTracer: Tracer {

public:
    /**
     * Konstruktorfunktion
     * @param objects Szenenobjeke
     * @param lights Lichtquellen der Szene
     * @param photonCount Anzahl zu verschießender Photonen
     */
    PhotonTracer(const SceneObjectList& objects, const LightList& lights, GLuint photonCount);
    /**
     * Destruktorfunktion
     */
    virtual ~PhotonTracer();

    /**
     * Erstellt die Photon Map der Szene.
     * @return Photon Map
     */
    PhotonMapManager* createPhotonMaps();

private:
    /** Szenenobjekte */
    const SceneObjectList& objects;
    /** Lichtquellen */
    const LightList& lights;
    /** Anzahl der zu schießenden Photonen pro Lichtquelle */
    GLuint photonCount;

    /**
     * Verschießt Photonen ausgehend von den Lichtquellen.
     * @return Vektor aus Photonen
     */
    PhotonMapManager* trace();

    /**
     * Verschießt die Photonen einer Lichtquelle.
     * @param light Lichtquelle
     * @param photons bisher gespeicherte Photonen
     */
    void emitPhotonsFromLightSrc(Light light, PhotonMapManager& photons);

    /**
     * Schießt ein Photon durch die Szene und speichert die Ergebnisse in der Photon-Liste.
     * @param photon Photon
     * @param photons Liste
     * @param type Letzte Propagationsart, initial direkteBeleuchtung
     */
    void tracePhoton(Photon& photon, PhotonMapManager& photons);

    /**
     * Speichert das zuletzt reflektierte Photon ausgehend von seiner Propagier-Historie in die entsprechende Map.
     * @param intersectionList Propagier-Historie
     * @param photons Photon Map Manager
     * @param photonColor Farbe des Photons, modifiziert durch letzten Aufprall
     */
    void savePhotons(IntersectionList intersectionList, PhotonMapManager& photons,
            Color3f photonColor);

};

}

#endif /* PHOTONTRACER_HPP_ */

#ifndef PHOTON_HPP_
#define PHOTON_HPP_

#include <commonTypes.hpp>

namespace momentum {

/**
 * Klasse zur Repräsentation eines Photons zur Umsetzung des Photon Mappings.
 */
class Photon {
public:
    /**
     * Konstruktorfunktion
     * @param ray Strahl
     * @param color Farbe
     */
    Photon(Ray3f ray, Color3f color);

    /**
     * Destruktorfunktion
     */
    virtual ~Photon();

    /**
     * Liefert die Position des Photons.
     * Dient als Schnittstelle für die GTE NearestNeighborQuery.
     */
    Vector3f GetPosition() const;

    /**
     * Versetzt das Photon an eine neue Position
     * @param newPos neue Position
     */
    void setPosition(Vector3f newPos);

    /**
     * Liefert den Strahl des Photons.
     * @return Strahl
     */
    const Ray3f& getRay() const;

    /**
     * Liefert die Farbe des Photons.
     * @return Farbe
     */
    Color3f getColor();

    /**
     * Setzt den Strahl des Photons
     * @param ray Strahl
     */
    void setRay(Ray3f ray);

private:
    /** Strahl: Position und Direction */
    Ray3f ray;

    /** Farbe / Intensität des Photons */
    Color3f color;
};

}

#endif /* PHOTON_HPP_ */

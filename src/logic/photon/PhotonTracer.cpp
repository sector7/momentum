#include "PhotonTracer.hpp"
#include <Photon.hpp>
#include <mathUtils.hpp>
#include <debugGL.hpp>
#include <math.h>
#include <stdio.h>
#include <iostream>

namespace momentum {

/**
 * Konstruktorfunktion
 * @param objects Szenenobjeke
 * @param lights Lichtquellen der Szene
 * @param photonCount Anzahl zu verschießender Photonen
 */
PhotonTracer::PhotonTracer(const SceneObjectList& objects, const LightList& lights,
        GLuint photonCount) :
        Tracer(), objects(objects), lights(lights), photonCount(
                photonCount) {
    initRand();
}

/**
 * Destruktorfunktion
 */
PhotonTracer::~PhotonTracer() {
}

/**
 * Verschießt Photonen ausgehend von den Lichtquellen.
 * @return Vektor aus Photonen
 */
PhotonMapManager* PhotonTracer::trace() {
    PhotonMapManager* photons = new PhotonMapManager();
    for (Light* light : this->lights) {
        INFO(("Emitting Photons from Lightsource... \n"));
        emitPhotonsFromLightSrc(*light, *photons);
        INFO((" done.\n"));
    }
    photons->createMaps();
    return photons;
}

/**
 * Verschießt die Photonen einer Lichtquelle.
 * @param light Lichtquelle
 * @param photons bisher gespeicherte Photonen
 */
void PhotonTracer::emitPhotonsFromLightSrc(Light light, PhotonMapManager& photons) {
    // Intensität aufteilen
    Color3f photonColor = (light.getWatt() / (float) this->photonCount) * light.getColor();
    for (GLuint i = 0; i < this->photonCount; i++) {

        Photon photon = Photon(calcRandomRay(light.getPosition()), photonColor);
        tracePhoton(photon, photons);
        printProgressbar(i, this->photonCount, 10, 50);
    }
}

/**
 * Schießt ein Photon durch die Szene und speichert die Ergebnisse in der Photon-Liste.
 * @param photon Photon
 * @param photons Liste
 * @param type Letzte Propagationsart, initial direkteBeleuchtung
 */
void PhotonTracer::tracePhoton(Photon& photon, PhotonMapManager& photons) {
    IntersectionList intersectionList;
    IntersectionResult intersection;
    GLfloat refractionIndex = RI_AIR;
    Color3f photonColor = photon.getColor();

    while (!(intersectionList = traceRay(
            photon.getRay(),
            refractionIndex,
            photonColor,
            this->objects,
            !intersectionList.empty() ? intersectionList.back().object : nullptr,
            intersectionList)
            ).empty()
            && intersectionList.back().propagationType != PROPAG_ABSORPTION) {

        intersection = intersectionList.back();
        // Farbe der Diffusen Reflexion verrechnen
        if (intersection.propagationType == PROPAG_DIFFUSE) {
            photonColor *= intersection.object->getMaterial().diffuse;
        }
        // Speichern
        savePhotons(intersectionList, photons, photonColor);
        // Und neuen Strahl los schießen
        photon.setRay(
                calcDiffuseRay(intersection.ray.origin,
                        intersection.object->getNormalAt(intersection.ray.origin)));
        refractionIndex = intersection.object->getMaterial().refractionIndex;
    }
}

/**
 * Speichert das zuletzt reflektierte Photon ausgehend von seiner Propagier-Historie in die entsprechende Map.
 * @param intersectionList Propagier-Historie
 * @param photons Photon Map Manager
 * @param photonColor Farbe des Photons, modifiziert durch letzten Aufprall
 */
void PhotonTracer::savePhotons(IntersectionList intersectionList, PhotonMapManager& photons,
        Color3f photonColor) {
    if (intersectionList.empty()) {
        return;
    } else if (intersectionList.size() == 1) {
        // Photon kommt direkt von der Lichtquelle
        photons.put(DIRECT_ILLU, Photon(Ray3f(intersectionList.back().ray), photonColor));
    } else {
        // Photon ausgehend von seiner vorherigen Reflexion in Photon Map einordnen
        // Wir müssen uns an dieser Stelle nur das letze und vorletzte Photon aus der Liste ansehen
        // Die komplette Liste ist nur zu Visualisierungszwecken
        switch (intersectionList[intersectionList.size() - 2].propagationType) {
        case PROPAG_DIFFUSE:
            // Photon wurde vorher Diffus reflektiert
            photons.put(INDIRECT_ILLU, Photon(Ray3f(intersectionList.back().ray), photonColor));
            break;
        case PROPAG_SPECULAR:
            case PROPAG_TRANSMISSION:
            // Photon wurde spekular reflektiert / transmittiert -> Kaustik
            photons.put(CAUSTIC_ILLU, Photon(Ray3f(intersectionList.back().ray), photonColor));
            break;
        default:
            return;
        }
    }
}

/**
 * Erstellt die Photon Map der Szene.
 * @return Photon Map
 */
PhotonMapManager* PhotonTracer::createPhotonMaps(){
    return this->trace();
}

}

#include "Photon.hpp"
#include <sceneTypes.hpp>

namespace momentum {

/**
 * Konstruktorfunktion
 * @param ray Strahl
 * @param color Farbe
 * @param intensity Licht-Intensität
 * @param refractionIndex Brechungsindex des Materials durch das das Photon fliegt
 */
Photon::Photon(Ray3f ray, Color3f color) :
        ray(ray), color(color) {
}

/**
 * Destruktorfunktion
 */
Photon::~Photon() {
}

/**
 * Liefert die Position des Photons.
 * Dient als Schnittstelle für die GTE NearestNeighborQuery.
 * @return Position
 */
Vector3f Photon::GetPosition() const {
    return this->ray.origin;
}

/**
 * Versetzt das Photon an eine neue Position
 * @param newPos neue Position
 */
void Photon::setPosition(Vector3f newPos) {
    this->ray.origin = newPos;
}

/**
 * Setzt den Strahl des Photons.
 * @param ray Strahl
 */
void Photon::setRay(Ray3f ray) {
    this->ray = ray;
}

/**
 * Liefert den Strahl des Photons.
 * @return Strahl
 */
const Ray3f& Photon::getRay() const {
    return this->ray;
}

/**
 * Liefert die Farbe des Photons.
 * @return Farbe
 */
Color3f Photon::getColor() {
    return this->color;
}

}

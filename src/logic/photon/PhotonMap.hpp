#ifndef PHOTONMAP_HPP_
#define PHOTONMAP_HPP_

#include <photonAndRayTypes.hpp>
#include <SceneObject.hpp>

#include <GteNearestNeighborQuery.h>

namespace momentum {

/**
 * Klasse zur Repräsentation einer PhotonMap auf Basis eines KD-Trees.
 */
class PhotonMap {
public:
    /**
     * Konstruktorfunktion
     * @param maxLeafSize maximale Kinderanzahl
     * @param maxLevel maximale Tiefe des Baums
     * @param radius Suchradius
     */
    PhotonMap(int maxLeafSize, int maxLevel, GLfloat radius);

    /**
     * Konstruktorfunktion
     * @param photons Liste der Photonen
     * @param maxLeafSize maximale Kinderanzahl
     * @param maxLevel maximale Tiefe des Baums
     * @param radius Suchradius
     */
    PhotonMap(const Photons& photons, int maxLeafSize, int maxLevel, GLfloat radius);

    /**
     * Destruktorfunktion
     */
    virtual ~PhotonMap();

    /** Liefert die Liste der Photonen, die zur Initialisierung genutzt wurden */
    const Photons& getPhotons() const;

    /** Liefert den Zeiger auf den ersten Datenwertes des Vertex Buffer Arrays*/
    GLfloat* getPhotonData() const;

    /** Liefert den Zeiger auf den ersten Farbwertes des Vertex Buffer Arrays*/
    GLfloat* getPhotonColors() const;

    /** Liefert den Zeiger auf den ersten Index des Indexarrays */
    GLuint* getPhotonIndices() const;

    /** Liefert die Gesamtanzahl der vorhandenen Photonen */
    unsigned long getTotalPhotonCount() const;

    /**
     * Setzt die Liste an Photonen.
     * @param photons Photonen
     */
    void setPhotons(Photons photons);

    /**
     * Erzeugt den KD-Tree auf Grundlage der vorhandenen Liste an Photonen.
     */
    void createKDTree();

    /**
     * Fügt der Liste an Photonen ein Photon hinzu.
     * @param p Photon
     */
    void put(Photon p);

    /**
     * Ermittelt die Farbe eines Punktes ausgehend von den gespeicherten Photonen.
     * @param pos Schnittpunkt
     * @param object geschnittenes Objekt
     * @return color
     */
    Color3f calcColor(const Vector3f& pos, SceneObject* object) const;

    /**
     * Erstellt ein Vertex Buffer Objekt aus der Liste der Photonen und
     * speichert es in der PhotonMap.
     */
    void createVertexBufferObject();

private:
    /** KD-Struktur zur Abfrage der nächstgelegenen Photonen */
    NearestPhotonQuery* nearestPhotonQuery;

    /** Suchradius */
    const GLfloat radius;

    /** Anzahl der in der Szene vorhandenen Photonen */
    unsigned long totalPhotonCount;

    /** Liste der Photonen, die zur Initialisierung genutzt wurden */
    Photons photons;

    /** Eigenschaften des konstruierten KD-Baums */
    const int maxLeafSize, maxLevel;

    /** Koordinaten aller Photonen */
    GLfloat* photonData = nullptr;

    /** Indizes (= Anzahl) aller Photonen */
    GLuint* photonIndices = nullptr;

    /**
     * Ermittelt die nächstgelegenen Nachbar-Photonen
     * @param pos Schnittpunkt
     * @param object geschnittenes Objekt
     * @return Liste der gefundenen relevanten Photonen
     */
    Photons findNeighbors(const Vector3f& pos, SceneObject* object) const;

    /**
     * Liefert die Information, ob das Photon für den Schnittpunkt relevant ist.
     * (true wenn sich das Photon auf der gleichen Objektseite befindet und die Einschlagrichtung stimmt)
     * @param pos Schnittpunkt
     * @param object geschnittenes Objekt
     * @param photon gefundenes Photon
     * @return true: ist relevant, false: von falscher Seite aufgetroffen
     */
    GLboolean isOnCorrectSide(const Vector3f& pos, SceneObject* object, Photon photon) const;

};
}
#endif /* PHOTONMAP_HPP_ */

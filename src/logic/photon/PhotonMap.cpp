#include "PhotonMap.hpp"
#include <sceneTypes.hpp>
#include <stdlib.h>
#include <mathUtils.hpp>
#include <debugGL.hpp>

namespace momentum {

/**
 * Konstruktorfunktion
 * @param maxLeafSize maximale Kinderanzahl
 * @param maxLevel maximale Tiefe des Baums
 * @param radius Suchradius
 */
PhotonMap::PhotonMap(int maxLeafSize, int maxLevel, GLfloat radius) :
        nearestPhotonQuery(nullptr),
                radius(radius),
                totalPhotonCount(0),
                photons(*(new Photons())),
                maxLeafSize(maxLeafSize),
                maxLevel(maxLevel) {
}

/**
 * Konstruktorfunktion
 * @param photons Liste der Photonen
 * @param maxLeafSize maximale Kinderanzahl
 * @param maxLevel maximale Tiefe des Baums
 * @param radius Suchradius
 */
PhotonMap::PhotonMap(const Photons& photons, int maxLeafSize, int maxLevel, GLfloat radius) :
        nearestPhotonQuery(new NearestPhotonQuery(photons, maxLeafSize, maxLevel)),
                radius(radius), photons(photons),
                maxLeafSize(maxLeafSize),
                maxLevel(maxLevel) {
    createVertexBufferObject();
}

/**
 * Destruktorfunktion
 */
PhotonMap::~PhotonMap() {
    free(this->photonIndices);
    free(this->photonData);
}

/**
 * Setzt die Liste an Photonen.
 * @param photons Photonen
 */
void PhotonMap::setPhotons(Photons photons) {
    this->photons = photons;
    createKDTree();
}

/**
 * Erzeugt den KD-Tree auf Grundlage der vorhandenen Liste an Photonen.
 */
void PhotonMap::createKDTree() {
    if (!photons.empty())
        nearestPhotonQuery = new NearestPhotonQuery(photons, maxLeafSize, maxLevel);
    createVertexBufferObject();
}

/**
 * Fügt der Liste an Photonen ein Photon hinzu.
 * @param p Photon
 */
void PhotonMap::put(Photon p) {
    photons.push_back(p);
}

/** Liefert die Liste der Photonen, die zur Initialisierung genutzt wurden */
const Photons& PhotonMap::getPhotons() const {
    return this->photons;
}

/** Liefert das Vertex Buffer Datenarray */
GLfloat* PhotonMap::getPhotonData() const {
    return this->photonData;
}

/** Liefert den Zeiger auf den ersten Farbwertes des Vertex Buffer Arrays*/
GLfloat* PhotonMap::getPhotonColors() const {
    return &(this->photonData[3]);
}

/** Liefert den Zeiger auf den ersten Index des Indexarrays */
GLuint* PhotonMap::getPhotonIndices() const {
    return this->photonIndices;
}

/** Liefert die Gesamtanzahl der vorhandenen Photonen */
long unsigned int PhotonMap::getTotalPhotonCount() const {
    return this->totalPhotonCount;
}

/**
 * Ermittelt die nächstgelegenen Nachbar-Photonen
 * @param pos Schnittpunkt
 * @param object geschnittenes Objekt
 * @return Liste der gefundenen relevanten Photonen
 */
Photons PhotonMap::findNeighbors(const Vector3f& pos, SceneObject* object) const {
    if (photons.empty())
        return photons;
    Neighbors neighbours;
    Photons photons;
    int foundNeighbours = this->nearestPhotonQuery->FindNeighbors(pos, this->radius, neighbours);
    // Aussortieren der Photonen, die die falsche Einschlagrichtung haben
    for (GLint index = 0; index < foundNeighbours; index++) {
        Photon photon = this->photons[neighbours[index]];
        //if (isOnCorrectSide(pos, object, photon)) {
        photons.push_back(photon);
        //}
    }
    return photons;
}

/**
 * Liefert die Information, ob das Photon für den Schnittpunkt relevant ist.
 * (true wenn sich das Photon auf der gleichen Objektseite befindet und die Einschlagrichtung stimmt)
 * @param pos Schnittpunkt
 * @param object geschnittenes Objekt
 * @param photon gefundenes Photon
 * @return true: ist relevant, false: von falscher Seite aufgetroffen
 */
GLboolean PhotonMap::isOnCorrectSide(const Vector3f& pos, SceneObject* object,
        Photon photon) const {
    Vector3f n = object->getNormalAt(pos);
    // prüfen ob Fläche die gleiche Normale hat
    GLboolean relevantSide = (object->getType() == oSphere)
            || (n == object->getNormalAt(photon.GetPosition()));
    // Falls relevant, auch noch Winkel zwischen n und Einschlagrichtung
    return relevantSide && (M_PI_2 > CALC_ANGLE_BETWEEN(n, -photon.getRay().direction));
}

/**
 * Ermittelt die Farbe eines Punktes ausgehend von den gespeicherten Photonen.
 * @param pos Schnittpunkt
 * @param object geschnittenes Objekt
 * @return color
 */
Color3f PhotonMap::calcColor(const Vector3f& pos, SceneObject* object) const {
    Photons foundPhotons = findNeighbors(pos, object);
    Color3f color = Color3f( { COLOR_BLACK });
    for (Photon& photon : foundPhotons) {
        // Verrechnen der Farbe mit Abschwächung anhand des Abstands
        GLfloat dist = gte::Length(photon.GetPosition() - pos);
        color += photon.getColor() * gaussFunction(3.0f * dist, 0.0, this->radius);
    }

    return color * (GLfloat) foundPhotons.size()
            / ((GLfloat) M_PI * this->radius * this->radius);
}

/**
 * Erstellt ein Vertex Buffer Objekt aus der Liste der Photonen und
 * speichert es in der PhotonMap.
 */
void PhotonMap::createVertexBufferObject() {
    GLuint u = 0;

// Speicher für Vertex-Buffer-Arrays reservieren

    this->totalPhotonCount = this->photons.size();
// 2 Indizes pro Photon für Start- und Endpunkt
    this->photonData = new GLfloat[9 * 2 * this->totalPhotonCount];
    this->photonIndices = new GLuint[2 * this->totalPhotonCount];

// Photon-Informationen in Vertex-Buffer-Arrays schreiben
    for (Photon& p : this->photons) {

        Ray3f ray = p.getRay();
        Color3f col = p.getColor();
        gte::Normalize(col); // better view for debugging
        Point3f end = ray.origin - ray.direction * IMPACT_LENGTH;
        // Aufbauen des Daten- / Farb- Array
        for (int i = 0; i < 3; i++) {
            // Startpunkt
            this->photonData[6 * u + i] = ray.origin[i];
            this->photonData[6 * u + 3 + i] = col[i];
            // Endpunkt
            this->photonData[6 * u + 6 + i] = end[i];
            this->photonData[6 * u + 9 + i] = col[i];
        }

        // Aufbauen des Index-Arrays
        this->photonIndices[u] = u;
        this->photonIndices[u + 1] = u + 1;

        u += 2;
    }
}

}

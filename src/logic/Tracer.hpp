#ifndef TRACER_HPP_
#define TRACER_HPP_

#include <Scene.hpp>
#include <sceneTypes.hpp>
#include <objectTypes.hpp>

namespace momentum {

/**
 * Abstrakte Klasse, die die Basisfunktionalität zur Arbeit von Strahlen bereitstellt.
 */
class Tracer {
public:
    /**
     * Konstruktorfunktion
     */
    Tracer();
    /**
     * Destruktorfunktion
     */
    virtual ~Tracer();

protected:
    /**
     * Schießt einen Strahl durch eine Szene mit den übergebenen Szenen-Objekten und liefert den
     * Schnittpunkt, das Objekt und den Abstand zum nächstgelegenen Schnittpunkt.
     * @param ray Strahl
     * @param objects Szenenobjekte
     * @param self Ggf. Object, das von Schnitttest ausgenommen ist
     * @return Schnitt-Ergebnis
     */
    IntersectionResult findNearestIntersectionResult(const Ray3f& ray,
            const SceneObjectList& objects, SceneObject* self);

    /**
     * Schießt ein Photon durch die Szene und speichert die Ergebnisse in der Photon-Liste.
     * @param ray zu verfolgender Strahl
     * @param refractionIndex Brechungsindex des Austrittmaterials
     * @param color Farbe des Strahls
     * @param objects Szenenobjekte
     * @param self Ggf. Object, das von Schnitttest ausgenommen ist
     * @param photonPathSoFar bisherige Liste der Reflexionen des Photons (bei neuen Photonen leer)
     * @return Liste der Schnitt-Ergebnisse auf dem Weg des Strahls
     */
    IntersectionList traceRay(const Ray3f& ray, GLfloat refractionIndex,
            Color3f& color, SceneObjectList objects, SceneObject* self,
            IntersectionList photonPathSoFar);

    /**
     * Berechnet einen zufälligen Strahl in dessen Richtung ein Photon ausgehend von einem Punkt geschossen werden muss.
     * @param point Ausgangspunkt
     * @param currPhoton Zähler der geschossenen Photonen (inkl. des aktuellen)
     * @param totalPhotonCount Anzahl der insgesamt zu verschießenden Photonen
     * @return Strahl
     */
    Ray3f calcRandomRay(const Vector3f& point);

    /**
     * Berechnet den Spekularen Ray des Punktes point
     * @param point Schnittpunkt
     * @param dir Einfallsrichtung
     * @param n Normale
     * @return Strahl
     */
    Ray3f calcSpecularRay(const Vector3f& point, const Vector3f& dir, Vector3f n);

    /**
     * Berechnet einen zufälligen Diffusen Strahl
     * @param point Ausgangspunkt
     * @param n Normale der Oberfläche
     * @return Strahl
     */
    Ray3f calcDiffuseRay(const Vector3f& point, Vector3f n);

    /**
     * Berechnet den transmistierenden Strahl
     * @param point Schnittpunkt
     * @param dir Einfallsrichtung
     * @param n Normale
     * @param n1 Brechungsindex Austrittsmaterial
     * @param n2 Brechungsindex Eintrittsmaterial
     * @return Strahl
     */
    Ray3f calcTransmissionRay(const Vector3f& point, const Vector3f& dir, const Vector3f n,
            GLfloat n1, GLfloat n2);

    /**
     * Berechnet die Emissions-Richtung des Photons ausgehend von zwei gleichverteilten
     * Zufallszahlen xi1 und xi2 zwischen [0,1], die auf Kugelkoordinaten theta und phi
     * abgebildet werden.
     * @return Emissionsrichtung
     */
    Vector3f getRandomVector() const;

};

}

#endif /* TRACER_HPP_ */

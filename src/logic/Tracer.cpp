/*
 * Tracer.cpp
 *
 *  Created on: 06.07.2015
 *      Author: ellen
 */

#include "Tracer.hpp"
#include <mathUtils.hpp>

namespace momentum {

/**
 * Konstruktorfunktion
 */
Tracer::Tracer() {
}

/**
 * Destruktorfunktion
 */
Tracer::~Tracer() {
}

/**
 * Berechnet die Emissions-Richtung des Photons ausgehend von zwei gleichverteilten
 * Zufallszahlen xi1 und xi2 zwischen [0,1], die auf Kugelkoordinaten theta und phi
 * abgebildet werden.
 * @return Emissionsrichtung
 */
Vector3f Tracer::getRandomVector() const {
    GLfloat xi1 = RANDOM_FLOAT(), xi2 = RANDOM_FLOAT();
    // Abbilden auf Theta und Phi der Kugel (Punktlichtquelle)
    GLfloat theta = RAD2DEG(asinf(2.0 * xi1 - 1.0));
    GLfloat phi = RAD2DEG(2.0 * M_PI * xi2);

    return calcDirection(theta, phi);
}

/**
 * Berechnet einen zufälligen Strahl in dessen Richtung ein Photon ausgehend von einem Punkt geschossen werden muss.
 * @param point Ausgangspunkt
 * @param currPhoton Zähler der geschossenen Photonen (inkl. des aktuellen)
 * @param totalPhotonCount Anzahl der insgesamt zu verschießenden Photonen
 * @return Strahl
 */
Ray3f Tracer::calcRandomRay(const Vector3f& point) {
    return Ray3f(point, getRandomVector());
}

/**
 * Berechnet einen zufälligen Diffusen Strahl
 * @param point Ausgangspunkt
 * @param n Normale der Oberfläche
 * @return Strahl
 */
Ray3f Tracer::calcDiffuseRay(const Vector3f& point, Vector3f n) {
    Vector3f direction = getRandomVector();
    if (M_PI_2 > CALC_ANGLE_BETWEEN(n, -direction)) {
        direction *= -1.0f;
    }
    return Ray3f(point, direction);
}

/**
 * Berechnet den Spekularen Ray des Punktes point
 * @param point Schnittpunkt
 * @param dir Einfallsrichtung
 * @param n Normale
 * @return Strahl
 */
Ray3f Tracer::calcSpecularRay(const Vector3f& point, const Vector3f& dir, Vector3f n) {
    Vector3f reflection = dir - 2.0f * gte::Dot(n, dir) * n;
    gte::Normalize(reflection);
    return Ray3f(point, reflection);
}

/**
 * Berechnet den transmistierenden Strahl
 * @param point Schnittpunkt
 * @param dir Einfallsrichtung
 * @param n Normale
 * @param n1 Brechungsindex Austrittsmaterial
 * @param n2 Brechungsindex Eintrittsmaterial
 * @return Strahl
 */
Ray3f Tracer::calcTransmissionRay(const Vector3f& point, const Vector3f& dir, const Vector3f n,
        GLfloat n1, GLfloat n2) {
    // Brechungsindex
    GLfloat r = n1 / n2;
    // Einfallswinkel
    GLfloat angleOfIncidence = gte::Dot(-n, dir);
    // Austrittswinkel
    GLfloat angleOfRefraction = sqrtf(1.0 - r * r * powf(1.0 - angleOfIncidence, 2.0));

    Vector3f transmissionDir = r * dir + (r * angleOfIncidence - angleOfRefraction) * n;
    gte::Normalize(transmissionDir);

    return Ray3f(point, transmissionDir);
}

/**
 * Schießt einen Strahl durch eine Szene mit den übergebenen Szenen-Objekten und liefert den
 * Schnittpunkt, das Objekt und den Abstand zum nächstgelegenen Schnittpunkt.
 * @param ray Strahl
 * @param objects Szenenobjekte
 * @param self Ggf. Object, das von Schnitttest ausgenommen ist
 * @return Schnitt-Ergebnis
 */
IntersectionResult Tracer::findNearestIntersectionResult(const Ray3f& ray,
        const SceneObjectList& objects, SceneObject* self) {
    IntersectionResult intersection = {
            Ray3f( { 0.0, 0.0, 0.0 }, { 0.0, 0.0, 0.0 }), INFINITY, nullptr, PROPAG_NONE
    };

    for (SceneObject* obj : objects) {
        if (obj != self) {
            Vector3f* closestIntersectionPoint = obj->createIntersectionPoint(ray);

            if (closestIntersectionPoint != nullptr) { // Falls Objekt geschnitten
                GLfloat currIntersectionDist = gte::Length(ray.origin - *closestIntersectionPoint);

                if (currIntersectionDist < intersection.distance) { // Neue kürzeste Distanz
                    intersection.distance = currIntersectionDist;
                    intersection.ray = Ray3f(*closestIntersectionPoint, ray.direction);
                    intersection.object = obj;
                }
                delete closestIntersectionPoint;
            }
        }
    }
    return intersection;
}

/**
 * Schießt ein Photon durch die Szene und speichert die Ergebnisse in der Photon-Liste.
 * @param ray zu verfolgender Strahl
 * @param refractionIndex Brechungsindex des Austrittmaterials
 * @param color Farbe des Strahls
 * @param objects Szenenobjekte
 * @param self Ggf. Object, das von Schnitttest ausgenommen ist
 * @param photonPathSoFar bisherige Liste der Reflexionen des Photons (bei neuen Photonen leer)
 * @return Liste der Schnitt-Ergebnisse auf dem Weg des Strahls
 */
IntersectionList Tracer::traceRay(const Ray3f& ray, GLfloat refractionIndex,
        Color3f& color, SceneObjectList objects, SceneObject* self,
        IntersectionList photonPathSoFar) {
    IntersectionList intersectionList = photonPathSoFar;
    IntersectionResult intersection = findNearestIntersectionResult(ray, objects, self);
    if (intersection.object != nullptr) {
        Material material = intersection.object->getMaterial();
        GLfloat xi = RANDOM_FLOAT();
        if (xi < material.probabilityDiffuse) {
            // diffuse Reflexion
            // (Farbe darf hier nicht verrechnet werden,
            // sondern diffuser Fall wird in aufrufenden Methoden behandelt)
            intersection.propagationType = PROPAG_DIFFUSE;
            intersectionList.push_back(intersection);
            return intersectionList;
        } else if (xi < (material.probabilityDiffuse + material.probabilitySpecular)) {
            // spekulare Reflexion
            Ray3f specRay = calcSpecularRay(intersection.ray.origin, ray.direction,
                    intersection.object->getNormalAt(intersection.ray.origin));
            color *= material.specular;
            intersection.propagationType = PROPAG_SPECULAR;
            intersectionList.push_back(intersection);
            IntersectionList furtherPropagation = traceRay(specRay, material.refractionIndex, color,
                    objects,
                    intersection.object,
                    IntersectionList());
            intersectionList.insert(intersectionList.end(), furtherPropagation.begin(),
                    furtherPropagation.end());
            return intersectionList;
        } else if (xi
                < (material.probabilityDiffuse + material.probabilitySpecular
                        + material.probabilityTransmission)) {
            // Transmission
            Ray3f refractionRay = calcTransmissionRay(intersection.ray.origin,
                    ray.direction,
                    intersection.object->getNormalAt(intersection.ray.origin),
                    refractionIndex,
                    material.refractionIndex);
            color *= material.transmission;
            intersection.propagationType = PROPAG_TRANSMISSION;
            intersectionList.push_back(intersection);
            IntersectionList furtherPropagation = traceRay(refractionRay, material.refractionIndex,
                    color, objects,
                    intersection.object,
                    IntersectionList());
            intersectionList.insert(intersectionList.end(), furtherPropagation.begin(),
                    furtherPropagation.end());
            return intersectionList;
        } else {
            // Absorption
            intersection.propagationType = PROPAG_ABSORPTION;
            intersectionList.push_back(intersection);
            return intersectionList;
        }
    }
    return intersectionList;
}

}

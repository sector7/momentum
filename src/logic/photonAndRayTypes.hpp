#ifndef __PHOTONANDRAYTYPES_HPP__
#define __PHOTONANDRAYTYPES_HPP__

#include <commonTypes.hpp>
#include <objectTypes.hpp>
#include <Photon.hpp>
#include <mathUtils.hpp>

#include <stdio.h>
#include <list>
#include <array>
#include <limits.h>

#include <GteIntrRay3AlignedBox3.h>
#include <GteIntrRay3Sphere3.h>
#include <GteNearestNeighborQuery.h>

using namespace std;

namespace momentum {

/** PhotonMaps */
typedef enum {
    DIRECT_ILLU, INDIRECT_ILLU, CAUSTIC_ILLU
} PhotonMapType;

/** Aufzählung der Propagationstypen */
typedef enum {
    PROPAG_NONE, PROPAG_DIFFUSE, PROPAG_SPECULAR, PROPAG_TRANSMISSION, PROPAG_ABSORPTION
} PropagationType;

/** Brechungsfaktoren */
#define RI_AIR      1.000292f
#define RI_WATER    1.33f
#define RI_GLASS    1.8f

/** Photonen und kd-tree */
#define PHOTON_COUNT_PER_LIGHT_SRC    25000
#define MAX_NEIGHBOURS 100

#define DIMENSION 3
// Anzahl an Blättern, die sich in den Regionen befinden dürfen
// größere Zahl: Baum baut schneller, Queries dauern länger
// kleinere Zahl: Baum braucht länger, Queries gehen schneller
// -> Größe der Liste von Punkten, die in den Blättern gespeichert wird
#define MAX_LEAF_SIZE 5
// Maximale Anzahl an Trennebenen - nacheinander in x, y und z
// zwei Lichtquellen, eine Ebene pro Koordinate, 1000 möglichte Reflexionen
#define MAX_LEVEL MIN(INT_MAX, (PHOTON_COUNT_PER_LIGHT_SRC*2*3*1000))

// NearestNeighbour / NearestNeighborQuery<<N>, <Real>, <Site>, <MaxNeighbors>>
typedef gte::NearestNeighborQuery<DIMENSION, GLfloat, Photon, MAX_NEIGHBOURS> NearestPhotonQuery;

typedef std::vector<Photon> Photons;
// Liste der gefundenen Nachbarn:
// liefert die Indices aus der Liste, anhand derer der kd-Tree erstellt wurde
typedef std::array<GLint, MAX_NEIGHBOURS> Neighbors;

/** Raytracer */
#define RAY_COUNT 1
#define ANTIALIAS_FACTOR 1

// Länge der gerenderten Einschlagsrichtung der PhotonMaps
#define IMPACT_LENGTH 0.05f

}
#endif /* __PHOTONANDRAYTYPES_HPP__ */

/* ---- Eigene Header einbinden ---- */
#include "Raytracer.hpp"
#include <sceneTypes.hpp>
#include <debugGL.hpp>
#include <mathUtils.hpp>
#include <flag.hpp>

/* ---- Externe Header ---- */
#include <GteVector.h>
#include <GteVector3.h>
#include <iostream>
#include <cmath>

namespace momentum {

/**
 * Konstruktorfunktion - berechnet die benötigten Variablen.
 * @param eye Augpunkt
 * @param up Up-Vektor des Augpunkts
 * @param right Right-Vektor des Augpunkts
 * @param dir Blickrichtung
 * @param fov Öffnungswinkel der Kamera
 * @param windowWidth Breite des Fensters in Pixeln
 * @param windowHeight Höhe des Fensters in Pixeln
 * @param rayCount Anzahl an Strahlen für das Multisampling
 * @param antialiasingFactor Faktor für das Antialiasing
 * @param scene Szene
 */
Raytracer::Raytracer(Camera camera, GLuint windowWidth, GLuint windowHeight, GLuint rayCount,
        GLuint antialiasingFactor, const Scene& scene) :
        Tracer(), eye(camera.getPos()), windowWidth(windowWidth), windowHeight(windowHeight), rayCount(
                rayCount), antialiasingFactor(antialiasingFactor), scene(&scene) {
    INFO(("Kameraposition: (%f, %f, %f)\n", eye[X], eye[Y], eye[Z]));

    // Projektionsfläche berechnen
    GLfloat lengthProjection = tanf(DEG2RAD(camera.getFov()) / 2.0f) * 2.0f * this->znear;
    GLfloat ratio = (GLfloat) this->windowHeight / this->windowWidth;
    this->widthProjection = lengthProjection / ratio * camera.getRight();
    this->heightProjection = lengthProjection * camera.getUp();

    // Schrittweite pro Pixel
    this->deltaHeight = this->heightProjection / ((GLfloat) this->windowHeight);
    this->deltaWidth = this->widthProjection / ((GLfloat) this->windowWidth);

    // Ursprung
    this->originProjection = eye;
    this->originProjection += camera.getDir() * this->znear;
    this->originProjection -= (this->widthProjection + this->heightProjection) * 0.5f;

    // Ursprung ist direkt der unterst linkeste Pixel
    this->originProjection += 0.5f * (deltaWidth + deltaHeight);

}

Raytracer::~Raytracer() {

}

/**
 * Führt das Raytracing eines bestimmten Strahles aus.
 * @param ray Strahl
 * @return Farbe des Objekts
 */
Color3f Raytracer::raytrace(const Ray3f& ray) {
    static Color3f maxColor = { COLOR_WHITE };
    Color3f completeColor = Color3f( { COLOR_BLACK });
    IntersectionResult intersection;
    IntersectionList intersectionList;
    for (GLuint rayCount = 0; rayCount < this->rayCount; rayCount++) {
        // wichtig: weiße Farbe muss rein, da sie mit allen weiteren Objektfarben multipliziert wird
        Color3f rayColor = Color3f( { COLOR_WHITE });

        // Falls Strahl absorbiert wurde, einen neuen losschicken
        do {
            intersectionList = this->traceRay(ray, RI_AIR, rayColor, scene->getObjects(), nullptr,
                    intersectionList);
        } while (intersection.object != nullptr && intersection.propagationType == PROPAG_ABSORPTION);

        if (!intersectionList.empty()) {
            intersection = intersectionList.back();
            if (getFlag (flag_render_direct_illu)) {
                completeColor = directIllumination(intersection.ray.origin,
                        intersection.object->getNormalAt(intersection.ray.origin),
                        intersection.object);
            }
            if (getFlag (flag_render_photons)) {
                completeColor += scene->getPhotonMaps().calcColor(intersection.ray.origin,
                        intersection.object);
            }
        }
        // Verrechnen mit Farbe der Objekte, die wir vor der diffusen Reflexion getroffen haben
        completeColor *= rayColor;
    }
// INFO(("Color: (%f, %f, %f)\n", completeColor[0], completeColor[1], completeColor[2]));
// durch Anzahl der verschossenen Strahlen pro Pixel teilen
    // Farbe auf vollweiß begrenzen (Verhinderung von Übersteuerung)
    return min(maxColor, completeColor / (float) this->rayCount);
}

/**
 * Berechnet das lokale Beleuchtungsmodell für einen Punkt in der Geometrie.
 * @param closestIntersectionPoint Punkt, dessen Beleuchtung bestimmt werden soll
 * @param surfaceNormal Normale der getroffenen Oberfläche
 * @param self zu beleuchtendes Objekt
 * @return resultierende Farbe
 */
Color3f Raytracer::directIllumination(const Vector3f& closestIntersectionPoint,
        const Vector3f surfaceNormal, SceneObject* self) {
    Color3f direct = self->getMaterial().ambient;
    SceneObjectList objects = scene->getObjects();
    LightList lights = scene->getLights();
    for (const Light* light : lights) {
        Vector3f lightDir = light->getPosition() - closestIntersectionPoint;
        GLfloat lightDist = gte::Normalize(lightDir);
        Ray3f shadowRay = Ray3f(closestIntersectionPoint, lightDir);
        IntersectionResult intersection = findNearestIntersectionResult(shadowRay, objects, self);

        // Winkel zur Lichtquelle >= 90°?
        GLfloat lightAngle = CALC_ANGLE_BETWEEN(lightDir, surfaceNormal);
        if (M_PI_2 > lightAngle && (intersection.distance >= lightDist)) {
            direct += self->getMaterial().diffuse * light->getColor() * cosf(lightAngle)
                    / ((lightDist * lightDist) / LIGHT_WEAKENING);
        }
    }
    return direct;
}

/**
 * Berechnet den Strahl, der vom Augpunkt ausgehend durch die Projektionsfläche
 * an der Pixelposition x, y geschossen wird.
 * @param x X-Wert des Pixels im Framebuffer (horizontal)
 * @param y Y-Wert
 * @return Strahl
 */
Ray3f Raytracer::calcRay(GLfloat x, GLfloat y) {
    Vector3f pointOnProjectionSurface = originProjection;
    pointOnProjectionSurface += x * deltaWidth;
    pointOnProjectionSurface += y * deltaHeight;
    Vector3f direction = pointOnProjectionSurface - eye;
    gte::Normalize(direction);
    return Ray3f(eye, direction);
}

/**
 * Setzt die RGB Werte für eine Farbe.
 * @param color Zu setzende Farbe
 * @param target Pixel im Buffer, das Farbe erhält
 */
void Raytracer::setColor(const Color3f& color, GLfloat target[]) {
    target[R] = color[R];
    target[G] = color[G];
    target[B] = color[B];
}

/**
 * Berechnet die Pixel der Szene
 * @param pixelBuffer Array zum Speichern der Farbwerte
 * @param windowWidth Breite des Fensters
 * @param line Erste zu schreibende Zeile
 * @param toLine Letzte, zu schreibende Zeile
 */
void Raytracer::getPixelColorsForScene(GLfloat* pixelBuffer, GLuint windowWidth, GLuint fromLine,
        GLuint toLine) {
    for (GLuint x = 0; x < windowWidth; x++) {
        for (GLuint y = fromLine; y < toLine; y++) {
            Color3f pixelColor;
            pixelColor.MakeZero();
            for (GLuint aliasY = 0; aliasY < this->antialiasingFactor; aliasY++) {
                for (GLuint aliasX = 0; aliasX < this->antialiasingFactor; aliasX++) {
                    pixelColor += raytrace(
                            calcRay(x + ((GLfloat) aliasX) / this->antialiasingFactor,
                                    y + ((GLfloat) aliasY) / this->antialiasingFactor));
                }
            }
            setColor(pixelColor / (GLfloat)(this->antialiasingFactor * this->antialiasingFactor),
                    (GLfloat*) &pixelBuffer[(y * windowWidth + x) * 3]);
        }
    }
}

} /* namespace mmoentum */

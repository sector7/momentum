#ifndef __RAYTRACER_HPP__
#define __RAYTRACER_HPP__

#include <commons.hpp>

#include <Scene.hpp>
#include <Tracer.hpp>
#include <Camera.hpp>

namespace momentum {

/**
 * Klasse zur Umsetzung des Raytracing.
 */
class Raytracer: Tracer {

private:
    /** Füllfarbe, wenn kein Object getroffen wurde */
    Color3f g_fillColor = { 1.0f, 1.0f, 0.0f };
    /** Augpunkt */
    Vector3f eye;
    /** Abstand des Augpunkts zur Projektionsfläche */
    GLfloat znear = ZNEAR;
    /** Ursprung der Projektionsfläche (unten links) */
    Vector3f originProjection;
    /** Höhe der Projektionsfläche */
    Vector3f heightProjection;
    /** Breite der Projektionsfläche */
    Vector3f widthProjection;
    /** Breite des Fensters */
    GLint windowWidth;
    /** Höhe des Fensters */
    GLint windowHeight;
    /** horizontale Schrittweite pro Pixel */
    Vector3f deltaHeight;
    /** vertikale Schrittweite pro Pixel */
    Vector3f deltaWidth;
    /** Anzahl an Strahlen für das Multisampling */
    GLuint rayCount;
    /** Antialisasing Faktor */
    GLuint antialiasingFactor;

    /** Szene */
    const Scene* scene;

    /**
     * Führt das Raytracing eines bestimmten Strahles aus.
     * @param ray Strahl
     * @return Farbe des Objekts
     */
    Color3f raytrace(const Ray3f& ray);

    /**
     * Liefert die Farbe eines bestimmten Pixels.
     * @param x x-Koordinate
     * @param y y-Koordinate
     * @return Farbe
     */
    Color3f computePixelColor(GLuint x, GLuint y);

    /**
     * Setzt die RGB Werte für eine Farbe.
     * @param color Zu setzende Farbe
     * @param target Pixel im Buffer, das Farbe erhält
     */
    void setColor(const Color3f& color, GLfloat target[]);

    /**
     * Berechnet den Strahl, der vom Augpunkt ausgehend durch die Projektionsfläche
     * an der Pixelposition x, y geschossen wird.
     * @param x X-Wert des Pixels im Framebuffer (horizontal)
     * @param y Y-Wert ebenso. (vertikal)
     * @return Strahl
     */
    Ray3f calcRay(GLfloat x, GLfloat y);

    /**
     * Berechnet, wie groß der Schattenanteil durch Verdeckung ist.
     * @param closestIntersectionPoint Punkt, dessen Verdeckung bestimmt werden soll
     * @param surfaceNormal Normale der getroffenen Oberfläche
     * @param self zu schattierendes Objekt
     * @return Faktor zwischen 0 (keine Verdeckung) und 1 (vollständige Verdeckung)
     */
    Color3f directIllumination(const Vector3f& closestIntersectionPoint,
            const Vector3f surfaceNormal, SceneObject* self);

public:
    /**
     * Konstruktorfunktion - berechnet die benötigten Variablen.
     * @param camera Kameraeinstellung
     * @param windowWidth Breite des Fensters in Pixeln
     * @param windowHeight Höhe des Fensters in Pixeln
     * @param rayCount Anzahl an Strahlen für das Multisampling
     * @param antialiasingFactor Faktor für das Antialiasing
     * @param scene Szene
     */
    Raytracer(Camera camera, GLuint windowWidth, GLuint windowHeight, GLuint rayCount,
            GLuint antialiasingFactor, const Scene& scene);

    /**
     * Destruktorfunktion
     */
    virtual ~Raytracer();

    /**
     * Berechnet die Pixel der Szene
     * @param pixelBuffer Array zum Speichern der Farbwerte
     * @param windowWidth Breite des Fensters
     * @param line Erste zu schreibende Zeile
     * @param toLine Letzte, zu schreibende Zeile
     */
    void getPixelColorsForScene(GLfloat* pixelBuffer, GLuint windowWidth, GLuint fromLine,
            GLuint toLine);

};

} /* namespace momentum */

#endif

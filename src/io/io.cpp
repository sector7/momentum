/**
 * @file
 * Ein-/Ausgabe-Modul.
 * Das Modul kapselt die Ein- und Ausgabe-Funktionalität (insbesondere die GLUT-
 * Callbacks) des Programms.
 *
 * @author Ellen Schwartau (Minf9888) und Malte Jörgens (Minf9640)
 */

/* ---- System Header einbinden ---- */
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <ctype.h>

#include <GL/glut.h>

/* ---- Eigene Header einbinden ---- */
#include "io.hpp"
#include <scene.hpp>
#include <ioTypes.hpp>
#include <debugGL.hpp>
#include <flag.hpp>
#include <PixelRenderer.hpp>
#include <mathUtils.hpp>

/* ---- Konstanten ---- */
/** Anzahl der Aufrufe der Timer-Funktion pro Sekunde */
#define TIMER_CALLS_PS 60.0f

namespace momentum {

/* Scene Manager der Szene */
SceneManager sceneMgr = SceneManager();

/** Kamera */
Camera g_camera = Camera();

/** Konfiguration - über Default-Werte bzw. Parameter */
Config g_config = Config();

/* ---- Funktionen ---- */
/**
 * Liefert die Kamera-Einstellungen.
 * @return Kamera-Einstellung
 */
Camera getCamera() {
    return g_camera;
}

/**
 * Setzen der Projektionsmatrix.
 * Setzt die Projektionsmatrix unter Beruecksichtigung des Seitenverhaeltnisses
 * des Anzeigefensters, sodass das Seitenverhaeltnisse der Szene unveraendert
 * bleibt und gleichzeitig entweder in x- oder y-Richtung der Bereich von -1
 * bis +1 zu sehen ist.
 * @param aspect Seitenverhaeltnis des Anzeigefensters (In).
 */
void setProjection(GLdouble aspect) {
    /* Nachfolgende Operationen beeinflussen Projektionsmatrix */
    glMatrixMode (GL_PROJECTION);
    /* Matrix zuruecksetzen - Einheitsmatrix laden */
    glLoadIdentity();
    /* perspektivische Projektion */
    gluPerspective(g_camera.getFov(), /* Oeffnungswinkel */
    aspect, /* Seitenverhaeltnis */
    0.1, /* nahe Clipping-Ebene */
    99.0); /* ferne Clipping-Ebene */
}

/**
 * Rotiert die Blickrichtung sowie Up- und Right-Vektor der Kamera.
 * @param offset Bewegungsvektoren der Maus für jeweils Links- und
 * Rechtsklick.
 */
void turnCamera(MouseOffset offset) {
    g_camera.turnCamera(offset);
}

// -------------------------------------------------------------------------
//                          Callbacks
// -------------------------------------------------------------------------
/**
 * Verarbeitung eines Tasturereignisses.
 * ESC-Taste und q, Q beenden das Programm.
 * @param key Taste, die das Ereignis ausgeloest hat. (ASCII-Wert oder WERT des
 *        GLUT_KEY_<SPECIAL>.
 * @param status Status der Taste, GL_TRUE=gedrueckt, GL_FALSE=losgelassen.
 * @param isSpecialKey ist die Taste eine Spezialtaste?
 * @param x x-Position des Mauszeigers zum Zeitpunkt der Ereignisausloesung.
 * @param y y-Position des Mauszeigers zum Zeitpunkt der Ereignisausloesung.
 */
static void handleKeyboardEvent(int key, int status, GLboolean isSpecialKey, int x, int y) {
    /** Keycode der ESC-Taste */
#define ESC 27
#define SPACE 32

    /* Taste gedrückt */
    if (status == GLUT_DOWN) {
        /* keine Spezialtaste gedrückt */
        if (!isSpecialKey) {
            switch (toupper(key)) {
            /* Vorwärts gehen */
            case 'W':
                g_camera.moveCamera(dirUp);
                break;
                /* Nach links gehen */
            case 'A':
                g_camera.moveCamera(dirLeft);
                break;
                /* Nach rechts gehen */
            case 'S':
                g_camera.moveCamera(dirDown);
                break;
                /* Rückwärts gehen */
            case 'D':
                g_camera.moveCamera(dirRight);
                break;
            case 'C':
                g_camera.switchCamera();
                break;
                /* Hilfetext ausgeben */
            case 'H':
                toggleFlag (flag_usage);
                break;
            case 'L':
                if (!toggleFlag(flag_render_direct_illu))
                    setFlag (flag_render_photons);
                break;
            case 'P':
                if (!toggleFlag(flag_render_photons)) {
                    setFlag (flag_render_direct_illu);
                }
                break;
                /* Hilfetext ausgeben */
            case '1':
                toggleFlag (flag_show_direct_photons);
                break;
                /* Hilfetext ausgeben */
            case '2':
                toggleFlag (flag_show_indirect_photons);
                break;
                /* Hilfetext ausgeben */
            case '3':
                toggleFlag (flag_show_caustic_photons);
                break;
                /* Programm beenden */
            case 'Q':
                case ESC:
                exit(0);
                break;
            }
        } else {
            switch (key) {
            /* (De-)Aktivieren des Wireframemode */
            case GLUT_KEY_F1:
                toggleFlag (flag_wireframe);
                break;
            case GLUT_KEY_F2:
                toggleFlag (flag_raytracer);
                break;
            case GLUT_KEY_F3:
                toggleFlag (flag_coordinateSystem);
                break;
            case GLUT_KEY_F4:
                if (toggleFlag (flag_show_photons)) {
                    setFlag (flag_show_direct_photons);
                    setFlag (flag_show_indirect_photons);
                    setFlag (flag_show_caustic_photons);
                } else {
                    resetFlag (flag_show_direct_photons);
                    resetFlag (flag_show_indirect_photons);
                    resetFlag (flag_show_caustic_photons);
                }
                break;
                /* Vollbildmodus */
            case GLUT_KEY_F11:
                toggleFlag (flag_fullscreen);
                break;
            }
        }
    }
    glutPostRedisplay();
}

/**
 * Verarbeitung eines Mausereignisses.
 * Durch Bewegung der Maus bei gedrueckter Maustaste kann die aktuelle
 * Zeichenfarbe beeinflusst werden.
 * Falls Debugging aktiviert ist, wird jedes Mausereignis auf stdout
 * ausgegeben.
 * @param x x-Position des Mauszeigers zum Zeitpunkt der Ereignisausloesung.
 * @param y y-Position des Mauszeigers zum Zeitpunkt der Ereignisausloesung.
 * @param eventType Typ des Ereignisses.
 * @param button ausloesende Maustaste (nur bei Ereignissen vom Typ mouseButton).
 * @param buttonState Status der Maustaste (nur bei Ereignissen vom Typ mouseButton).
 */
static void handleMouseEvent(int x, int y, MouseEventType eventType, int button,
        int buttonState) {
    /* aktueller Status der beiden Maustasten */
    static int mouseButtonStates[2] = { GLUT_UP, GLUT_UP };

    /* x- und y-Position der Maus bei vorigem Schritt über die jeweilige
     Maustaste */
    static int oldMousePos[2][2] = { { 0, 0 }, { 0, 0 } };

    /* Veränderung der Mausposition */
    MouseOffset offset = { { 0.0f, 0.0f }, { 0.0f, 0.0f } };

    switch (eventType) {
    case mouseButton:
        switch (button) {
        case GLUT_LEFT_BUTTON:
            mouseButtonStates[0] = buttonState;
            /* Neue Mausposition merken */
            oldMousePos[0][0] = x;
            oldMousePos[0][1] = y;
            if (buttonState == GLUT_UP) {
                /* Offset zurücksetzen, wenn Maustaste losgelassen wurde */
                offset[0][0] = 0.0f;
                offset[0][1] = 0.0f;
            }
            break;
        case GLUT_RIGHT_BUTTON:
            mouseButtonStates[1] = buttonState;
            /* Neue Mausposition merken */
            oldMousePos[1][0] = x;
            oldMousePos[1][1] = y;
            if (buttonState == GLUT_UP) {
                /* Offset zurücksetzen, wenn Maustaste losgelassen wurde */
                offset[1][0] = 0.0f;
                offset[1][1] = 0.0f;
            }
            break;
        }
        break;
    case mouseMotion:
        if (mouseButtonStates[0] == GLUT_DOWN) {
            /* Bei Mausbewegung Offset berechnen */
            offset[0][0] = x - oldMousePos[0][0];
            oldMousePos[0][0] = x;
            offset[0][1] = y - oldMousePos[0][1];
            oldMousePos[0][1] = y;

            /* Mausbewegung in Relation zur Fenstergröße bringen */
            offset[0][0] /= (GLfloat) glutGet(GLUT_WINDOW_WIDTH);
            offset[0][1] /= (GLfloat) glutGet(GLUT_WINDOW_HEIGHT);
        }
        if (mouseButtonStates[1] == GLUT_DOWN) {
            /* Bei Mausbewegung Offset berechnen */
            offset[1][0] = x - oldMousePos[1][0];
            oldMousePos[1][0] = x;
            offset[1][1] = y - oldMousePos[1][1];
            oldMousePos[1][1] = y;

            /* Mausbewegung in Relation zur Fenstergröße bringen */
            offset[1][0] /= (GLfloat) glutGet(GLUT_WINDOW_WIDTH);
            offset[1][1] /= (GLfloat) glutGet(GLUT_WINDOW_HEIGHT);
        }

        /* Offset an Kamera übergeben */
        turnCamera(offset);

        /* Neuzeichnen anstossen */
        glutPostRedisplay();
        break;
    }
}

/**
 * Callback fuer Tastendruck.
 * Ruft Ereignisbehandlung fuer Tastaturereignis auf.
 * @param key betroffene Taste (In).
 * @param x x-Position der Maus zur Zeit des Tastendrucks (In).
 * @param y y-Position der Maus zur Zeit des Tastendrucks (In).
 */
static void cbKeyboard(unsigned char key, int x, int y) {
    handleKeyboardEvent(key, GLUT_DOWN, GL_FALSE, x, y);
}

/**
 * Callback fuer Druck auf Spezialtasten.
 * Ruft Ereignisbehandlung fuer Tastaturereignis auf.
 * @param key betroffene Taste (In).
 * @param x x-Position der Maus zur Zeit des Tastendrucks (In).
 * @param y y-Position der Maus zur Zeit des Tastendrucks (In).
 */
static void cbSpecial(int key, int x, int y) {
    handleKeyboardEvent(key, GLUT_DOWN, GL_TRUE, x, y);
}

/**
 * Mouse-Button-Callback.
 * @param button Taste, die den Callback ausgeloest hat.
 * @param state Status der Taste, die den Callback ausgeloest hat.
 * @param x X-Position des Mauszeigers beim Ausloesen des Callbacks.
 * @param y Y-Position des Mauszeigers beim Ausloesen des Callbacks.
 */
static void cbMouseButton(int button, int state, int x, int y) {
    handleMouseEvent(x, y, mouseButton, button, state);
}

/**
 * Mouse-Motion-Callback.
 * @param x X-Position des Mauszeigers.
 * @param y Y-Position des Mauszeigers.
 */
static void cbMouseMotion(int x, int y) {
    handleMouseEvent(x, y, mouseMotion, 0, 0);
}

/**
 * Callback fuer Aenderungen der Fenstergroesse.
 * Initiiert Anpassung der Projektionsmatrix an veränderte Fenstergroesse.
 * @param w Fensterbreite (In).
 * @param h Fensterhoehe (In).
 */
static void cbReshape(int w, int h) {
    /* Das ganze Fenster ist GL-Anzeigebereich */
    glViewport(0, 0, (GLsizei) w, (GLsizei) h);

    /* Anpassen der Projektionsmatrix an das Seitenverhältnis des Fensters */
    setProjection((GLdouble) w / (GLdouble) h);
}

/**
 * Zeichen-Callback.
 * Loescht die Buffer, ruft das Zeichnen der Szene auf und tauscht den Front-
 * und Backbuffer.
 */
void cbDisplay(const Scene& scene) {
    cbReshape(glutGet(GLUT_WINDOW_WIDTH), glutGet(GLUT_WINDOW_HEIGHT));
    /* Framebuffer und z-Buffer zuruecksetzen */
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    /* Nachfolgende Operationen beeinflussen Modelviewmatrix */
    glMatrixMode (GL_MODELVIEW);
    /* Matrix zuruecksetzen - Einheitsmatrix laden */
    glLoadIdentity();

    /* Kameraposition aus sphärischen Koordinaten in kartesische umrechnen
     * und setzen. */
    Vector3f pos = g_camera.getPos();
    Vector3f dir = g_camera.getDir();
    Vector3f up = g_camera.getUp();
    gluLookAt(pos[0], pos[1], pos[2], /* Position */
    dir[0] + pos[0], dir[1] + pos[1],
            dir[2] + pos[2], /* Look-At */
            up[0], up[1], up[2]); /* Up-Vektor */

    /* Szene zeichnen */
    drawScene(scene);

    /* Objekt anzeigen */
    glutSwapBuffers();
}

void displayScene(void) {
    if (getFlag (flag_raytracer)) {
        glDepthMask (GL_FALSE);
        glDisable (GL_DEPTH_TEST);
        PixelRenderer::display(sceneMgr.getScene(), g_config);
    } else {
        glDepthMask (GL_TRUE);
        glEnable (GL_DEPTH_TEST);
        cbDisplay(sceneMgr.getScene());
    }
}

/**
 * Registrierung der GLUT-Callback-Routinen.
 */
static void registerCallbacks(void) {
    /* Mouse-Button-Callback (wird ausgefuehrt, wenn eine Maustaste
     * gedrueckt oder losgelassen wird) */
    glutMouseFunc(cbMouseButton);

    /* Mouse-Motion-Callback (wird ausgefuehrt, wenn die Maus bewegt wird,
     * waehrend eine Maustaste gedrueckt wird) */
    glutMotionFunc(cbMouseMotion);

    /* Tasten-Druck-Callback - wird ausgefuehrt, wenn eine Taste gedrueckt wird */
    glutKeyboardFunc(cbKeyboard);

    /* Spezialtasten-Druck-Callback - wird ausgefuehrt, wenn Spezialtaste
     * (F1 - F12, Links, Rechts, Oben, Unten, Bild-Auf, Bild-Ab, Pos1, Ende oder
     * Einfuegen) gedrueckt wird */
    glutSpecialFunc(cbSpecial);

    /* Reshape-Callback - wird ausgefuehrt, wenn neu gezeichnet wird (z.B. nach
     * Erzeugen oder Groessenaenderungen des Fensters) */
    glutReshapeFunc(cbReshape);

    /* Display-Callback - wird an mehreren Stellen imlizit (z.B. im Anschluss an
     * Reshape-Callback) oder explizit (durch glutPostRedisplay) angestossen */
    glutDisplayFunc(displayScene);

    /* Eigene Callbacks */
    setCallback(flag_wireframe, handleWireframeMode);
    setCallback(flag_fullscreen, handleFullscreen);
}

// -------------------------------------------------------------------------
//                          Initialisierung
// -------------------------------------------------------------------------

/**
 * Initialisiert das Programm (inkl. I/O und OpenGL) und startet die
 * Ereignisbehandlung.
 * @param title Beschriftung des Fensters
 * @param width Breite des Fensters
 * @param height Hoehe des Fensters
 */
static void initIO() {

    initFlags();
    IO_INFO(("Initialisiere Szene...\n"));
    if (initScene(sceneMgr.getScene())) {
        IO_INFO(("...fertig.\n"));
        IO_INFO(("Registriere Callbacks...\n"));
        registerCallbacks();
        IO_INFO(("...fertig.\n\n"));
    } else {
        IO_INFO(("...fehlgeschlagen.\n"));
    }
}

/**
 * Initialisiert das Programm (inkl. I/O und OpenGL) und startet die
 * Ereignisbehandlung.
 * @param title Beschriftung des Fensters
 * @param width Breite des Fensters
 * @param height Hoehe des Fensters
 * @param config Konfiguration
 * @return ID des erzeugten Fensters, 0 im Fehlerfall
 */
int initAndStartIO(char const* title, int width, int height, Config config) {
    /* Kamera und Konfiguration zwischenspeichern */
    g_camera.loadCamera(config.CAMERA_SETTING);
    g_config = config;

    /* Szene initialisieren */
    sceneMgr = SceneManager(config);

    /* Kommandozeile immitieren */
    int argc = 0;

    /* Glut initialisieren */
    glutInit(&argc, 0);
    IO_INFO(("Erzeuge Fenster..."));

    /* Initialisieren des Fensters */
    /* RGB-Framebuffer und z-Buffer anfordern */
    glutInitDisplayMode(GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(width, height);
    glutInitWindowPosition((glutGet(GLUT_SCREEN_WIDTH) - width) / 2,
            (glutGet(GLUT_SCREEN_HEIGHT) - height) / 2);
    /* Fenster erzeugen */
    if (glutCreateWindow(title)) {
        initIO();
        glutMainLoop();
    } else {
        IO_INFO(("...fehlgeschlagen.\n"));
    }

    return 0;
}

} /* Namespace Momentum */


/*
 * ioTypes.hpp
 *
 *  Created on: May 13, 2015
 *      Author: timo
 */

#ifndef __IOTYPES_HPP__
#define __IOTYPES_HPP__

/** Faktoren der Kamerabewegung */
#define MOUSE_SENSITIVITY 90
#define MOVEMENT_SPEED 0.1f

/** Mindest- und Maximalradius der Kugel, auf der sich die Kamera bewegt */
#define MIN_RADIUS 2.0f
#define MAX_RADIUS 50.0f

#ifdef DEBUG
/** Gibt an, ob Debug-Ausgaben dieses Moduls angezeigt werden sollen */
#define IO_PRINT GL_FALSE
/** Makro zur Ausgabe von Debug-Informationen */
#define IO_INFO(ARGS) if (IO_PRINT) {INFO(ARGS);}
#else
#define IO_INFO(ARGS)
#endif

#endif /* __IOTYPES_HPP__ */

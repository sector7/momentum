#ifndef __IO_HPP__
#define __IO_HPP__
/**
 * @file
 * Schnittstelle des Ein-/Ausgabe-Moduls.
 * Das Modul kapselt die Ein- und Ausgabe-Funktionalität (insbesondere die GLUT-
 * Callbacks) des Programms.
 *
 * @author Ellen Schwartau (Minf9888) und Malte Jörgens (Minf9640)
 */

#include <commons.hpp>
#include <SceneManager.hpp>
#include <Scene.hpp>
#include <Camera.hpp>
#include <Config.hpp>

namespace momentum {

/**
 * Initialisiert das Programm (inkl. I/O und OpenGL) und startet die
 * Ereignisbehandlung.
 * @param title Beschriftung des Fensters
 * @param width Breite des Fensters
 * @param height Hoehe des Fensters
 * @param config Konfiguration
 * @return ID des erzeugten Fensters, 0 im Fehlerfall
 */
int initAndStartIO(char const* title, int width, int height, Config config);

/**
 * Zeichen-Callback.
 * Loescht die Buffer, ruft das Zeichnen der Szene auf und tauscht den Front-
 * und Backbuffer.
 */
void cbDisplay(Scene& scene);

/**
 * Liefert die Kamera-Einstellungen.
 * @return Kamera-Objekt
 */
Camera getCamera();

} /*namespace momentum*/

#endif

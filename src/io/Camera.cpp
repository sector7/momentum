/**
 * Klasse zur Repräsentation der Kamera.
 */
#include "Camera.hpp"
#include <debugGL.hpp>
#include <mathUtils.hpp>
#include <ioTypes.hpp>

#include <math.h>

namespace momentum {

/**
 * Konstruktorfunktion - initialisiert die Parameter der Kamera.
 * @param pos Position
 * @param dir Richtungsvektor
 * @param up Up-Vektor
 * @param right Right-Vektor
 * @param fov Field of View
 * @param horizontalRotation Winkel der horizontalen Rotation
 * @param verticalRotation Winkel der vertikalen Rotation
 * @param currentCameraType Typ der aktuell angezeigten Kamera
 */
Camera::Camera(Vector3f pos, Vector3f dir, Vector3f up, Vector3f right, GLfloat fov,
        GLfloat horizontalRotation, GLfloat verticalRotation, CameraType currentCameraType)
:
        pos(pos), dir(dir), up(up), right(right), fov(fov), horizontalRotation(horizontalRotation), verticalRotation(
                verticalRotation), currentCameraType(currentCameraType) {
}

/**
 * Erzeugt eine Kamera auf Grundlage der gegebenen Konfiguration.
 * @param settings Einstellungen
 */
Camera::Camera(CameraSetting settings) :
        Camera(settings.pos, settings.dir, settings.up, settings.right, settings.fov,
                settings.horizontalRotation,
                settings.verticalRotation, settings.cameraType) {
}

/**
 * Erzeugt die gewünschte Kamera.
 * @param type gewünschte Kamera Einstellung
 */
Camera::Camera(CameraType type) :
        Camera(this->cameras[type]) {
}

/**
 * Erzeugt die Default-Kamera.
 */
Camera::Camera() :
        Camera(CAM_GLOBAL) {
}

/**
 * Destruktorfunktion
 */
Camera::~Camera() {
}

// -------------------------------------------------------------------------
//                          Kamera-Rotation
// -------------------------------------------------------------------------
/**
 * Berechnet die aktuelle Rotation der Kamera ausgehend vom Maus-Offset.
 * @param offset Offset der Maus
 */
void Camera::calcRotation(MouseOffset offset) {
    /* Rotation um y-Achse, 0° und 360° ist dieselbe Blickrichtung */
    this->horizontalRotation += offset[0][0] * MOUSE_SENSITIVITY;
    if (this->horizontalRotation < 0.0f)
        this->horizontalRotation += 360.0f;
    if (this->horizontalRotation >= 360.0f)
        this->horizontalRotation -= 360.0f;

    /* Höhenrotation: 90° entspricht Nordpol, -90° entspricht Südpol */
    this->verticalRotation += offset[0][1] * MOUSE_SENSITIVITY;
    if (this->verticalRotation > 90.0f)
        this->verticalRotation = 90.0f;
    if (this->verticalRotation < -90.0f)
        this->verticalRotation = -90.0f;
}

/**
 * Rotiert die Blickrichtung der Kamera.
 */
void Camera::rotateDirVector() {
    /**this->dir = {
     sinf(DEG2RAD(this->horizontalRotation)) * cosf(DEG2RAD(this->verticalRotation)),
     sinf(DEG2RAD(this->verticalRotation)),
     cosf(DEG2RAD(this->verticalRotation)) * cos(DEG2RAD(this->horizontalRotation))
     };*/
    this->dir = calcDirection(this->verticalRotation, this->horizontalRotation);
    // Normieren auf Länge 1
    gte::Normalize(this->dir);
}

/**
 * Berechnet den normierten Up-Vector der Kamera ausgehen von Blickrichtung und Right-Vektor
 */
void Camera::calcUpVector() {
    this->up = gte::Cross(this->right, this->dir);
    // Normieren auf Länge 1
    gte::Normalize(this->up);
}

/**
 * Rotiert den Right-Vektor der Kamera.
 */
void Camera::rotateRightVector() {
    this->right = {
        (-cosf(DEG2RAD(this->horizontalRotation))),
        0.0f,
        sinf(DEG2RAD(this->horizontalRotation))
    };
    // Normieren auf Länge 1
    gte::Normalize(this->right);
}

/**
 * Rotiert Blickrichtungs-, Up- und Right-Vektor der Kamera.
 */
void Camera::rotateVectors() {
    // Blickrichtung rotieren
    rotateDirVector();
    // Right-Vektor rotieren
    rotateRightVector();
    // Up-Vektor rotieren
    calcUpVector();
}

/**
 * Rotiert die Blickrichtung sowie Up- und Right-Vektor der Kamera.
 * @param offset Bewegungsvektoren der Maus für jeweils Links- und
 * Rechtsklick.
 */
void Camera::turnCamera(MouseOffset offset) {
    // Mausbewegung in Rotation umwandeln
    calcRotation(offset);
    // Up-, Right- und Blickrichtungs-Vektor rotieren
    rotateVectors();

    // DEBUG INFO
    IO_INFO(("Vertical Rotation: %f\n", this->verticalRotation));
    IO_INFO(("Horizontale Rotation: %f\n", this->horizontalRotation));
    IO_INFO(("Direction: %f,%f, %f \n", this->dir[0], this->dir[1], this->dir[2]));
    IO_INFO(("Up-Vektor: %f,%f, %f \n", this->up[0], this->up[1], this->up[2]));
    IO_INFO(
            ("Right-Vektor: %f,%f, %f \n", this->right[0], this->right[1], this->right[2]));
    IO_INFO(("Kreuzprodukt Dir und Right Kamera: %f\n", gte::Dot(this->right, this->dir)));
}

/**
 * Berechnet neue Position der Kamera.
 * @param dir Bewegungsrichtung
 */
void Camera::moveCamera(Directions dir) {
    switch (dir) {
    case dirUp:
        this->pos += this->dir * MOVEMENT_SPEED;
        break;
    case dirRight:
        this->pos += this->right * MOVEMENT_SPEED;
        break;
    case dirDown:
        this->pos -= this->dir * MOVEMENT_SPEED;
        break;
    case dirLeft:
        this->pos -= this->right * MOVEMENT_SPEED;
        break;
    }

    IO_INFO(("New Pos: %f, %f, %f\n", this->pos[0], this->pos[1], this->pos[2]));
}

/**
 * Schlatet die Kamera-Einstellung weiter.
 */
void Camera::switchCamera() {
    this->currentCameraType =
            this->currentCameraType == (CAMERA_COUNT - 1) ?
                    static_cast<CameraType>(0) :
                    static_cast<CameraType>(this->currentCameraType + 1);
    this->loadCamera(this->currentCameraType);
}

/**
 * Lädt die Einstellungen für eine Kamers
 * @param cameraType gewünschte Kameraeinstellung
 */
void Camera::loadCamera(CameraType cameraType) {
    this->pos = this->cameras[cameraType].pos;
    this->dir = this->cameras[cameraType].dir;
    this->fov = this->cameras[cameraType].fov;
    this->right = this->cameras[cameraType].right;
    this->up = this->cameras[cameraType].up;
    this->horizontalRotation = this->cameras[cameraType].horizontalRotation;
    this->verticalRotation = this->cameras[cameraType].verticalRotation;
}

// -------------------------------------------------------------------------
//                          Setter / Getter
// -------------------------------------------------------------------------

/**
 * Liefert den Richtungsvektor.
 * @return Richtungsvektor
 */
const Vector3f& Camera::getDir() const {
    return dir;
}

/**
 * Liefert den Field of View
 * @return Field of view
 */
GLfloat Camera::getFov() const {
    return fov;
}

/**
 * Liefert die Kamerapostition
 * @return Kameraposition
 */
const Vector3f& Camera::getPos() const {
    return pos;
}

/**
 * Liefert den Rechts-Vektor
 * @return Right-Vektor
 */
const Vector3f& Camera::getRight() const {
    return right;
}

/**
 * Liefert den Up-Vektor
 * @return Up-Vektor
 */
const Vector3f& Camera::getUp() const {
    return up;
}

} // namespace momentum

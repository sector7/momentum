#ifndef CAMERA_HPP_
#define CAMERA_HPP_

#include <commons.hpp>

namespace momentum {

/**
 * Klasse zur Repräsentation der Kamera.
 */
class Camera {
public:
    /**
     * Konstruktorfunktion - initialisiert die Parameter der Kamera.
     * @param pos Position
     * @param dir Richtungsvektor
     * @param up Up-Vektor
     * @param right Right-Vektor
     * @param fov Field of View
     * @param horizontalRotation Winkel der horizontalen Rotation
     * @param verticalRotation Winkel der vertikalen Rotation
     * @param currentCameraType Typ der aktuell angezeigten Kamera
     */
    Camera(Vector3f pos, Vector3f dir, Vector3f up, Vector3f right, GLfloat fov,
            GLfloat horizontalRotation, GLfloat verticalRotation, CameraType currentCameraType);

    /**
     * Erzeugt eine Kamera auf Grundlage der gegebenen Konfiguration.
     * @param settings Einstellungen
     */
    Camera(CameraSetting settings);

    /**
     * Erzeugt die gewünschte Kamera.
     * @param type gewünschte Kamera Einstellung
     */
    Camera(CameraType type);

    /**
     * Erzeugt die Default-Kamera.
     */
    Camera();

    /**
     * Destruktorfunktion
     */
    virtual ~Camera();

    /**
     * Dreht die Kamera auf Grundlage der Mousbewegung.
     * @param offset Mausbewegung
     */
    void turnCamera(MouseOffset offset);

    /**
     * Liefert den Richtungsvektor.
     * @return Richtungsvektor
     */
    const Vector3f& getDir() const;

    /**
     * Liefert die Kamerapostition
     * @return Kameraposition
     */
    const Vector3f& getPos() const;

    /**
     * Liefert den Rechts-Vektor
     * @return Right-Vektor
     */
    const Vector3f& getRight() const;

    /**
     * Liefert den Up-Vektor
     * @return Up-Vektor
     */
    const Vector3f& getUp() const;

    /**
     * Liefert den Field Of View.
     * @return Field of View
     */
    GLfloat getFov() const;

    /**
     * Lädt die Einstellungen für eine Kamers
     * @param cameraType gewünschte Kameraeinstellung
     */
    void loadCamera(CameraType cameraType);

    /**
     * Berechnet neue Position der Kamera.
     * @param dir Bewegungsrichtung
     */
    void moveCamera(Directions dir);

    /**
     * Schlatet die Kamera-Einstellung weiter.
     */
    void switchCamera();

private:
    /**
     * Vordefinierte Kamera-Einstellungen
     */
    CameraSetting cameras[CAMERA_COUNT] = {
            // Globale Einstellung
            { { -4.834435, 1.763789, -5.686691 }, // Position
                    { 0.539543, -0.531178, 0.653256 }, // Blickrichtung
                    { 0.338259, 0.847260, 0.409550 }, // Up-Vektor
                    { -0.771022, 0.000000, 0.636809 }, // Rechts
                    80.0f, // Öffnungswinkel
                    39.554272, // Rotation um y-Achse
                    -32.085098, // Höhenrotation
                    CAM_GLOBAL }, // Typ der Kamera
            // Position zur Betrachtung der Kaustiken
            { { 1.430875, 1.000000, -7.591468 }, // Position
                    { 0.075295, -0.384860, 0.919899 }, // Blickrichtung
                    { 0.031396, 0.922975, 0.383577 }, // Up-Vektor
                    { -0.996667, 0.000000, 0.081579 }, // Rechts
                    80.0f, // Öffnungswinkel
                    4.679306, // Rotation um y-Achse
                    -22.635021, // Höhenrotation
                    CAM_SPHERES }, // Typ der Kamera
            // Spiegelnde Kugel
            { { -1.079728, 0.360160, 1.565693 }, // Position
                    { 0.981589, -0.166511, 0.093576 }, // Blickrichtung
                    { 0.165759, 0.986040, 0.015802 }, // Up-Vektor
                    { -0.094901, 0.000000, 0.995487 }, // Rechts
                    80.0f, // Öffnungswinkel
                    84.554375, // Rotation um y-Achse
                    -9.585011, // Höhenrotation
                    CAM_SPECULAR },
            // Farbausblutung
            { { -4.518685, 1.167606, 3.995943 }, // Position
                    { 0.677628,-0.596016, 0.430796 }, // Blickrichtung
                    { 0.502978,0.802972, 0.319764 }, // Up-Vektor
                    { -0.536502,0.000000, 0.843899 }, // Rechts
                    80.0f, // Öffnungswinkel
                    57.554203, // Rotation um y-Achse
                    -36.585098, // Höhenrotation
                    CAM_COLOR },
            // Glas-Objekt
            { { -0.824009, -0.763254, -2.150626 }, // Position
                    { -0.940584, -0.241074, 0.239136 }, // Blickrichtung
                    { -0.233641, 0.970507, 0.059401 }, // Up-Vektor
                    { -0.246403, 0.000000, -0.969168 }, // Rechts
                    80.0f, // Öffnungswinkel
                    284.264771, // Rotation um y-Achse
                    -13.949935, //  Höhenrotation
                    CAM_CAUSTIC_SPHERE },
            // Farbausblutung
            { { -4.464027, 0.036364, -5.411908 }, // Position
                    { 0.846653, -0.379053, -0.373493 }, // Blickrichtung
                    { 0.346807, 0.925375, -0.152991 }, // Up-Vektor
                    { 0.403613, 0.000000, 0.914930 }, // Rechts
                    80.0f, // Öffnungswinkel
                    113.804245, // Rotation um y-Achse
                    -22.275028, // Höhenrotation
                    CAM_CAUSTIC_CUBE }
    };

    /** Position */
    Vector3f pos;

    /** Blickrichtung */
    Vector3f dir;

    /** Up-Vektor */
    Vector3f up;

    /** Right-Vektor */
    Vector3f right;

    /** field of view */
    GLfloat fov;

    /** Winkel der horizontalen Rotation */
    GLfloat horizontalRotation;

    /** Winkel der vertikalen Rotation */
    GLfloat verticalRotation;

    /**
     * Aktuell eingestellte Kamera-Konfiguration.
     */
    CameraType currentCameraType;

    /**
     * Berechnet die aktuelle Rotation der Kamera ausgehend vom Maus-Offset.
     * @param offset Offset der Maus
     */
    void calcRotation(MouseOffset offset);

    /**
     * Rotiert die Blickrichtung der Kamera.
     */
    void rotateDirVector();

    /**
     * Berechnet den normierten Up-Vector der Kamera ausgehen von Blickrichtung und Right-Vektor
     */
    void calcUpVector();

    /**
     * Rotiert den Right-Vektor der Kamera.
     */
    void rotateRightVector();

    /**
     * Rotiert Blickrichtungs-, Up- und Right-Vektor der Kamera.
     */
    void rotateVectors();

}
;

} // namespace momentum

#endif /* CAMERA_HPP_ */

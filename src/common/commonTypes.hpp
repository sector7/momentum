#ifndef __COMMON_TYPES_HPP__
#define __COMMON_TYPES_HPP__
/**
 * @file
 * Typenschnittstelle.
 * Das Modul kapselt die Typdefinitionen des Programms.
 *
 * @author Ellen Schwartau (Minf9888) und Malte Jörgens (Minf9640)
 */

/* ---- System Header einbinden ---- */
#include <GL/gl.h>
#include <GteVector3.h>
#include <GteVector4.h>
#include <GteRay.h>

namespace momentum {

/* ---- Typedeklarationen ---- */
/** Vektor im 3D-Raum */
typedef gte::Vector3<GLfloat> Vector3f;
typedef gte::Vector4<GLfloat> Vector4f;

/** Punkt im Raum */
typedef Vector3f Point3f;

/** Strahlen */
typedef gte::Ray3<GLfloat> Ray3f;

/** RGB-Farbwert */
typedef Vector3f Color3f;

/** RGBA-Farbwert */
typedef GLfloat Color4f[4];

/** Maus-Offset */
typedef GLfloat MouseOffset[2][2];

/** Datentyp fuer Mausereignisse. */
typedef enum e_MouseEventType {
    mouseButton, mouseMotion
} MouseEventType;

/** Datentyp für Kameraposition. */
typedef enum e_cameraSettings {
    CAM_GLOBAL,
    CAM_SPHERES,
    CAM_SPECULAR,
    CAM_COLOR,
    CAM_CAUSTIC_CUBE,
    CAM_CAUSTIC_SPHERE,
    CAMERA_COUNT
} CameraType;

typedef struct {
    Vector3f pos;
    Vector3f dir;
    Vector3f up;
    Vector3f right;
    GLfloat fov;
    GLfloat horizontalRotation;
    GLfloat verticalRotation;
    CameraType cameraType;
} CameraSetting;

typedef enum e_directions {
    dirUp, dirRight, dirDown, dirLeft
} Directions;

/** Funktionszeiger zum Aufruf nach bestimmter Flag-Änderung. */
typedef void (*FlagCallback)(GLboolean);

}
#endif

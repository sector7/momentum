/*
 * commons.hpp
 *
 *  Created on: May 13, 2015
 *      Author: timo
 */

#ifndef COMMONS_HPP_
#define COMMONS_HPP_

#include <mathUtils.hpp>
#include <commonTypes.hpp>
#include <commonConstants.hpp>

#endif /* COMMONS_HPP_ */

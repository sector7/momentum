#ifndef __COMMON_CONSTANTS_HPP__
#define __COMMON_CONSTANTS_HPP__
/**
 * @file
 * Diese Datei beinhaltet übungsübergreifende, allgemeine Konstanten.
 *
 * @author Ellen Schwartau (Minf9888) und Malte Jörgens (Minf9640)
 */

#include <math.h> // M_PI and others

#define THREAD_COUNT 10
#define THREAD_BLOCKSIZE 1

#define INITIAL_WINDOW_WIDTH 800
#define INITIAL_WINDOW_HEIGHT 600
#define ZNEAR 10.0

/** Hilfetext */
#define USAGE_LINE_COUNT 16
#define USAGE_LINES {\
        "Hilfe:",\
        "[F1]:                Wireframe Modus",\
        "[F2]:                Aktuelle Ansicht rendern",\
		"[F3]:                Einblenden des Koordinatensystems",\
		"[F4]:                Einblenden propagierter Photonen",\
		"[F11]:               Umschaltung Vollbild",\
		"",\
		"[1]:                 Einblendung direkte Photonen",\
		"[2]:                 Einblendung indirekter Photonen",\
		"[3]:                 Einblendung spekulare Photonen",\
		"",\
		"[C]:                 Durchwechseln vorgegebener Ansichten",\
		"[L]:                 Umschaltung direkte Beluchtung",\
		"[W], [A], [S], [D]:  Kamerabewegung",\
		"[Q], [Esc]:          Beenden",\
		"Linke Maustaste:     Blickwinkel Rotation" \
    }

#endif

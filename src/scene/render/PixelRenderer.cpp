#include <GL/gl.h>
#include <GL/glext.h>
#include <GL/glut.h>
#include <pngwriter.h>

#include <debugGL.hpp>
#include <io.hpp>
#include <PixelRenderer.hpp>
#include <iostream>
#include <Camera.hpp>

/** Renderaufgabe */
typedef struct s_job {
    GLuint firstLine;
    GLuint lastLine;
} Job;

/** Liste mit Renderaufgaben */
typedef std::list<Job> JobList;

/** Liste der Renderthreads */
typedef std::list<std::thread> ThreadList;

namespace momentum {
/**
 * Konstruktorfunktion
 */
PixelRenderer::PixelRenderer() {
}

/**
 * Destruktorfunktion
 */
PixelRenderer::~PixelRenderer() {
}

/**
 * Funktion zur Darstellung der Szene.
 * @param scene Szene
 * @param config Konfiguration
 */
void PixelRenderer::display(const Scene& scene, Config config) {
    Camera camera = getCamera();
    // Aktuelle Größe auslesen
    GLint windowWidth = glutGet(GLUT_WINDOW_WIDTH);
    GLint windowHeight = glutGet(GLUT_WINDOW_HEIGHT);
    pngwriter png(windowWidth, windowHeight, 0, "out/test.png");
    Raytracer* raytracer = new Raytracer(camera, windowWidth, windowHeight, config.MULTISAMPLING,
            config.ANTIALIASING, scene);

    GLfloat* pixelBuffer = new GLfloat[windowWidth * windowHeight * 3]; // Pixelbuffer initialisiern
    ThreadList threadList; // Threadliste
    JobList pendingJobs; // Liste mit anstehenden Renderaufgaben
    JobList runningJobs; // Liste mit aktuellen Renderaufgaben

    Job j;
    for (GLint i = 0; i < windowHeight; i += THREAD_BLOCKSIZE) {
        j.firstLine = i;
        j.lastLine = MIN(i + THREAD_BLOCKSIZE, windowHeight);
        pendingJobs.push_back(j);
    }

    // So lange es noch Arbeit gibt, starte Threads
    while (!pendingJobs.empty()) {
        // Alle Threads starten
        for (int t = 0; t < THREAD_COUNT; t++) {
            if (!pendingJobs.empty()) {
                threadList.push_back(
                        std::thread(&Raytracer::getPixelColorsForScene,
                                raytracer,
                                pixelBuffer,
                                windowWidth,
                                pendingJobs.front().firstLine,
                                pendingJobs.front().lastLine));
                pendingJobs.pop_front();
            }
        }

        // Auf alle Threads warten
        for (std::thread& t : threadList)
            t.join();

        // Zwischenergebnis zeichnen
        GLint rasterPos[2];
        glGetIntegerv(GL_CURRENT_RASTER_POSITION, rasterPos);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Pixel in den Framebuffer schreiben
        glMatrixMode (GL_PROJECTION);
        glLoadIdentity();
        glMatrixMode (GL_MODELVIEW);
        glLoadIdentity();

        glRasterPos2i(-1, -1);
        glDrawPixels(windowWidth, windowHeight, GL_RGB, GL_FLOAT, pixelBuffer);
        glutSwapBuffers();

        for (GLint y = 0; y < windowHeight; y++)
            for (GLint x = 0; x < windowWidth; x++)
                png.plot(x, y, pixelBuffer[(y * windowWidth + x) * 3],
                        pixelBuffer[(y * windowWidth + x) * 3 + 1],
                        pixelBuffer[(y * windowWidth + x) * 3 + 2]);

        // Threadliste aufräumen
        threadList.clear();

    }
    png.close();

    // Speicher nach Fertigstellung des Bildes aufräumen
    delete raytracer;
    free(pixelBuffer);

}

}

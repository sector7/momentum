#ifndef __DRAW_PIXEL_EXAMPLE_HPP__
#define __DRAW_PIXEL_EXAMPLE_HPP__

#include <Raytracer.hpp>
#include <GL/glut.h>
#include <thread>
#include <list>
#include <Config.hpp>

namespace momentum {

/**
 * Rendert Pixel in einer bestimmten Farbe.
 */
class PixelRenderer {

public:
    /**
     * Konstruktorfunktion
     */
    PixelRenderer();

    /**
     * Destruktorfunktion
     */
    virtual ~PixelRenderer();

    /**
     * Funktion zur Darstellung der Szene.
     * @param scene Szene
     * @param config Konfiguration
     */
    static void display(const Scene& scene, Config config);

};
}
#endif

#ifndef SCENE_HPP_
#define SCENE_HPP_

#include <sceneTypes.hpp>
#include <photonAndRayTypes.hpp>
#include <SceneObject.hpp>
#include <Light.hpp>
#include <PhotonMap.hpp>
#include <PhotonMapManager.hpp>

#include <list>

namespace momentum {

/** Intersection Ergebnis */
typedef struct {
    Ray3f ray;
    GLfloat distance;
    SceneObject* object;
    PropagationType propagationType;
} IntersectionResult;

/** Liste an Schnitt-Ergebnissen, die beim Propagieren von Strahlen entstehen */
typedef std::vector<IntersectionResult> IntersectionList;

/** Liste der Szenenobjekte */
typedef std::list<SceneObject*> SceneObjectList;

/** Liste der Lichter */
typedef std::list<Light*> LightList;

/**
 * Klasse zur Repräsentation einer Szene, bestehend aus Szenenobjekten und Lichtern.
 */
class Scene {

private:
    /** Liste der Szenenobjekte */
    SceneObjectList objects;
    /** Liste der Lichtquellen */
    LightList lights;
    /** Verwaltung der PhotonMaps */
    PhotonMapManager* photonMaps;

public:
    /**
     * Konstruktorfunktion
     */
    Scene();
    /**
     * Destruktorfunktion
     */
    virtual ~Scene();

    /**
     * Liefert die Liste der zu zeichnenden Szenenobjekte.
     * @return Liste der Szenenobjekte.
     */
    const SceneObjectList& getObjects() const;

    /**
     * Liefert die Liste der in der Szene befindlichen Lichtquellen.
     * @return Liste der Lichtquellen.
     */
    const LightList& getLights() const;

    /**
     * Methode zum Hinzufügen eines Objektes zu der Szene.
     * @param obj Hinzuzufügendes Objekt
     * @return Diese Instanz
     */
    Scene* addObject(SceneObject* obj);

    /**
     * Fügt ein neues Licht zur Liste der Lichtquellen hinzu.
     * @param light Lichtquelle
     * @return Diese Instanz
     */
    Scene* addLight(Light* light);

    /**
     * Liefert die PhotonMap der Szene.
     * @return PhotonMap
     */
    const PhotonMapManager& getPhotonMaps() const;

    /**
     * Setzt den PhotonMapManager der Szene.
     * @param photonMapManager
     */
    void setPhotonMapManager(PhotonMapManager* photonMapManager);

};

}

#endif /* SCENE_HPP_ */

/*
 * sceneObject.cpp
 *
 *  Created on: May 27, 2015
 *      Author: timo
 */
#include "SceneObject.hpp"

namespace momentum {
/**
 * Konstruktor. Da diese Klasse virtuell ist, kann sie nicht direkt instaziiert werden.
 * @param t Typ des Szenenobjekts
 * @param color Farbe des Scenenobjekts
 */
SceneObject::SceneObject(ObjectType type, Material material) {
    this->type = type;
    this->material = material;
}

/**
 * Destruktor.
 */
SceneObject::~SceneObject() {
}

/**
 * Gibt den Typ des Szenenobjekts zurück.
 * @return Typ
 */
ObjectType SceneObject::getType() {
    return this->type;
}

/**
 * Liefert das Material des Szenenobjekts.
 * @return Farbe
 */
Material SceneObject::getMaterial() {
    return this->material;
}
}

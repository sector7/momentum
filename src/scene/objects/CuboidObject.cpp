/*
 * CuboidObject.cpp
 *
 *  Created on: Jun 3, 2015
 *      Author: timo
 */

#include <CuboidObject.hpp>
#include <debugGL.hpp>

namespace momentum {
/**
 * Konstruktorfunktion zu Erstellung eines Quaders.
 * @param min minimaler Vertex
 * @param max maximaler Vertex
 * @param color Farbe des Scenenobjekts
 */
CuboidObject::CuboidObject(Vector3f min, Vector3f max, Material material) :
        SceneObject(oCuboid, material), object(AlignedBox3f(min, max)),
                extent(Vector3f( { max[0] - min[0], max[1] - min[1], max[2] - min[2] })) {
}

/**
 * Destruktorfunktion
 */
CuboidObject::~CuboidObject() {
}

/**
 * Liefert die Ecke des Quaders mit den kleinsten Koordinaten
 * @return minimaler Vertex
 */
Vector3f CuboidObject::getMinVertex() {
    return this->object.min;
}

/**
 * Liefert die Ecke des Quaders mit den größten Koordinaten
 * @return maximaler Vertex
 */
Vector3f CuboidObject::getMaxVertex() {
    return this->object.max;
}

/**
 * Liefert die Information, ob ein Strahl das Objekt schneidet.
 * @param ray Strahl
 * @return bool
 */
bool CuboidObject::intersect(const Ray3f& ray) {
    BoxIntersectionQueryResult r = BoxIntersectionQuery()(ray, this->object);
    return r.intersect;
}

/**
 * Liefert den Abstand zum dichtesten Punkt, in dem sich Strahl und Objekt schneiden.
 * Liefert -1 wenn kein Schnittpunkt existiert.
 * @param ray Strahl
 * @return Abstand
 */
GLfloat CuboidObject::getIntersectDistance(const Ray3f& ray) {
    GLfloat res = -1.0f;
    // Berechne Ergebnisobjekt mit Test, Anzahl und Position der Schnittpunkte
    BoxFindIntersectionQueryResult r = BoxFindIntersectionQuery()(ray, this->object);

    // Wenn Schnittpunkt existert, finde den nächst-gelegenen
    if (r.intersect) {
        res = r.lineParameter[0];
        if (r.numPoints > 1 && r.lineParameter[1] < res)
            res = r.lineParameter[1];
    }

    return res;
}

/**
 * Liefert den dichtesten Punkt in Strahlrichtung ausgehend vom Definitionspunkt des Strahls
 * in dem sich Strahl und Objekt schneiden.
 * Liefert NULL wenn kein Schnittpunkt existiert.
 * @param ray Strahl
 * @return Schnittpunkt
 */
Vector3f* CuboidObject::createIntersectionPoint(const Ray3f& ray) {
    BoxFindIntersectionQueryResult r = BoxFindIntersectionQuery()(ray, this->object);
    return r.intersect ? new Vector3f(r.point[0]) : nullptr;
}

/**
 * Liefert die Normale an einem Punkt, von dem angenommen wird, dass er sich auf der Oberfläche
 * des Objekts befindet.
 * @param point Punkt auf der Oberfläche des Objekts
 * @return Normalenvektor des Objekts am Punkt
 */
Vector3f CuboidObject::getNormalAt(const Vector3f& point) {
    /** Koordinatensystem */
    const static Vector3f COORDINATE_SYSTEM[3] = {
            { 1.0f, 0.0f, 0.0f },
            { 0.0f, 1.0f, 0.0f },
            { 0.0f, 0.0f, 1.0f } };
    GLfloat minDistance = INFINITY;
    Vector3f normal, boxCenter, boxExtent;
    this->object.GetCenteredForm(boxCenter, boxExtent);

    // Verschiebung des Punktes in Koordinatensystem der Box
    Point3f centeredPoint = point - boxCenter;

    for (GLint i = X; i <= Z; i++) {
        GLfloat distance = abs(boxExtent[i] - abs(centeredPoint[i]));
        if (distance < minDistance) {
            minDistance = distance;
            normal = copysignf(1.0f, centeredPoint[i]) * COORDINATE_SYSTEM[i];
        }
    }
#ifdef DEBUG
    if (gte::Length(normal) == 0.0f) {
        INFO(("Nullvektor an Punkt (%.2f|%.2f|%.2f) von Objekt (%.2f|%.2f|%.2f)",
                        point[0], point[1], point[2], boxCenter[0], boxCenter[1], boxCenter[2]));
    }
#endif
    return normal;
}

/**
 * Zeichnet den Würfel in OpenGL.
 */
void CuboidObject::draw(void) {
    glPushMatrix(); {
        // Würfel verschieben und skalieren
        glTranslatef(this->object.min[0] + extent[0] / 2.0f,
                this->object.min[1] + extent[1] / 2.0f,
                this->object.min[2] + extent[2] / 2.0f);
        glScalef(extent[0], extent[1], extent[2]);
        // Würfel zeichnen
        drawUnityCuboid();
    }
    glPopMatrix();
}

/**
 * Zeichnet ein Quadrat mit der Kantenlaenge 1, das aus mehreren kleineren
 * Quadraten zusammen gesetzt ist.
 */
void CuboidObject::drawUnitySquare() {
    int x, y;
    GLfloat normVect[3] = { 0.0f, 0.0f, 1.0f };
    for (y = 0; y < CUBOID_ACCURACY + 1; y++) {
        glBegin (GL_QUAD_STRIP);
        glNormal3fv(normVect);
        for (x = 0; x <= CUBOID_ACCURACY + 1; x++) {
            glVertex3f(-0.5f + x / (CUBOID_ACCURACY + 1.0f),
                    0.5f - y / (CUBOID_ACCURACY + 1.0f),
                    0.0f);
            glVertex3f(-0.5f + x / (CUBOID_ACCURACY + 1.0f),
                    0.5f - (y + 1) / (CUBOID_ACCURACY + 1.0f),
                    0.0f);
        }
        glEnd();
    }
}

/**
 * Zeichnet einen Einheitswürfel.
 */
void CuboidObject::drawUnityCuboid() {
    /* Frontflaeche */
    glPushMatrix();
    glTranslatef(0.0f, 0.0f, 0.5f);
    drawUnitySquare();
    glPopMatrix();

    /* rechte Seitenflaeche */
    glPushMatrix();
    glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
    glTranslatef(0.0f, 0.0f, 0.5f);
    drawUnitySquare();
    glPopMatrix();

    /* Rueckseitenflaeche */
    glPushMatrix();
    glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
    glTranslatef(0.0f, 0.0f, 0.5f);
    drawUnitySquare();
    glPopMatrix();

    /* linke Seitenflaeche */
    glPushMatrix();
    glRotatef(270.0f, 0.0f, 1.0f, 0.0f);
    glTranslatef(0.0f, 0.0f, 0.5f);
    drawUnitySquare();
    glPopMatrix();

    /* Deckelflaeche */
    glPushMatrix();
    glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
    glTranslatef(0.0f, 0.0f, 0.5f);
    drawUnitySquare();
    glPopMatrix();

    /* Bodenflaeche */
    glPushMatrix();
    glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
    glTranslatef(0.0f, 0.0f, 0.5f);
    drawUnitySquare();
    glPopMatrix();
}

}

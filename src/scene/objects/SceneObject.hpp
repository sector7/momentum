/*
 * SceneObject.hpp
 *
 *  Created on: May 27, 2015
 *      Author: timo
 */

#ifndef SCENEOBJECT_HPP_
#define SCENEOBJECT_HPP_

#include <commons.hpp>
#include <GteVector3.h>
#include <GteRay.h>
#include <vector>
#include <GL/glut.h>

#include <sceneTypes.hpp>

namespace momentum {

/**
 * Abstrakte Klasse zur Repräsentation von Scenen-Objekten.
 */
class SceneObject {
protected:
    /** Typ des Objektes */
    ObjectType type;
    /** Farbe des Objektes */
    Material material;

public:
    /**
     * Konstruktor. Da diese Klasse virtuell ist, kann sie nicht direkt instaziiert werden.
     * @param type Typ des Scenenobjekts
     * @param color Farbe des Scenenobjekts
     */
    SceneObject(ObjectType type, Material material);

    /**
     * Destruktor.
     */
    virtual ~SceneObject();

    /**
     * Gibt den Typ des Szenenobjekts zurück.
     * @return Typ
     */
    ObjectType getType();

    /**
     * Liefert das Material des Szenenobjekts.
     * @return Farbe
     */
    Material getMaterial();

    /**
     * Liefert die Information, ob ein Strahl das Objekt schneidet.
     * @param ray Strahl
     * @return bool
     */
    virtual bool intersect(const Ray3f& ray) = 0;

    /**
     * Liefert den Abstand zum dichtesten Punkt, in dem sich Strahl und Objekt schneiden.
     * Liefert -1 wenn kein Schnittpunkt existiert.
     * @param ray Strahl
     * @return Abstand
     */
    virtual GLfloat getIntersectDistance(const Ray3f& ray) = 0;

    /**
     * Liefert den dichtesten Punkt in Strahlrichtung ausgehend vom Definitionspunkt des Strahls
     * in dem sich Strahl und Objekt schneiden.
     * Liefert NULL wenn kein Schnittpunkt existiert.
     * @param ray Strahl
     * @return Schnittpunkt
     */
    virtual Vector3f* createIntersectionPoint(const Ray3f& ray) = 0;

    /**
     * Liefert die Normale an einem Punkt, von dem angenommen wird, dass er sich auf der Oberfläche
     * des Objekts befindet.
     * @param point Punkt auf der Oberfläche des Objekts
     * @return Normalenvektor des Objekts am Punkt
     */

    virtual Vector3f getNormalAt(const Vector3f& point) = 0;

    /**
     * Zeichnet das Objekt in OpenGL.
     */
    virtual void draw(void) = 0;
};

} /* namespace momentum */

#endif /* SCENEOBJECT_HPP_ */

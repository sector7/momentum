#ifndef __OBJECTTYPES_HPP__
#define __OBJECTTYPES_HPP__

#include <commonTypes.hpp>
#include <Photon.hpp>
#include <mathUtils.hpp>

#include <stdio.h>
#include <list>
#include <array>
#include <limits.h>

#include <GteIntrRay3AlignedBox3.h>
#include <GteIntrRay3Sphere3.h>
#include <GteNearestNeighborQuery.h>

using namespace std;

namespace momentum {

/** Objekt Klassen */
typedef gte::AlignedBox3<float> AlignedBox3f;
typedef gte::Sphere3<float> Sphere3f;

/** Queries */
// Box
typedef gte::TIQuery<GLfloat, Ray3f, AlignedBox3f> BoxIntersectionQuery;
typedef BoxIntersectionQuery::Result BoxIntersectionQueryResult;
typedef gte::FIQuery<GLfloat, Ray3f, AlignedBox3f> BoxFindIntersectionQuery;
typedef BoxFindIntersectionQuery::Result BoxFindIntersectionQueryResult;

// Spheres
typedef gte::TIQuery<GLfloat, Ray3f, Sphere3f> SphereIntersectionQuery;
typedef SphereIntersectionQuery::Result SphereIntersectionQueryResult;
typedef gte::FIQuery<GLfloat, Ray3f, Sphere3f> SphereFindIntersectionQuery;
typedef SphereFindIntersectionQuery::Result SphereFindIntersectionQueryResult;

}
#endif /* __OBJECTTYPES_HPP__ */

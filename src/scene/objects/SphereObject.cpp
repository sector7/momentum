/*
 * SphereObject.cpp
 *
 *  Created on: Jun 24, 2015
 *      Author: timo
 */

#include <SphereObject.hpp>
#include <objectTypes.hpp>

namespace momentum {

GLUquadric* SphereObject::quad = gluNewQuadric(); // Speichermangel wird nicht abgefangen.

/**
 * Konstruktorfunktion zu Erstellung einer Kugel.
 * @param center Mittelpunkt der Kugel
 * @param radius Radius der Kugel
 * @param color Farbe des Scenenobjekts
 */
SphereObject::SphereObject(Vector3f center, GLfloat radius, Material material) :
        SceneObject(oSphere, material), object(Sphere3f(center, radius)) {
}

/**
 * Destruktorfunktion
 */
SphereObject::~SphereObject() {
}

/**
 * Liefert den Radius der Kugel.
 * @return Radius
 */
GLfloat SphereObject::getRadius() {
    return object.radius;
}

/**
 * Liefert den Mittelpunkt der Kugel.
 * @return Mittelpunkt
 */
Vector3f SphereObject::getCenter() {
    return object.center;
}

/**
 * Liefert die Information, ob ein Strahl das Objekt schneidet.
 * @param ray Strahl
 * @return bool
 */
bool SphereObject::intersect(const Ray3f& ray) {
    SphereIntersectionQueryResult r = SphereIntersectionQuery()(ray, this->object);
    return r.intersect;
}

/**
 * Liefert den Abstand zum dichtesten Punkt, in dem sich Strahl und Objekt schneiden.
 * Liefert -1 wenn kein Schnittpunkt existiert.
 * @param ray Strahl
 * @return Abstand
 */
GLfloat SphereObject::getIntersectDistance(const Ray3f& ray) {
    GLfloat res = -1.0f;
    SphereFindIntersectionQueryResult r = SphereFindIntersectionQuery()(ray, this->object);

    // Wenn Schnittpunkt existert, finde den nächst-gelegenen
    if (r.intersect) {
        res = r.parameter[0];
    }

    return res;
}

/**
 * Liefert den dichtesten Punkt in Strahlrichtung ausgehend vom Definitionspunkt des Strahls
 * in dem sich Strahl und Objekt schneiden.
 * Liefert NULL wenn kein Schnittpunkt existiert.
 * @param ray Strahl
 * @return Schnittpunkt
 */
Vector3f* SphereObject::createIntersectionPoint(const Ray3f& ray) {
    SphereFindIntersectionQueryResult r = SphereFindIntersectionQuery()(ray, this->object);

    // Wenn Schnittpunkt existert, finde den nächst-gelegenen
    return (r.numIntersections >= 1) ? new Vector3f(r.point[0]) : nullptr;
}

/**
 * Liefert die Normale an einem Punkt, von dem angenommen wird, dass er sich auf der Oberfläche
 * des Objekts befindet.
 * @param point Punkt auf der Oberfläche des Objekts
 * @return Normalenvektor des Objekts am Punkt
 */
Vector3f SphereObject::getNormalAt(const Vector3f& point) {
    // TODO Radius prüfen?
    Vector3f normal = point - this->object.center;
    gte::Normalize(normal);
    return normal;
}

/**
 * Zeichnet die Kugel in OpenGL.
 */
void SphereObject::draw(void) {
    glPushMatrix();
    {
        glTranslatef(this->object.center[0],
                this->object.center[1],
                this->object.center[2]);
        gluSphere(SphereObject::quad, this->object.radius, SPHERE_SLICES, SPHERE_STACKS);
    }
    glPopMatrix();
}

}


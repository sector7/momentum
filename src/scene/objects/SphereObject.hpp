#ifndef SRC_SCENE_OBJECTS_SPHEREOBJECT_HPP_
#define SRC_SCENE_OBJECTS_SPHEREOBJECT_HPP_

#include <SceneObject.hpp>
#include <sceneTypes.hpp>

namespace momentum {

/**
 * Klasse zur Repräsentation einer Kugel.
 */
class SphereObject: public SceneObject {
private:
    /** Kugel im Raum */
    Sphere3f object;

public:
    /** Quadric-Objekt zum Zeichnen der Kugel */
    static GLUquadric* quad;

    /**
     * Konstruktorfunktion zu Erstellung einer Kugel.
     * @param center Mittelpunkt der Kugel
     * @param radius Radius der Kugel
     * @param color Farbe des Scenenobjekts
     */
    SphereObject(Vector3f center, GLfloat radius, Material material);
    /**
     * Destruktorfunktion
     */
    ~SphereObject();
    /**
     * Liefert den Radius der Kugel.
     * @return Radius
     */
    GLfloat getRadius();
    /**
     * Liefert den Mittelpunkt der Kugel.
     * @return Mittelpunkt
     */
    Vector3f getCenter();
    /**
     * Liefert die Information, ob ein Strahl das Objekt schneidet.
     * @param ray Strahl
     * @return bool
     */
    bool intersect(const Ray3f& ray);
    /**
     * Liefert den Abstand zum dichtesten Punkt, in dem sich Strahl und Objekt schneiden.
     * Liefert -1 wenn kein Schnittpunkt existiert.
     * @param ray Strahl
     * @return Abstand
     */
    virtual GLfloat getIntersectDistance(const Ray3f& ray);

    /**
     * Liefert den dichtesten Punkt in Strahlrichtung ausgehend vom Definitionspunkt des Strahls
     * in dem sich Strahl und Objekt schneiden.
     * Liefert NULL wenn kein Schnittpunkt existiert.
     * @param ray Strahl
     * @return Schnittpunkt
     */
    virtual Vector3f* createIntersectionPoint(const Ray3f& ray);

    /**
     * Liefert die Normale an einem Punkt, von dem angenommen wird, dass er sich auf der Oberfläche
     * des Objekts befindet.
     * @param point Punkt auf der Oberfläche des Objekts
     * @return Normalenvektor des Objekts am Punkt
     */
    virtual Vector3f getNormalAt(const Vector3f& point);

    /**
     * Zeichnet die Kugel in OpenGL.
     */
    virtual void draw(void);
};

} /* namespace momentum */

#endif /* SRC_SCENE_OBJECTS_SPHEREOBJECT_HPP_ */

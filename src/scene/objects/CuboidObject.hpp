#ifndef CUBOIDOBJECT_HPP_
#define CUBOIDOBJECT_HPP_

#include <SceneObject.hpp>
#include <objectTypes.hpp>

namespace momentum {

/**
 * Klasse zur Repräsentation eines Quaders.
 */
class CuboidObject: public SceneObject {
private:
    /** Box im Raum ausgerichtet am Koordinatensystem */
    AlignedBox3f object;
    Vector3f extent;

    /**
     * Zeichnet ein Quadrat mit der Kantenlaenge 1, das aus mehreren kleineren
     * Quadraten zusammen gesetzt ist.
     */
    void drawUnitySquare(void);

    /** Zeichnet einen Einheitswürfel mit der Kantenlänge 1, dessen
     * Mittelpunkt im Koordinatenursprung liegt.
     */
    void drawUnityCuboid(void);

public:
    /**
     * Konstruktorfunktion zu Erstellung eines Quaders.
     * @param min minimale Ausdehnung
     * @param max maximale Ausdehnung
     * @param color Farbe des Scenenobjekts
     */
    CuboidObject(Vector3f min, Vector3f max, Material material);
    /**
     * Destruktorfunktion
     */
    ~CuboidObject();
    /**
     * Liefert die Ecke des Quaders mit den kleinsten Koordinaten
     * @return minimaler Vertex
     */
    Vector3f getMinVertex();
    /**
     * Liefert die Ecke des Quaders mit den größten Koordinaten
     * @return maximaler Vertex
     */
    Vector3f getMaxVertex();
    /**
     * Liefert die Information, ob ein Strahl das Objekt schneidet.
     * @param ray Strahl
     * @return bool
     */
    bool intersect(const Ray3f& ray);
    /**
     * Liefert den Abstand zum dichtesten Punkt, in dem sich Strahl und Objekt schneiden.
     * Liefert -1 wenn kein Schnittpunkt existiert.
     * @param ray Strahl
     * @return Abstand
     */
    virtual GLfloat getIntersectDistance(const Ray3f& ray);

    /**
     * Liefert den dichtesten Punkt in Strahlrichtung ausgehend vom Definitionspunkt des Strahls
     * in dem sich Strahl und Objekt schneiden.
     * Liefert NULL wenn kein Schnittpunkt existiert.
     * @param ray Strahl
     * @return Schnittpunkt
     */
    virtual Vector3f* createIntersectionPoint(const Ray3f& ray);

    /**
     * Liefert die Normale an einem Punkt, von dem angenommen wird, dass er sich auf der Oberfläche
     * des Objekts befindet.
     * @param point Punkt auf der Oberfläche des Objekts
     * @return Normalenvektor des Objekts am Punkt
     */
    virtual Vector3f getNormalAt(const Vector3f& point);

    /**
     * Zeichnet den Würfel in OpenGL.
     */
    virtual void draw(void);
};

} /* namespace momentum */

#endif /* CUBOIDOBJECT_HPP_ */

/*
 * Lightc.cpp
 *
 *  Created on: 01.07.2015
 *      Author: ellen
 */

#include <Light.hpp>
#include <debugGL.hpp>

namespace momentum {

/**
 * Konstruktorfunktion - Initialisiert die Lichtquelle.
 * @param pos Position
 * @param type Typ
 * @param color Farbe pro Farbkomponente
 */
Light::Light(Vector3f pos, LightType type, GLfloat watt, Color3f color) :
        pos(pos), type(type), watt(watt), color(color) {

}

/**
 * Destruktorfunktion
 */
Light::~Light() {
}

/**
 * Liefert den Typen der Lichtquelle.
 * @return Typ der Lichtquelle
 */
LightType Light::getType() {
    return this->type;
}

/**
 * Liefert die Farbe der Lichtquelle
 * @return Farbe
 */
Color3f Light::getColor() const {
    return this->color;
}

/**
 * Liefert die Position der Lichtquelle
 * @return Position
 */
Vector3f Light::getPosition(void) const {
    return this->pos;
}

/**
 * Liefert die Wattzahl der Lichtquelle
 * @return Wattzahl
 */
GLfloat Light::getWatt(void) {
    return this->watt;
}

/**
 * Zeichnet die Repräsentation der Lichtquelle in OpenGL.
 */
void Light::drawRepresentation(void) {
    glPushMatrix();
    {
        glTranslatef(this->pos[0],
                this->pos[1],
                this->pos[2]);
        gluSphere(SphereObject::quad, LIGHT_REPRESENTATION_RADIUS, SPHERE_SLICES, SPHERE_STACKS);
    }
    glPopMatrix();
}

}

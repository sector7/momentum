#ifndef LIGHTC_HPP_
#define LIGHTC_HPP_

#include <commonTypes.hpp>
#include <sceneTypes.hpp>
#include <SphereObject.hpp>

namespace momentum {

/**
 * Klasse zur Repräsentation einer Lichtquelle.
 */
class Light {
public:
    /**
     * Konstruktorfunktion - Initialisiert die Lichtquelle.
     * @param pos Position
     * @param type Typ
     * @param watt Wattzahl der Lampe
     * @param color Farbe pro Farbkomponente
     */
    Light(Vector3f pos, LightType type, GLfloat watt, Color3f color);

    /**
     * Destruktor.
     */
    virtual ~Light();

    /**
     * Liefert die Position der Lichtquelle
     * @return Position
     */
    Vector3f getPosition(void) const;

    /**
     * Liefert die Wattzahl der Lichtquelle
     * @return Wattzahl
     */
    GLfloat getWatt(void);

    /**
     * Liefert den Typen der Lichtquelle.
     * @return Typ der Lichtquelle
     */
    LightType getType();

    /**
     * Liefert die Farbe der Lichtquelle
     * @return Farbe
     */
    Color3f getColor() const;

    /**
     * Zeichnet die Repräsentation der Lichtquelle in OpenGL.
     */
    void drawRepresentation(void);

private:
    /** Position der Lichtquelle */
    Vector3f pos;
    /** Typ */
    LightType type;
    /** Watt des Lichtes */
    GLfloat watt;
    /** Lichtfarbe */
    Color3f color;
};

} /* namespace momentum */
#endif /* LIGHT_H_ */

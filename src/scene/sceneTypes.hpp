#ifndef __SCENETYPES_HPP__
#define __SCENETYPES_HPP__

#include <commonTypes.hpp>
#include <Photon.hpp>
#include <mathUtils.hpp>

#include <stdio.h>
#include <list>
#include <array>
#include <limits.h>

#include <GteIntrRay3AlignedBox3.h>
#include <GteIntrRay3Sphere3.h>
#include <GteNearestNeighborQuery.h>

using namespace std;

namespace momentum {

/** Koordinatenindizes */
enum e_coordIndices {
    X, Y, Z, H
};

/** Farbindizes */
enum e_colorIndices {
    R, G, B, A
};

/** Objekttyp */
typedef enum e_objectType {
    oRect, oSphere, oCuboid
} ObjectType;

typedef enum {
    AMBIENT, DIFFUSE, SPECULAR
} LightComponent;

/** Lichttypen */
typedef enum {
    POINT, LAMBERT
} LightType;
typedef Color3f LightColor;

typedef struct s_objectMaterial {
    Color3f ambient; /* Selbstleuchten des Objektes */
    Color3f diffuse;
    Color3f specular;
    Color3f transmission;
    GLfloat shininess[1]; /* Glanzpunkt-Ausmaß. Wert zwischen 0.0 und 128.0 */
    GLfloat probabilityDiffuse;
    GLfloat probabilitySpecular;
    GLfloat probabilityTransmission;
    GLfloat refractionIndex;
} Material;

/** Farben */
#define COLOR_BLACK     0.0f, 0.0f, 0.0f

#define COLOR_RED       1.0f, 0.1f, 0.1f
#define COLOR_ORANGE    1.0f, 0.5f, 0.0f
#define COLOR_GREEN     0.1f, 1.0f, 0.1f
#define COLOR_YELLOW    1.0f, 1.0f, 0.1f
#define COLOR_LIGHT_YELLOW    1.0f, 1.0f, 0.6f
#define COLOR_BLUE      0.1f, 0.1f, 1.0f
#define COLOR_PURPLE    1.0f, 0.1f, 1.0f
#define COLOR_CYAN      0.1f, 1.0f, 1.0f
#define COLOR_WHITE     1.0f, 1.0f, 1.0f

#define COLOR_LIGHT_GRAY     0.75f, 0.75f, 0.75f
#define COLOR_LIGHT_GREEN     0.5f, 1.0f, 0.5f

#define COLOR_DARK_RED       0.5f, 0.05f, 0.05f
#define COLOR_DARK_GREEN     0.05f, 0.5f, 0.05f
#define COLOR_DARK_YELLOW    0.5f, 0.5f, 0.05f
#define COLOR_DARK_BLUE      0.05f, 0.05f, 0.5f
#define COLOR_DARK_PURPLE    0.5f, 0.05f, 0.5f
#define COLOR_DARK_CYAN      0.05f, 0.5f, 0.5f
#define COLOR_GRAY           0.5f, 0.5f, 0.5f

/** Materialien */
//                              Material({  { ambient },    { diffuse },    { specular },   {transmission}, shininess,
//                              propabilities diffuse       / specular      / transmission, refraction index})
#define MAT_SKYBOX              Material({  {COLOR_BLACK},  {COLOR_GRAY},   {COLOR_BLACK}, {COLOR_BLACK},   0.0, \
                                            0.2,            0.0,            0.0,            0.0})

#define MAT_BOX_GREEN           Material({  {COLOR_BLACK},  {COLOR_LIGHT_GREEN},    {COLOR_WHITE},    {COLOR_LIGHT_GREEN},    80.0, \
                                            0.0,            0.15,            0.85,            RI_GLASS})

#define MAT_BOX_ORANGE           Material({  {COLOR_BLACK},  {COLOR_ORANGE},    {COLOR_WHITE},    {COLOR_ORANGE},    80.0, \
                                            1.0,            0.0,            0.0,            RI_GLASS})

#define MAT_SPHERE_RED          Material({  {COLOR_BLACK},  {COLOR_RED},    {COLOR_RED},    {COLOR_RED},    80.0, \
                                            0.0,            1.0,            0.0,            RI_GLASS})

#define MAT_SPHERE_YELLOW       Material({  {COLOR_BLACK},  {COLOR_YELLOW}, {COLOR_YELLOW},  {COLOR_YELLOW}, 120.0, \
                                            0.0,            1.0,            0.0,            RI_GLASS})

#define MAT_SPHERE_BLUE         Material({  {COLOR_BLACK},  {COLOR_BLUE}, {COLOR_BLUE},  {COLOR_BLUE}, 80.0, \
                                            1.0,            0.0,            0.0,            RI_GLASS})

#define MAT_SPHERE_GREEN        Material({  {COLOR_LIGHT_GREEN},  {COLOR_LIGHT_GREEN}, {COLOR_LIGHT_GREEN},  {COLOR_LIGHT_GREEN}, 80.0, \
                                            0.0,            0.0,            1.0,            RI_GLASS})

#define MAT_GLAS                Material({  {COLOR_GRAY},  {COLOR_WHITE}, {COLOR_WHITE},  {COLOR_WHITE}, 30.0, \
                                0.0,            0.0,            1.0,            RI_GLASS})

/** Größen */
#define THICKNESS 0.15f

#define SKYBOX_Y 2.0f
#define SKYBOX_X 8.0f
#define SKYBOX_Z 8.0f
#define SKYBOX_THICKNESS 0.01f

#define SPHERE_SLICES 30.0f
#define SPHERE_STACKS 30.0f
#define SPHERE_SIZE (SKYBOX_Y * 0.5)

#define CUBOID_ACCURACY 10
#define LIGHT_REPRESENTATION_RADIUS 0.05f
#define LIGHT_WEAKENING 10.0f

#ifdef DEBUG
/** Gibt an, ob Debug-Ausgaben dieses Moduls angezeigt werden sollen */
#define SCENE_PRINT GL_TRUE
/** Makro zur Ausgabe von Debug-Informationen */
#define SCENE_INFO(ARGS) if (SCENE_PRINT) {INFO(ARGS);}
#else
#define SCENE_INFO(ARGS)
#endif

/* ------------ Texturen ------------------ */
typedef struct s_texture {
    GLuint id;
    char const *filename;
} Texture;

/** Objekt Klassen */
typedef gte::AlignedBox3<float> AlignedBox3f;
typedef gte::Sphere3<float> Sphere3f;

}
#endif /* __SCENETYPES_HPP__ */

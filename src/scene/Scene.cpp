#include "Scene.hpp"

namespace momentum {

/**
 * Konstruktorfunktion
 */
Scene::Scene() :
        photonMaps() {
}

/**
 * Destruktorfunktion
 */
Scene::~Scene() {
}

/**
 * Liefert die Liste der zu zeichnenden Szenenobjekte.
 * @return Liste der Szenenobjekte.
 */
const SceneObjectList& Scene::getObjects() const {
    return this->objects;
}

/**
 * Liefert die Liste der in der Szene befindlichen Lichtquellen.
 * @return Liste der Lichter.
 */
const LightList& Scene::getLights() const {
    return this->lights;
}

/**
 * Methode zum Hinzufügen eines Objektes zu der Szene.
 * @param obj Hinzuzufügendes Objekt
 * @return Diese Instanz
 */
Scene* Scene::addObject(SceneObject* obj) {
    objects.push_back(obj);
    return this;
}

/**
 * Fügt ein neues Licht zur Liste der Lichtquellen hinzu.
 * @param light Lichtquelle
 * @return Diese Instanz
 */
Scene* Scene::addLight(Light* light) {
    lights.push_back(light);
    return this;
}

/**
 * Liefert die PhotonMap der Szene.
 * @return PhotonMap
 */
const PhotonMapManager& Scene::getPhotonMaps() const {
    return *(this->photonMaps);
}

/**
 * Setzt den PhotonMapManager der Szene.
 * @param photonMapManager
 */
void Scene::setPhotonMapManager(PhotonMapManager* photonMapManager) {
    this->photonMaps = photonMapManager;
}

}


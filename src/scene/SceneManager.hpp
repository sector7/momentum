#ifndef __SCENEMANAGER_HPP__
#define __SCENEMANAGER_HPP__

#include <SceneObject.hpp>
#include <sceneTypes.hpp>
#include <Light.hpp>
#include <Scene.hpp>
#include <PhotonMapManager.hpp>
#include <Config.hpp>

namespace momentum {

/**
 * Klasse zur Organisation der Szene.
 * Erstellt und beinhaltet die zu zeichnenden Szenenobjekte
 */
class SceneManager {
private:
    /** Darzustellende Szene mit Objekten und Lichtern */
    Scene scene;

    /**
     * Erstellt die Kugeln der Szene.
     */
    void initSpheres();

    /**
     * Erstellt eine Box um die Szenenobjekte herum.
     */
    void initSkybox();

    /**
     * Erstellt die würfel der Szene.
     */
    void initQuadrics();

    /**
     * Initialisiert die Lichtquellen der Szene.
     */
    void initLights();

    /**
     * Initialisiert die PhotonMaps der Szene.
     * @param photonCount Anzahl an Photonen pro Lichtquelle
     */
    void initPhotonMaps(GLuint photonCount);

public:
    /**
     * Konstruktorfunktion
     * Erstellt die Szenenobjekte
     * @param config Programmkonfiguration
     */
    SceneManager(Config config);

    /**
     * Konstruktorfunktion
     * Erstellt die Szenenobjekte mit den Default Parametern
     */
    SceneManager();

    /**
     * Destruktorfunktion
     */
    virtual ~SceneManager();

    /**
     * Liefert das Szenen-Objekt.
     * @return Szenen-Objekt
     */
    const Scene& getScene();
};

} /* namespace momentum */

#endif /* __SCENEMANAGER_HPP__ */

#include "SceneManager.hpp"

#include <CuboidObject.hpp>
#include <SphereObject.hpp>
#include <PhotonTracer.hpp>
#include <mathUtils.hpp>
#include <debugGL.hpp>

// Liste von Lichtern
namespace momentum {

/**
 * Konstruktorfunktion
 * Erstellt die Szenenobjekte
 * @param config Programmkonfiguration
 */
SceneManager::SceneManager(Config config) {
    // Zufallszahlen initialisieren
    initRand();
    initLights();
    initSpheres();
    initSkybox();
    initQuadrics();
    initPhotonMaps(config.PHOTON_COUNT);
}

/**
 * Konstruktorfunktion
 * Erstellt einen Dummy Scene-Mgr
 */
SceneManager::SceneManager() {
}

/**
 * Destruktorfunktion
 */
SceneManager::~SceneManager() {
}

/**
 * Erstellt die Kugeln der Szene.
 */
void SceneManager::initSpheres() {
    scene.addObject(new SphereObject(
            { 0.0f, 0.0f, 0.0f },
            SPHERE_SIZE,
            MAT_SPHERE_RED));
    scene.addObject(new SphereObject(
            { SKYBOX_X * 0.25f, SKYBOX_Y * 0.25, SKYBOX_Z * 0.25f },
            SPHERE_SIZE * 0.75,
            MAT_SPHERE_YELLOW));
    scene.addObject(new SphereObject(
            { SKYBOX_X * 0.5f, -SKYBOX_Y / 3.0, -SKYBOX_Z * 0.25f },
            SPHERE_SIZE,
            MAT_SPHERE_BLUE));
    // Glas-Kugel groß
    scene.addObject(new SphereObject(
            { -SKYBOX_X * 0.45, -SKYBOX_Y * 0.45, -SKYBOX_Z * 0.2f },
            SPHERE_SIZE,
            MAT_SPHERE_GREEN));
    // Glas-Kugel klein
    scene.addObject(new SphereObject(
            { -SKYBOX_X * 0.28f, 0.4, -SKYBOX_Z
                    * 0.85f + SKYBOX_Y },
            SPHERE_SIZE / 3.0,
            MAT_GLAS));
}

/**
 * Erstellt die würfel der Szene.
 */
void SceneManager::initQuadrics() {
    // diffuser Würfel
    scene.addObject(
            new CuboidObject(
                    { -SKYBOX_X * 0.25f, -SKYBOX_Y + SKYBOX_THICKNESS, SKYBOX_Z * 0.85f - SKYBOX_Y }, /* min */
                    { -SKYBOX_X * 0.25f + SKYBOX_Y, SKYBOX_THICKNESS, SKYBOX_Z * 0.85f }, /* max */
                    MAT_BOX_ORANGE) /* material */
                    );
    // Glaswürfel
    scene.addObject(
            new CuboidObject(
                    { -SKYBOX_X * 0.3f / 2, SKYBOX_THICKNESS * 2, SKYBOX_Z
                            * 0.75f - SKYBOX_Y / 2 }, /* min */
                    { -SKYBOX_X * 0.3f + SKYBOX_Y, SKYBOX_Y / 2 + SKYBOX_THICKNESS * 2, SKYBOX_Z
                            * 0.9f - SKYBOX_Y }, /* max */
                    MAT_BOX_GREEN) /* material */
                    );
    // diffuser Würfel
    scene.addObject(
            new CuboidObject(
                    { -SKYBOX_X * 0.25f, -SKYBOX_Y + SKYBOX_THICKNESS, -SKYBOX_Z * 0.85f }, /* min */
                    { -SKYBOX_X * 0.25f + SKYBOX_Y, SKYBOX_THICKNESS, -SKYBOX_Z * 0.85f + SKYBOX_Y }, /* max */
                    MAT_SPHERE_BLUE) /* material */
                    );
}

/**
 * Erstellt eine Box um die Szenenobjekte herum.
 */
void SceneManager::initSkybox() {
    /* rechte Fläche */
    scene.addObject(new CuboidObject(
            { SKYBOX_X - SKYBOX_THICKNESS, -SKYBOX_Y, -SKYBOX_Z }, /* min */
            { SKYBOX_X, SKYBOX_Y, SKYBOX_Z }, /* max */
            MAT_SKYBOX) /* material */
            );
    /* linke Fläche */
    scene.addObject(new CuboidObject(
            { -SKYBOX_X, -SKYBOX_Y, -SKYBOX_Z }, /* min */
            { -SKYBOX_X + SKYBOX_THICKNESS, SKYBOX_Y, SKYBOX_Z }, /* max */
            MAT_SKYBOX) /* material */
            );
    /* hintere Fläche */
    scene.addObject(new CuboidObject(
            { -SKYBOX_X + SKYBOX_THICKNESS, -SKYBOX_Y, -SKYBOX_Z }, /* min */
            { SKYBOX_X - SKYBOX_THICKNESS, SKYBOX_Y, -SKYBOX_Z + SKYBOX_THICKNESS }, /* max */
            MAT_SKYBOX) /* material */
            );
    /* vordere Fläche */
    scene.addObject(new CuboidObject(
            { -SKYBOX_X + SKYBOX_THICKNESS, -SKYBOX_Y, SKYBOX_Z - SKYBOX_THICKNESS }, /* min */
            { SKYBOX_X - SKYBOX_THICKNESS, SKYBOX_Y, SKYBOX_Z }, /* max */
            MAT_SKYBOX) /* material */
            );
    /* untere Fläche */
    scene.addObject(
            new CuboidObject(
                    { -SKYBOX_X + SKYBOX_THICKNESS, -SKYBOX_Y, -SKYBOX_Z + SKYBOX_THICKNESS }, /* min */
                    { SKYBOX_X - SKYBOX_THICKNESS, -SKYBOX_Y + SKYBOX_THICKNESS, SKYBOX_Z
                            - SKYBOX_THICKNESS }, /* max */
                    MAT_SKYBOX) /* material */
                    );
    /* obere Fläche */
    scene.addObject(
            new CuboidObject(
                    { -SKYBOX_X + SKYBOX_THICKNESS, SKYBOX_Y - SKYBOX_THICKNESS, -SKYBOX_Z
                            + SKYBOX_THICKNESS }, /* min */
                    { SKYBOX_X - SKYBOX_THICKNESS, SKYBOX_Y, SKYBOX_Z - SKYBOX_THICKNESS }, /* max */
                    MAT_SKYBOX) /* material */
                    );

}

/**
 * Initialisiert die Lichtquellen der Szene.
 */
void SceneManager::initLights() {
    Color3f color = { 1.0f, 1.0f, 1.0f };

    scene.addLight(new Light(
            { SKYBOX_X * 0.5, SKYBOX_Y * 0.9, SKYBOX_Z * 0.5f }, // Position
            POINT, // Typ
            100.0f, // Watt
            color
            ));

    scene.addLight(new Light(
            { -SKYBOX_X * 0.6, SKYBOX_Y * 0.1, -SKYBOX_Z * 0.25f }, // Position
            POINT, // Typ
            50.0f, // Watt
            color
            ));

    scene.addLight(new Light(
            { -SKYBOX_X * 0.3f + 0.125, 0.6 + SPHERE_SIZE / 3.0, -SKYBOX_Z * 0.8f + SKYBOX_Y }, // Position
            POINT, // Typ
            25.0f, // Watt
            color
            ));

}

/**
 * Initialisiert die PhotonMaps der Szene.
 * @param photonCount Anzahl an Photonen pro Lichtquelle
 */
void SceneManager::initPhotonMaps(GLuint photonCount) {
    PhotonTracer tracer = PhotonTracer(scene.getObjects(), scene.getLights(), photonCount);
    PhotonMapManager* pmm = tracer.createPhotonMaps();
    INFO(
            ("Größe der Photonmaps - Direkt:    %d\n", pmm->getPhotonMap(DIRECT_ILLU).getPhotons().size()));
    INFO(
            ("Größe der Photonmaps - Indirekt:  %d\n", pmm->getPhotonMap(INDIRECT_ILLU).getPhotons().size()));
    INFO(
            ("Größe der Photonmaps - Kaustiken: %d\n", pmm->getPhotonMap(CAUSTIC_ILLU).getPhotons().size()));
    this->scene.setPhotonMapManager(pmm);
}

/**
 * Liefert das Szenen-Objekt.
 * @return Szenen-Objekt
 */
const Scene& SceneManager::getScene() {
    return scene;
}

} /* namespace momentum */

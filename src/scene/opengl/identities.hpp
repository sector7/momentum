#ifndef __IDENTITIES_HPP__
#define __IDENTITIES_HPP__
/**
 * @file
 * Darstellungs-Modul.
 * Das Modul kapselt die Rendering-Funktionalitaet (insbesondere der OpenGL-
 * Aufrufe) des Programms.
 *
 * @author Ellen Schwartau (Minf9888) und Malte Jörgens (Minf9640)
 */

using namespace momentum;

/**
 * Zeichnet ein Quadrat mit der Kantenlaenge 1 und passt die Textur Koordinaten entsprechend
 * der angegebenen Skalierung an.
 */
void drawIdentitySquare(Color3f color, float resizeTextureX = 1.0f, float resizeTextureY = 1.0f);

/**
 * Zeichnet einen Einheitswürfel, der von innen sichtbar ist.
 */
void drawIdentitySkybox(Color3f color);

/**
 * Zeichnet einen Einheitswürfel, der von außen sichtbar ist und passt die Textur Koordinaten
 * entsprechend der angegebenen Skalierung an.
// */
//void drawIdentityCube(Color3f color, float resizeTextureX = 1.0f, float resizeTextureY = 1.0f,
//        float resizeTextureZ = 1.0f);

/**
 * Zeichnet eine gluSphere mit dem Durchmesser 1 im Ursprung.
 */
GLUquadricObj* drawIdentitySphere(GLUquadricObj* quadric);

#endif

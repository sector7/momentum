#ifndef __SCENE_HPP__
#define __SCENE_HPP__
/**
 * @file
 * Darstellungs-Modul.
 * Das Modul kapselt die Rendering-Funktionalitaet (insbesondere der OpenGL-
 * Aufrufe) des Programms.
 *
 * @author Ellen Schwartau (Minf9888) und Malte Jörgens (Minf9640)
 */

#include <PixelRenderer.hpp>

namespace momentum {

/**
 * Zeichen-Funktion.
 * Stellt die Szene dar. Ein weisses Rechteck wird an der vorher im Logik-Modul
 * berechnet Position gezeichnet.
 * Ausgabe eines Quadrats.
 */
void drawScene(const Scene& scene);

/**
 * Initialisierung der Szene (inbesondere der OpenGL-Statusmaschine).
 * Setzt Hintergrund- und Zeichenfarbe.
 * @param lights Liste aller Lichter in der Szene
 * @return Rueckgabewert: im Fehlerfall 0, sonst 1.
 */
int initScene(const Scene& scene);

/**
 * Initialisierung der Lichtquellen.
 * Setzt Eigenschaften der Lichtquellen (Farbe, Oeffnungswinkel, ...)
 */
void setLights(LightList lights);

/**
 * Wireframemodus setzen.
 */
void handleWireframeMode(GLboolean wireframe);

/**
 * Vollbild-Modus einstellen.
 * @param   mode    Vollbild-Modus aktiv?
 */
void handleFullscreen(GLboolean mode);

}
#endif

/**
 * @file
 * Darstellungs-Modul.
 * Das Modul kapselt die Rendering-Funktionalitaet (insbesondere der OpenGL-
 * Aufrufe) des Programms.
 *
 * @author Ellen Schwartau (Minf9888) und Malte Jörgens (Minf9640)
 */

/* ---- System Header einbinden ---- */
#include <GL/glut.h>
#include <math.h>

/* ---- Eigene Header einbinden ---- */
#include <commons.hpp>
#include <sceneTypes.hpp>
#include <scene.hpp>
#include <debugGL.hpp>
#include <identities.hpp>

/* ---- Externe Header einbinden ---- */
#include <algorithm>

using namespace std;

/**
 * Zeichnet ein Quadrat mit der Kantenlaenge 1, das aus mehreren kleineren
 * Quadraten zusammen gesetzt ist.
 */
void drawIdentitySquare(Color3f color, float resizeTextureX, float resizeTextureY) {
    static GLfloat normVect[] = { 0.0f, 1.0f, 0.0f };
    float ratio = resizeTextureX / resizeTextureY;
    float xFactor = min(1.0f, ratio);
    float yFactor = min(1.0f, 1.0f / ratio);
    glColor3fv(&color[0]);
    glBegin (GL_QUADS);
    {
        glNormal3fv(normVect);
        glTexCoord2f(0.0f, 0.0f);
        glVertex3f(-0.5f, 0.0f, -0.5f);
        glTexCoord2f(0.0f, yFactor);
        glVertex3f(-0.5f, 0.0f, 0.5f);
        glTexCoord2f(xFactor, yFactor);
        glVertex3f(0.5f, 0.0f, 0.5f);
        glTexCoord2f(xFactor, 0.0f);
        glVertex3f(0.5f, 0.0f, -0.5f);
    }
    glEnd();
}

/**
 * Zeichnet einen Einheitswürfel, der von innen sichtbar ist.
 */
void drawIdentitySkybox(Color3f color) {
    /* Wände zeichnen */
    glPushMatrix();
    {
        /* Deckelflaeche */
        glPushMatrix();
        glTranslatef(0.0f, 1.0f, 0.0f);
        glRotatef(180.0f, 1.0f, 0.0f, 0.0f);
        drawIdentitySquare(color);
        glPopMatrix();

        /* Bodenflaeche */
        glPushMatrix();
        drawIdentitySquare(color);
        glPopMatrix();

        /* Frontflaeche */
        glPushMatrix();
        glTranslatef(0.0f, 0.5f, 0.5f);
        glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
        drawIdentitySquare(color);
        glPopMatrix();

        /* Rueckseitenflaeche */
        glPushMatrix();
        glTranslatef(0.0f, 0.5f, -0.5f);
        glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
        drawIdentitySquare(color);
        glPopMatrix();

        /* rechte Seitenflaeche */
        glPushMatrix();
        glTranslatef(0.5, 0.5f, 0.0f);
        glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
        drawIdentitySquare(color);
        glPopMatrix();

        /* linke Seitenflaeche */
        glPushMatrix();
        glTranslatef(-0.5f, 0.5f, 0.0f);
        glRotatef(-90.0f, 0.0f, 0.0f, 1.0f);
        drawIdentitySquare(color);
        glPopMatrix();

    }
    glPopMatrix();
}

///**
// * Zeichnet einen Einheitswürfel, der von außen sichtbar ist.
// * TODO: Kann weg, wenn glutSolidCube() in den Szenenobjekten funktioniert.
// */
//void drawIdentityCube(Color3f color, float resizeTextureX, float resizeTextureY,
//        float resizeTextureZ) {
//    /* Wände zeichnen */
//    glPushMatrix();
//    {
//        /* Deckelflaeche */
//        glPushMatrix();
//        glTranslatef(0.0f, 1.0f, 0.0f);
//        drawIdentitySquare(color, resizeTextureX, resizeTextureZ);
//        glPopMatrix();
//
//        /* Bodenflaeche */
//        glPushMatrix();
//        glRotatef(180.0f, 1.0f, 0.0f, 0.0f);
//        drawIdentitySquare(color, resizeTextureX, resizeTextureZ);
//        glPopMatrix();
//
//        /* Frontflaeche */
//        glPushMatrix();
//        glTranslatef(0.0f, 0.5f, 0.5f);
//        glRotatef(90.0f, 1.0f, 0.0f, 0.0f);
//        drawIdentitySquare(color, resizeTextureX, resizeTextureY);
//        glPopMatrix();
//
//        /* Rueckseitenflaeche */
//        glPushMatrix();
//        glTranslatef(0.0f, 0.5f, -0.5f);
//        glRotatef(-90.0f, 1.0f, 0.0f, 0.0f);
//        drawIdentitySquare(color, resizeTextureX, resizeTextureY);
//        glPopMatrix();
//
//        /* rechte Seitenflaeche */
//        glPushMatrix();
//        glTranslatef(0.5, 0.5f, 0.0f);
//        glRotatef(-90.0f, 0.0f, 0.0f, 1.0f);
//        drawIdentitySquare(color, resizeTextureY, resizeTextureZ);
//        glPopMatrix();
//
//        /* linke Seitenflaeche */
//        glPushMatrix();
//        glTranslatef(-0.5f, 0.5f, 0.0f);
//        glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
//        drawIdentitySquare(color, resizeTextureY, resizeTextureZ);
//        glPopMatrix();
//    }
//    glPopMatrix();
//}

/**
 * Zeichnet eine gluSphere mit dem Durchmesser 1 im Ursprung.
 */
GLUquadricObj* drawIdentitySphere(GLUquadricObj* quadric) {
    /* Wenn kein Quadric-Objekt vorhanden, erstellen */
    if (quadric == NULL) {
        /* Ja, das hier ist eine innere Zuweisung und genauso gemeint! :) */
        if ((quadric = gluNewQuadric()) != NULL) {
            /* Normalen für Quadrics berechnen lassen */
            gluQuadricNormals(quadric, GLU_SMOOTH);
        } else {
            ERROR(("Quadric-Erstellung fehlgeschlagen!"));
        }
    }
    gluSphere(quadric, 0.5f, SPHERE_SLICES, SPHERE_STACKS);
    return quadric;
}


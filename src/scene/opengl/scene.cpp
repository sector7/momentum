/**
 * @file
 * Darstellungs-Modul.
 * Das Modul kapselt die Rendering-Funktionalitaet (insbesondere der OpenGL-
 * Aufrufe) des Programms.
 */

/* ---- System Header einbinden ---- */
#include <GL/glut.h>
#include <math.h>

/* ---- Eigene Header einbinden ---- */
#include <commons.hpp>
#include <sceneTypes.hpp>
#include <scene.hpp>
#include <identities.hpp>
#include <string.hpp>
#include <debugGL.hpp>
#include <flag.hpp>
#include <PhotonMap.hpp>
#include <io.hpp>

namespace momentum {

/* ---- globale Variablen ---- */
/** Quadric-Speicher */
GLUquadricObj* g_quadric = NULL;

/**
 * Setzt die Farbe des Lichts je Komponente (ambient, diffuc, specular)
 * @param lightId int Id des Lichts
 * @param color LightColor Farbe pro Farbkomponente
 */
void setLightColor(int ID, Color3f color) {
    GLfloat c[3] = { color[0], color[1], color[2] };
    for (int i = 0; i < 3; i++) {
        /* Selbe Farbe der Lichtquelle pro Kanal setzen */
        glLightfv(GL_LIGHT0 + ID, GL_AMBIENT + i, c);
    }
}

/**
 * Setzt die Position des Lichts.
 * @param lightId int Id des Lichts
 * @param lightPos Vector3f Position des Lichts
 */
void setLightPos(int ID, Vector3f lightPos) {
    Vector4f lp = { 0.0f, 0.0f, 0.0f, 0.0f };

    /* Lichtposition in homogene Koordinaten "umwandeln" */
    lp[X] = lightPos[X];
    lp[Y] = lightPos[Y];
    lp[Z] = lightPos[Z];
    lp[H] = 1.0f;

    /* Licht positionieren */
    glLightfv(GL_LIGHT0 + ID, GL_POSITION, &lp[0]);
}

/* --------------- Initialisierung ---------------------------------- */

/**
 * Initialisierung der Lichtquellen.
 * Setzt Eigenschaften der Lichtquellen (Farbe, Oeffnungswinkel, ...)
 */
void setLights(LightList lights) {
    int lightID = 0;
    /* Für jede Lichtquelle Farbe und Position setzen */
    for (LightList::const_iterator iter = lights.begin(),
            end = lights.end();
            iter != end;
            iter++) {

        Light* l = *iter;

        setLightPos(lightID, l->getPosition());
        setLightColor(lightID, l->getColor());

        // Licht einschalten
        glEnable(GL_LIGHT0 + lightID);
        glEnable (GL_LIGHTING);

        lightID++;
    }

}

/**
 * Initialisierung der Szene (inbesondere der OpenGL-Statusmaschine).
 * Setzt Hintergrund- und Zeichenfarbe, aktiviert Tiefentest und
 * Backface-Culling.
 * @return Rueckgabewert: im Fehlerfall 0, sonst 1.
 */
int initScene(const Scene& scene) {
    /* Setzen der Farbattribute */
    /* Hintergrundfarbe */
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    /* Zeichenfarbe */
    //glColor3f(1.0f, 1.0f, 1.0f);
    /* Z-Buffer-Test aktivieren */
    glEnable (GL_DEPTH_TEST);
    /* Polygonrueckseiten nicht anzeigen */
    glCullFace (GL_BACK);
    glDisable (GL_CULL_FACE);
    glDisable (GL_COLOR_MATERIAL);

    /* Normalen nach Transformationen wieder auf die
     * Einheitslaenge bringen */
    glEnable (GL_NORMALIZE);

    /* Lichtquellen aktivieren */
    setLights(scene.getLights());

    return (GLGETERROR == GL_NO_ERROR);
}

/* ------------- Hilfsfunktionen ------------------------------------ */
/**
 * Zeichnet Informationen in Textform in das Fenster
 */
static void drawUsage(void) {
    Color3f textColor = { COLOR_BLACK };
    int i;
    static char const* usageLines[USAGE_LINE_COUNT] = USAGE_LINES;

    for (i = 0; i < USAGE_LINE_COUNT; i++) {
        /* einzelne Zeilen der Usage */
        drawString(0.05f, 0.1f + 0.05f * i, &textColor[0], usageLines[i]);
    }
}
/**
 * Zeichnet einen Pfeil mit Länge 1 entlang der positiven z-Achse von -0.5 nach 0.5.
 */
static void drawArrow(void) {
    glBegin (GL_LINE_STRIP);
    {
        /* Achse */
        glVertex3f(0.0f, 0.0f, -0.5f);
        glVertex3f(0.0f, 0.0f, 0.5f);
        /* erster Pfeil */
        glVertex3f(0.05f, 0.0f, 0.45f);
        glVertex3f(-0.05f, 0.0f, 0.45f);
        glVertex3f(0.0f, 0.0f, 0.5f);
        /* zweiter Pfeil */
        glVertex3f(0.0f, 0.05f, 0.45f);
        glVertex3f(0.0f, -0.05f, 0.45f);
        glVertex3f(0.0f, 0.0f, 0.5f);
    }
    glEnd();
}

/**
 * Zeichnet Koordinatenachsen (inklusive Beschriftung).
 */
static void drawAxes(void) {
    /* Farben der Koordinatenbeschriftung */
    Color3f axesTextColor = { 1.0f, 1.0f, 0.0f };

    glColor3f(1.0, 0.0f, 0.0f);

    /* x-Achse */
    glPushMatrix();
    {
        glRotatef(90.0f, 0.0f, 1.0f, 0.0f);
        glScalef(1.0f, 1.0f, 4.0f);
        drawArrow();
    }
    glPopMatrix();

    glColor3f(0.0, 1.0f, 0.0f);

    /* y-Achse */
    glPushMatrix();
    {
        glRotatef(270.0f, 1.0f, 0.0f, 0.0f);
        glScalef(1.0f, 1.0f, 4.0f);
        drawArrow();
    }
    glPopMatrix();

    glColor3f(0.0, 0.0f, 1.0f);

    /* z-Achse */
    glPushMatrix();
    {
        glScalef(1.0f, 1.0f, 4.0f);
        drawArrow();
    }
    glPopMatrix();

    /* Beschriftungen der Koordinatenachsen */
    glColor3fv(&axesTextColor[0]);
    glRasterPos3f(2.1f, 0.0f, 0.0f);
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, 'x');
    glRasterPos3f(0.0f, 2.1f, 0.0f);
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, 'y');
    glRasterPos3f(0.0f, 0.0f, 2.1f);
    glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, 'z');
}

/**
 * Zeichnet die Kugeln der Szene.
 */
void drawSpheres() {
    glColor4f(0.8f, 1.0f, 1.0f, 1.0f);
    glPushMatrix();
    {
        glTranslatef(0.0f, THICKNESS * 2.0, 0.0f);
        glScalef(SPHERE_SIZE, SPHERE_SIZE, SPHERE_SIZE);
        g_quadric = drawIdentitySphere(g_quadric);
    }
    glPopMatrix();
}

/* -------------- DEBUG: Darstellen der Lichtkugeln ------------------ */
/**
 * Zeichnet die Lichtquellen als selbstleuchtende Kugel.
 */
static void drawLightSources(LightList lights) {
    Color3f lightColor;

    Vector3f noEmission = { 0.0, 0.0, 0.0 };

    /* Nicht in den Tiefenpuffer zeichnen */
    //    glDepthMask (GL_FALSE);
    for (LightList::const_iterator iter = lights.begin(),
            end = lights.end();
            iter != end;
            iter++) {
        Light* l = *iter;
        lightColor = l->getColor();
        /* Material selbstleuchten machen */
        Color3f emission = lightColor;
        glMaterialfv(GL_FRONT, GL_EMISSION, &emission[0]);
        l->drawRepresentation();
    }

    /* selbstleuchtend wieder "ausschalten" */
    glMaterialfv(GL_FRONT, GL_EMISSION, &noEmission[0]);

//    glDepthMask (GL_TRUE);
}

/* -------------- Darstellung der PhotonMap ------------------ */
/**
 * Zeichnet die Photonen der PhotonMap als Punkte
 */
static void drawPhotonMap(const PhotonMap& pm) {
    glEnable (GL_COLOR_MATERIAL);
    glDisable (GL_LIGHTING);

    glEnableClientState (GL_COLOR_ARRAY);
    glEnableClientState (GL_VERTEX_ARRAY);

    /* Pointer auf Vertex Array setzen */
    glVertexPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), pm.getPhotonData());
    glColorPointer(3, GL_FLOAT, 6 * sizeof(GLfloat), pm.getPhotonColors());

    glDrawElements
    (GL_LINES,  //Primitivtyp
            pm.getTotalPhotonCount() * 2, //Anzahl Indizes zum Zeichnen
            GL_UNSIGNED_INT,  //Typ der Indizes
            pm.getPhotonIndices());         //Index-Array

    glDisableClientState(GL_VERTEX_ARRAY);
    glDisableClientState(GL_COLOR_ARRAY);
    glDisable(GL_COLOR_MATERIAL);
    glEnable(GL_LIGHTING);
}

/* ------------------ Scene ----------------------------------------- */
/**
 * Zeichenfunktion.
 */
void drawScene(const Scene& scene) {
    /** Material */
    Material material;
    /** Farbe */
    GLfloat color[3] = { 0.0f, 0.0f, 0.0f };

    /* ggf. Koordinatensachsen zeichnen */
    if (getFlag (flag_coordinateSystem)) {
        drawAxes();
    }

    /* Positioniere Lichtquellen */
    setLights(scene.getLights());

    // Iteriere und zeichne alle Szenenobjekte
    SceneObjectList objects = scene.getObjects();
    for (SceneObjectList::const_iterator iter = objects.begin(),
            end = objects.end();
            iter != end;
            iter++) {
        SceneObject* obj = *iter;
        material = obj->getMaterial();

        color[0] = material.ambient[0], color[1] = material.ambient[1], color[2] =
                material.ambient[2];
        glMaterialfv(GL_FRONT, GL_AMBIENT, color);

        color[0] = material.diffuse[0], color[1] = material.diffuse[1], color[2] =
                material.diffuse[2];
        glMaterialfv(GL_FRONT, GL_DIFFUSE, color);

        color[0] = material.specular[0], color[1] = material.specular[1], color[2] =
                material.specular[2];
        glMaterialfv(GL_FRONT, GL_SPECULAR, color);

        glMaterialfv(GL_FRONT, GL_SHININESS, material.shininess);

        obj->draw();

        // Zeichne Repräsentation der Lichtquellen
        drawLightSources(scene.getLights());

        /* PhotonMap Repräsentation oder Szene zeichnen */
        if (getFlag (flag_show_direct_photons))
            drawPhotonMap(scene.getPhotonMaps().getPhotonMap(DIRECT_ILLU));
        if (getFlag (flag_show_indirect_photons))
            drawPhotonMap(scene.getPhotonMaps().getPhotonMap(INDIRECT_ILLU));
        if (getFlag (flag_show_caustic_photons))
            drawPhotonMap(scene.getPhotonMaps().getPhotonMap(CAUSTIC_ILLU));
    }
    /* ggf. Hilfetext ausgeben */
    if (getFlag (flag_usage)) {
        drawUsage();
    }
}

/* ----------------- toggle Funktionen ------------------------------ */

/**
 * (De-)aktiviert den Wireframe-Modus.
 * @param mode Wireframe-Modus aktiv?
 */
void handleWireframeMode(GLboolean mode) {
    if (mode) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        /* Polygonrückseiten anzeigen */
        glDisable (GL_CULL_FACE);
    } else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        /* Polygonrueckseiten nicht anzeigen */
        glEnable (GL_CULL_FACE);
    }
    SCENE_INFO(("Wireframe: %i\n", mode));
}

/**
 * Vollbild-Modus einstellen.
 * @param mode Vollbild-Modus aktiv?
 */
void handleFullscreen(GLboolean mode) {
    static GLint oldWindowSize[2] = { 0, 0 };
    if (mode) {
        /* Alte Fenstergröße zwischenspeichern */
        oldWindowSize[0] = glutGet(GLUT_WINDOW_WIDTH);
        oldWindowSize[1] = glutGet(GLUT_WINDOW_HEIGHT);
        glutFullScreen();
    } else {
        glutReshapeWindow(oldWindowSize[0], oldWindowSize[1]);
    }
}

/* -------------- Finalisierung ------------------------ */
/**
 * Räumt noch reservierten Speicher auf.
 */
void finalizeScene(void) {
    SCENE_INFO(("Quadric-Speicher freigeben...\n"));
    gluDeleteQuadric(g_quadric);
    SCENE_INFO(("...fertig.\n"));
}
}

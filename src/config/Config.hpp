/**
 * Klasse zur Konfiguration des Programms auf Grundlage von Default-Werten und Programmparametern.
 */

#ifndef CONFIG_HPP_
#define CONFIG_HPP_

#include <commons.hpp>
#include <photonAndRayTypes.hpp>

namespace momentum {

class Config {
public:
    /**
     * Initialisiert die Konfiguration auf Grundlage der Programmparameter.
     * @param argc Anzahl der Kommandozeilenparameter (In).
     * @param argv Kommandozeilenparameter (In).
     */
    Config(int argc, char **argv);

    /**
     * Konstruktor
     */
    Config();

    /**
     * Destruktorfunktion.
     */
    virtual ~Config();

    /**
     * Startkonfiguration der Kamera
     */
    CameraType CAMERA_SETTING;

    /**
     * Fenstergröße
     */
    GLuint WINDOW_WIDTH;

    /**
     * Fensterhöhe
     */
    GLuint WINDOW_HEIGHT;

    /**
     * Anzahl an Photonen
     */
    GLuint PHOTON_COUNT;

    /**
     * Anzahl an Strahlen pro Pixel
     */
    GLuint ANTIALIASING;

    /**
     * Vervielfältigung der Strahlen
     */
    GLuint MULTISAMPLING;

private:
    /**
     * Konvertiert eine Zeichenfolge in einen Integer.
     * @param val Zeichenfolge
     * @return Integer
     */
    int parseIntVal(char *val);


};

} /* namespace momentum */

#endif /* CONFIG_HPP_ */

/**
 * Klasse zur Konfiguration des Programms auf Grundlage von Default-Werten und Programmparametern.
 */

#include "Config.hpp"

#include <string>
#include <sstream>

namespace momentum {

/**
 * Konstruktor
 */
Config::Config() {

}

/**
 * Initialisiert die Konfiguration auf Grundlage der Programmparameter.
 * @param argc Anzahl der Kommandozeilenparameter (In).
 * @param argv Kommandozeilenparameter (In).
 */
Config::Config(int argc, char **argv) :
        CAMERA_SETTING(CAM_GLOBAL), WINDOW_WIDTH(INITIAL_WINDOW_WIDTH), WINDOW_HEIGHT(
                INITIAL_WINDOW_HEIGHT), PHOTON_COUNT(PHOTON_COUNT_PER_LIGHT_SRC), ANTIALIASING(
                ANTIALIAS_FACTOR), MULTISAMPLING(
                RAY_COUNT) {
    for (int i = 1; i < (argc - 1); i += 2) {
        char key = (*argv[i]);
        int intVal = parseIntVal(argv[i + 1]);

        switch (key) {
        case 'c':
            CAMERA_SETTING = static_cast<CameraType>(intVal);
            break;
        case 'p':
            PHOTON_COUNT = intVal;
            break;
        case 'a':
            ANTIALIASING = intVal;
            break;
        case 'r':
            MULTISAMPLING = intVal;
            break;
        case 'w':
            WINDOW_WIDTH = intVal;
            break;
        case 'h':
            WINDOW_HEIGHT = intVal;
            break;
        }
    }
}

/**
 * Destruktorfunktion.
 */
Config::~Config() {
}

/**
 * Konvertiert eine Zeichenfolge in einen Integer.
 * @param val Zeichenfolge
 * @return Integer
 */
int Config::parseIntVal(char *val) {
    int intVal = 0;
    stringstream ss;
    ss << val;
    ss >> intVal;
    return intVal;
}

}
/* namespace momentum */

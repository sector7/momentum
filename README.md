# Praktikum Virtuelle Realität und Simulation

## Teilnehmer Gruppe 4 ##
* Ellen Schwartau, 101493, inf101493@fh-wedel.de
* Malte Jörgens, 101519, inf101519@fh-wedel.de
* Timo Gröger, 101504, inf101504@fh-wedel.de


* * *

## Aufgabenbeschreibung ##
Für das Praktikum Virtuelle Realität und Simulation soll in diesem Projekt eine
Lichtsimulation implementiert werden. Das Licht der Szene wird mit den Verfahren
des Photon-Mappings und Raytracings annäherungsweise berechnet und dargestellt.

Es ist keine echtzeitfähige Simulation geplant, sondern die Generierung von statischen
Bildern.

Die Vorstellung des aktuellen Entwicklungsstandes ist im Rahmen der Veranstaltung
Fotorealismus und Simulation erfolgt.

## Programmstart ##
Das Programm kann in der folgenden Weise gestartet werden. Die hier angegebenen Parameter entsprechen den Standardeinstellungen, wenn das Programm ohne weitere Parameter aufgerufen wird. Die Erklärung der optionalen Parameter als Schlüssel-Wert-Paare befindet sich darunter.

./momentum a 1 r 1 p 25000 w 800 h 600 c 0


Parameter | automatische Optimierung
--------- | ------------------------
`a`       | Antialiasing Faktor 
`c`       | Kameraeinstellung (0-5)
`h`       | Fensterhöhe
`p`       | Anzahl der Photonen pro Lichtquelle
`r`       | Multisampling
`w`       | Fensterbreite

## Bedienung des Programms ##
Mit `H`lassen sich zur Laufzeit die Information zur Bedienung über Tastatur und Maus anzeigen.

## Programmvoraussetzungen ##
Zum Kompilieren und Start des Programms unter Linux werden folgende Pakete vorausgesetzt

* gcc
* make
* cmake
* libfreetype6-dev

## make Befehle ##
Bei keiner Parameterangabe wird das Ziel `all` ausgeführt (unten fett).

Befehl            | Effekt
----------------- | -----------------
  **`all`**       | Kompiliert das Programm und die mitgelieferten Bibliotheken
`cleanall`        | Bereinigt das Programm und Kompilate der Bibliotheken
`clean`           | Bereinigt das Programm
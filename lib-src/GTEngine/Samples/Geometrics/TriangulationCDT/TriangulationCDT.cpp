// Geometric Tools LLC, Redmond WA 98052
// Copyright (c) 1998-2015
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
// File Version: 1.5.0 (2014/10/24)

#include "TriangulationCDTWindow.h"

int main(int, char const*[])
{
    LogReporter reporter(
        "LogReport.txt",
        Logger::Listener::LISTEN_FOR_ALL,
        Logger::Listener::LISTEN_FOR_ALL,
        Logger::Listener::LISTEN_FOR_ALL,
        Logger::Listener::LISTEN_FOR_ALL);

    Window::Parameters parameters;
    parameters.title = L"TriangulationCDTWindow";
    parameters.xOrigin = 0;
    parameters.yOrigin = 0;
    parameters.xSize = 512;
    parameters.ySize = 512;
    parameters.allowResize = false;
    parameters.deviceCreationFlags = D3D11_CREATE_DEVICE_DEBUG;

    TriangulationCDTWindow* window =
        TheWindowSystem.Create<TriangulationCDTWindow>(parameters);

    if (window)
    {
        HWND handle = window->GetHandle();
        ShowWindow(handle, SW_SHOW);
        UpdateWindow(handle);

        for (;;)
        {
            WaitMessage();
            MSG msg;
            if (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
            {
                if (msg.message == WM_QUIT)
                {
                    break;
                }

                TranslateMessage(&msg);
                DispatchMessage(&msg);
            }
        }

        TheWindowSystem.Destroy<TriangulationCDTWindow>(window);
    }

    return 0;
}

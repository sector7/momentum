// Geometric Tools LLC, Redmond WA 98052
// Copyright (c) 1998-2015
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
// File Version: 1.5.0 (2014/10/02)

#pragma once

#include <GteLightingEffect.h>
#include <GteTexture2.h>
using namespace gte;

class DirectionalLightTextureEffect : public LightingEffect
{
public:
    DirectionalLightTextureEffect(LightingConstants const& lighting,
        std::shared_ptr<Texture2> const& texture, SamplerState::Filter filter,
        SamplerState::Mode mode0, SamplerState::Mode mode1);

    std::shared_ptr<Texture2> const& GetTexture() const;
    std::shared_ptr<SamplerState> const& GetSampler() const;

private:
    std::shared_ptr<Texture2> mTexture;
    std::shared_ptr<SamplerState> mSampler;
    static std::string const msHLSLString;
};

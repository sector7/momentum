// Geometric Tools LLC, Redmond WA 98052
// Copyright (c) 1998-2015
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
// File Version: 1.5.0 (2014/10/02)

#pragma once

#include <GTEngine.h>
#include "DirectionalLightTextureEffect.h"
using namespace gte;

class DirectionalLightTextureWindow : public Window
{
public:
    virtual ~DirectionalLightTextureWindow();
    DirectionalLightTextureWindow(Parameters& parameters);

    virtual void OnIdle();

private:
    bool SetEnvironment();
    bool CreateTerrain();
    void UpdateConstants();

    Environment mEnvironment;
    Vector4<float> mTextColor;
    std::shared_ptr<DirectionalLightTextureEffect> mEffect;
    Vector4<float> mLightWorldDirection;
    std::shared_ptr<Visual> mTerrain;
};

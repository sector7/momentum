// Geometric Tools LLC, Redmond WA 98052
// Copyright (c) 1998-2015
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
// File Version: 1.5.1 (2014/12/02)

#include "DirectionalLightTextureWindow.h"

//----------------------------------------------------------------------------
DirectionalLightTextureWindow::~DirectionalLightTextureWindow()
{
}
//----------------------------------------------------------------------------
DirectionalLightTextureWindow::DirectionalLightTextureWindow(Parameters& parameters)
    :
    Window(parameters),
    mTextColor({ 0.0f, 0.0f, 0.0f, 1.0f })
{
    if (!SetEnvironment() || !CreateTerrain())
    {
        parameters.created = false;
        return;
    }

    mEngine->SetClearColor(
        Vector4<float>{ 134.0f, 189.0f, 212.0f, 255.0f } / 255.0f);

    mCamera.SetFrustum(60.0f, GetAspectRatio(), 0.01f, 100.0f);
    Vector4<float> camPosition{ 0.0f, -7.0f, 1.5f, 1.0f };
    Vector4<float> camDVector{ 0.0f, 1.0f, 0.0f, 0.0f };
    Vector4<float> camUVector{ 0.0f, 0.0f, 1.0f, 0.0f };
    Vector4<float> camRVector = Cross(camDVector, camUVector);
    mCamera.SetFrame(camPosition, camDVector, camUVector, camRVector);

    EnableCameraMotion(0.005f, 0.002f, 2.0f, 2.0f);
    EnableObjectMotion();
    UpdateCW();
}
//----------------------------------------------------------------------------
void DirectionalLightTextureWindow::OnIdle()
{
    MeasureTime();
    MoveCamera();
    UpdateConstants();

    mEngine->ClearBuffers();
    mEngine->Draw(mTerrain);
    DrawFrameRate(8, mYSize-8, mTextColor);
    mEngine->DisplayColorBuffer(0);

    UpdateFrameCount();
}
//----------------------------------------------------------------------------
bool DirectionalLightTextureWindow::SetEnvironment()
{
    std::string path = mEnvironment.GetVariable("GTE_PATH");
    if (path == "")
    {
        LogError("You must create the environment variable GTE_PATH.");
        return false;
    }
    mEnvironment.Insert(path + "/Samples/Data/");

    if (mEnvironment.GetPath("BTHeightField.bmp") == "")
    {
        LogError("Cannot find file BTHeightField.bmp.");
        return false;
    }

    if (mEnvironment.GetPath("BTStone.bmp") == "")
    {
        LogError("Cannot find file BTStone.bmp.");
        return false;
    }

    return true;
}
//----------------------------------------------------------------------------
bool DirectionalLightTextureWindow::CreateTerrain()
{
    // Create the visual effect.  The world up-direction is (0,0,1).  Choose
    // the light to point down.
    LightingConstants lighting;
    lighting.materialAmbient = { 0.5f, 0.5f, 0.5f, 1.0f };
    lighting.materialDiffuse = { 0.5f, 0.5f, 0.5f, 1.0f };
    lighting.materialSpecular = { 1.0f, 1.0f, 1.0f, 75.0f };
    lighting.lightAmbient = mEngine->GetClearColor();
    lighting.lightAttenuation = { 1.0f, 0.0f, 0.0f, 1.0f };
    mLightWorldDirection = { 0.0f, 0.0f, -1.0f, 0.0f };

    std::string stoneFile = mEnvironment.GetPath("BTStone.bmp");
    std::shared_ptr<Texture2> stoneTexture(WICFileIO::Load(stoneFile, true));
    stoneTexture->AutogenerateMipmaps();

    mEffect.reset(new DirectionalLightTextureEffect(lighting, stoneTexture,
        SamplerState::MIN_L_MAG_L_MIP_L, SamplerState::CLAMP,
        SamplerState::CLAMP));

    // Create the height field for terrain using heights from a gray-scale
    // bitmap image.
    struct Vertex
    {
        Vector3<float> position, normal;
        Vector2<float> tcoord;
    };
    VertexFormat vformat;
    vformat.Bind(VA_POSITION, DF_R32G32B32_FLOAT, 0);
    vformat.Bind(VA_NORMAL, DF_R32G32B32_FLOAT, 0);
    vformat.Bind(VA_TEXCOORD, DF_R32G32_FLOAT, 0);

    std::string heightFile = mEnvironment.GetPath("BTHeightField.bmp");
    Texture2* heightTexture = WICFileIO::Load(heightFile, false);
    MeshFactory mf;  // Fills in Vertex.position, Vertex.tcoord.
    mf.SetVertexFormat(vformat);
    float extent = 8.0f;
    mTerrain = mf.CreateRectangle(heightTexture->GetWidth(),
        heightTexture->GetHeight(), extent, extent);

    // The mesh factory creates a flat height field.  Use the height-field
    // image to generate the heights and use a random number generator to
    // perturb them, just to add some noise.
    std::mt19937 mte;
    std::uniform_real_distribution<float> rnd(-1.0f, 1.0f);
    std::shared_ptr<VertexBuffer> vbuffer = mTerrain->GetVertexBuffer();
    unsigned int numVertices = vbuffer->GetNumElements();
    Vertex* vertex = vbuffer->Get<Vertex>();
    unsigned char* heights = heightTexture->Get<unsigned char>();
    for (unsigned int i = 0; i < numVertices; ++i)
    {
        float height = static_cast<float>(heights[4 * i]) / 255.0f;
        float perturb = 0.05f * rnd(mte);
        vertex[i].position[2] = 3.0f * height + perturb;
    }
    delete heightTexture;

    mTerrain->SetEffect(mEffect);
    mTerrain->Update();
    mTerrain->UpdateModelNormals();  // Fill in Vertex.normal.
    SubscribeCW(mTerrain, mEffect->GetPVWMatrixConstant());
    return true;
}
//----------------------------------------------------------------------------
void DirectionalLightTextureWindow::UpdateConstants()
{
    Matrix4x4<float> invWMatrix = Inverse(mTerrain->worldTransform);
    Vector4<float> cameraWorldPosition = mCamera.GetPosition();
    LightingConstants& param = mEffect->GetLighting();
#if defined(GTE_USE_MAT_VEC)
    param.cameraModelPosition = invWMatrix * cameraWorldPosition;
    param.lightModelDirection = invWMatrix * mLightWorldDirection;
#else
    param.cameraModelPosition = cameraWorldPosition * invWMatrix;
    param.lightModelDirection = mLightWorldDirection * invWMatrix;
#endif
    mEngine->Update(mEffect->GetLightingConstant());
    UpdateCW();
}
//----------------------------------------------------------------------------

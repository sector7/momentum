// Geometric Tools LLC, Redmond WA 98052
// Copyright (c) 1998-2015
// Distributed under the Boost Software License, Version 1.0.
// http://www.boost.org/LICENSE_1_0.txt
// http://www.geometrictools.com/License/Boost/LICENSE_1_0.txt
// File Version: 1.0.2 (2014/10/06)

#pragma once

#include <GTEngine.h>
using namespace gte;

class MultipleRenderTargetsWindow : public Window
{
public:
    virtual ~MultipleRenderTargetsWindow();
    MultipleRenderTargetsWindow(Parameters& parameters);

    virtual void OnIdle();
    virtual bool OnCharPress(unsigned char key, int x, int y);

private:
    bool SetEnvironment();
    bool CreateScene();
    bool CreateOverlays();

    Vector4<float> mTextColor;
    Environment mEnvironment;
    std::shared_ptr<DrawTarget> mDrawTarget;
    std::shared_ptr<Visual> mSquare;
    std::shared_ptr<Texture2> mLinearDepth;
    std::shared_ptr<OverlayEffect> mOverlay[7];
    unsigned int mActiveOverlay;

    static std::string const msOverlayPShader[5];
};

# ------------------------------------------------- #
# Fachhochschule Wedel - SS2015                     #
# Projekt Fotorealismus und Simulation              #
#                                                   #
# Ellen Schwartau (M_Inf 101493)                    #
# Malte Jörgens   (M_Inf 101519)                    #
# Timo Gröger     (M_Inf 101504)                    #
#                                                   #
# ------------------------------------------------- #

# Compiler
CC			= gcc

# Linker
LD			= g++

# Target name
BINARY=momentum

# Include directories
INCLUDES	=	-Isrc/ \
				-Isrc/common/ \
				-Isrc/config/ \
				-Isrc/io/ \
				-Isrc/logic/ \
				-Isrc/logic/photon/ \
				-Isrc/logic/raytracer/ \
				-Isrc/scene/ \
				-Isrc/scene/light/ \
				-Isrc/scene/objects/ \
				-Isrc/scene/render/ \
				-Isrc/scene/opengl/ \
				-Isrc/utils/ \
				-Ilib-src/GTEngine/Include/ \
				-Ilib-src/pngwriter/src/ \
				-I/usr/X11R6/include/ \
				-I/usr/include/freetype2/
				
# Compiler flags
CFLAGS_COMMON   = -c -g -std=c++11 -Wall -Wextra -Wno-unused-parameter -pthread
CFLAGS		= $(CFLAGS_COMMON)
# Linker flags
LDFLAGS		=
# Linker libraries
LDLIBS		= -pthread -lm -lGL -lGLU -lglut -lpngwriter -lfreetype -lpng -lgtengine -Llib/ -L/usr/X11R6/lib64 -L/usr/lib/nvidia-304/ -lz


# Source codes
SOURCE=$(wildcard src/*.cpp src/*/*.cpp src/*/*/*.cpp src/*/*/*/*.cpp)
OBJECTS=$(SOURCE:.cpp=.o)

.PHONY: all help clean lib cleanlib cleanall debug profile init

default: all

all: $(BINARY)

cleanall: clean cleanlib

init:
	@mkdir -p out/

# Compile a single file
%.o : %.cpp
	@echo "$< übersetzen..."
	@$(CC) $(CFLAGS) $(INCLUDES) -o $@ $<
	@echo "Fertig mit $<"

# Link files to an executable
$(BINARY): init lib $(OBJECTS)
	@echo -n "$@ zusammenbauen...\t"
	@$(LD) $(LDFLAGS) $(OBJECTS) $(LDLIBS) -o $(BINARY)
	@echo "fertig, keine Fehler! :D"

# Clean the project
clean:
	@echo
	@echo -n "Aufräumen...\t\t"
	@rm -f $(OBJECTS) $(BINARY) *~ types gmon.out
	@echo "fertig!"

# Create the necessary libs
lib:
	@echo "Erstelle benötigte Libraries"
	@make -j -C lib-src/

# Clean the libraries
cleanlib:
	@echo "Bereinige Libraries"
	@make -C lib-src/ clean
	@rm -rf lib/
	
# Show help
help:
	@echo "Optionen:"
	@echo "make [all]    - Programm erstellen"
	@echo "make clean    - Compilate aufräumen"
	@echo "make debug    - Programm für DEBUG erstellen"
	@echo "make profile  - Programm für PROFILING erstellen"

debug: CFLAGS = $(CFLAGS_COMMON) -DDEBUG
debug: LDFLAGS =
debug: $(BINARY)

profile: CFLAGS = $(CFLAGS_COMMON) -pg
profile: LDFLAGS = -pg
profile: clean $(BINARY)
	

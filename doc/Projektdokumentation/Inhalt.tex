\section{Projektvorhaben}
Es wurde ein C++-Programm geschrieben, das eine dreidimensionale Szene mithilfe des Raytracing-Verfahrens darstellt. Zur Annäherung an ein globales Beleuchtungsmodell wurden Photon-Maps in Kombination mit direkter Beleuchtung verwendet. In der Szene befinden sich Objekte mit komplexen Materialeigenschaften, die das simulierte Licht realistisch reflektieren, brechen und absorbieren können sollen. So entstehen in der Szene beispielsweise Lichtbüschel ("`Kaustiken"'), die auf den diffus reflektierenden Objekten sichtbar werden.

Die Szene soll -- ähnlich einer Cornell-Box -- verschiedenartige Objektprimitive zeigen und diese aus mehreren (frei wählbaren) Kameraperspektiven rendern können, sodass möglichst viele der genannten Lichteffekte anschaulich abgebildet werden.

\section{Ergebnispräsentation}
\autoref{fig:endergebnis} zeigt die Totalaufnahme der implementierten Szene. Die folgenden Abschnitte gehen auf die visuellen Effekte ein. 

\begin{figure}[h]
\centering
\includegraphics[width=0.95\linewidth]{./img/DerbeTotale}
\caption{Gesamtansicht der erstellten und gerenderten Szene}
\label{fig:endergebnis}
\end{figure}


\subsection{Kaustiken}
Die Kaustiken entstehen durch die Bündelung von Photonen, die durch transmittierende Materialien propagiert werden. Im Beispiel auf \autoref{fig:KaustikenVergleich} wurden die Photonen in der Vorschauansicht links dargestellt und sind im gerenderten Bild in der Mitte als Kaustik sichtbar. Rechts wurde das vollständige Bild durch Summierung direkter und indirekter Beleuchtung berechnet.

\begin{figure}[h]
\centering
\begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=1.0\linewidth]{./img/Kaustik/OpenGL}
    \caption{Vorschauansicht gebündelter Photonen}
    \label{fig:KausikenVorschau}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=1.0\linewidth]{./img/Kaustik/nurDirekt}
    \captionsetup{width=0.8\textwidth}
    \caption{Direkte Beleuchtung durch Raytracer}
    \label{fig:KaustikenDirekteBeleuchtung}
\end{subfigure}

\vspace{0.5cm}
\begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=1.0\linewidth]{./img/Kaustik/nurIndirekt}
    \captionsetup{width=0.8\textwidth}
    \caption{Ausschließlich indirekte Beleuchtung}
    \label{fig:KaustikIndirekt}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=1.0\linewidth]{./img/Kaustik/nurKaustiken}
    \captionsetup{width=0.65\textwidth}
    \caption{Kaustik durch Lichtbündelung}
    \label{fig:NurKaustik}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=1.0\linewidth]{./img/Kaustik/kombiniert}
    \captionsetup{width=0.8\textwidth}
    \caption{Vollständig gerendertes Bild mit Kaustik}
    \label{fig:KaustikKomplett}
\end{subfigure}
\caption{Entstehung von Kaustiken}
\label{fig:KaustikenVergleich}
\end{figure}

Zur Verdeutlichung der Lichtbrechung an gläsernen Objekten (80\% transmittierend, 20\% reflektierend) wurde auf \autoref{fig:krasseSache} ein großer Glaswürfel erstellt und gerendert.

\begin{figure}[h]
\centering
\includegraphics[width=0.33\linewidth]{./img/krasseSache}
\caption{Lichtbündelung und -brechung innerhalb eines Glaswürfels}
\label{fig:krasseSache}
\end{figure}


\subsection{Reflexion}

\begin{wrapfigure}{r}{6cm}
\includegraphics[width=1\linewidth]{./img/spekulareReflextionstypen}
\caption{Direkte und gerichtete spekulare Reflexion.}
\label{fig:spekulareTypen}
\end{wrapfigure}
Die Reflexionen sind als total spekulare Reflexionen implementiert; jeder Strahl wird nach dem Prinzip $Einfallswinkel = Ausfallswinkel$ reflektiert. Ein alternativer Ansatz wäre die Implementierung von Spiegeligkeit, die einen Bereich definiert, innerhalb dessen sich spekular reflektierte Strahlen verteilen. \autoref{fig:spekulareTypen} rechts zeigt beide Prinzipien und \autoref{fig:spekulareReflexion} unten die Spiegeligkeit der gelben und roten Kugel im Renderergebnis.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{./img/spekulareBilder}
\caption{Spiegeligkeit einer total reflektierenden Kugel}
\label{fig:spekulareReflexion}
\end{figure}

Das spiegelnde Material kann keine Photonen aufnehmen und reflektiert diese daher vollständig. Wenn der Raytracer bei der Bildberechnung auf eine spiegelnde Fläche trifft, wird der Strahl solange rekursiv verschossen, bis eine diffuse Fläche getroffen wird. Für die Demonstration einer doppelten Spiegelung wurde die rote Kugel an dieser Stelle ebenfalls zur Spiegelkugel definiert.

\subsection{Farbausblutung}
Die Farbausblutung entsteht an diffusen Objekten, an denen das Licht streuend reflektiert wird. In unserem Modell wird ein zufälliger Winkel über die Hemisphäre bestimmt und die Farbe des Objektes als Farbe des Photons übernommen. An diffusen Flächen um das Objekt herum ist ein Farbschimmer in der Objektfarbe sichtbar. Dieser Effekt lässt sich auf \autoref{fig:farbausblutung} deutlich erkennen.

\newpage

\begin{figure}[h]
\centering
\begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=1.0\linewidth]{./img/Ausbluntung_nurIndirekt_25kP_1-5r}
    \caption{Orangene Farbausblutung}
    \label{fig:farbausblutung1}
\end{subfigure}
~
\begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=1.0\linewidth]{./img/Farbausblutung}
    \caption{Blaue Farbausblutung}
    \label{fig:farbausblutung2}
\end{subfigure}
~
\caption{Farbausblutung durch Speicherung diffus reflektierter Photonen (indirekte Beleuchtung)}
\label{fig:farbausblutung}
\end{figure}

Das Rauschen des grünen Dominosteins links entsteht, da das Bild zur Demonstration der Farbausblutung ohne Multisampling gerendert wurde. Rechts ist die Farbausblutung gut sichtbar, weil ein geringer Suchradius verwendet wurde. Im Gegenzug steigt die Fleckigkeit des Ergebnisses.

\autoref{fig:diffusePhotonmap} zeigt die Visualisierung der diffusen Photonmap. Sie verdeutlicht die Ansammlung blauer Photonen um den Sockel des Würfels als Ursache der Farbausblutung.

\begin{figure}[h]
\centering
\includegraphics[width=0.3\linewidth]{./img/VisualisierungIndirektePhotonen}
\caption{Diffus reflektierte Photonen}
\label{fig:diffusePhotonmap}
\end{figure}


\subsection{Fleckigkeit}\label{sub:Fleckigkeit}
Die Fleckigkeit wird durch die Anpassung des Suchradius, die Anzahl der propagierten Photonen und die Intensitätsabschwächung innerhalb des Suchradius beeinflusst. Mit der Größe des Suchradius sinkt die Fleckigkeit und der Grad der Detaillierung, dafür steigt die Gesamthelligkeit des Renderergebnisses. \autoref{fig:FleckigkeitImBild} zeigt drei verschiedene Einstellungen.

\begin{figure}[h]
\centering
\begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=1.0\linewidth]{./img/Fleckigkeit/1_3.png}
    \caption{Suchradius 3.0, lineare Abschwächung \\}
    \label{fig:Fleckigkeit1}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=1.0\linewidth]{./img/Fleckigkeit/2_gauss2.png}
    \caption{Suchradius 2.0, Gauss-Abschwächung \\}
    \label{fig:Fleckigkeit2}
\end{subfigure}
~
\begin{subfigure}[b]{0.3\linewidth}
    \includegraphics[width=1.0\linewidth]{./img/Fleckigkeit/3_100W2.png}
    \caption{Suchradius 2.0, Gauss-Abschwächung, Reduktion der Lichtstärke auf $\frac{1}{3}$}
    \label{fig:Fleckigkeit3}
\end{subfigure}
\caption{Variation der Einstellungen zur Behebung der Fleckigkeit}
\label{fig:FleckigkeitImBild}
\end{figure}

Die lineare Abschwächung produziert grundsätzlich fleckigere Bilder als die Abschwächung nach Gauss. Durch eine Reduktion der Lichtenergie im dritten Bild konnte die Fleckigkeit fast vollständig eleminiert werden. Die Qualität der Beleuchtung kann auch durch die Erhöhung der Photonenanzahl gesteigert werden, dafür steigt die Renderzeit stark an. Durch Erhöhung des Suchradius für die Suche der nächstgelegenen Photonen und die Anzahl dieser werden Farbausblutungen weniger deutlich.

\subsection{Verdunklung an Kanten}\label{sub:Verdunklung an Kanten}
Die Kaustik wird in der Nähe der unteren Würfelkanten abgeschnitten, wie auf \autoref{fig:falscheKaustikdunkleRaumecken} erkennbar ist.

\begin{figure}[h]
\centering
\begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=1.0\linewidth]{./img/PhotonenMitFalscherNormaleVerwerfen}
    \caption{Abgeschnittene Kaustik und geringe Beleuchtung der Raumecken}
    \label{fig:falscheKaustikdunkleRaumecken}
\end{subfigure}
~
\begin{subfigure}[b]{0.4\linewidth}
    \includegraphics[width=1.0\linewidth]{./img/PhotonenMitFalschenNormalenBehalten}
    \caption{Behebung beider Probleme durch Deaktivieren einer Ausschlussroutine}
    \label{fig:falscheKaustikdunkleRaumeckenBehoben}
\end{subfigure}
\caption{Einfluss einer Ausschlussroutine für Photonen auf falschen Flächen}
\label{fig:behebungKaustikRaumecken}
\end{figure}

Die Ursache war eine zuvor implementierte Logik zum Ausschließen irrelevanter Photonen, die auf der falschen Seite eines Objektes liegen. Ein sehr dünnes, opakes, nur von einer Seite beleuchtetes Objekt würde auf der anderen Seite hell erscheinen, da Photonen auf Flächen mit einer anderen Normale vom kugelförmigen Suchradius ebenfalls gefunden werden. Der Ausschluss von Photonen auf Flächen mit anderen Normalen hätte dieses Problem behoben. \autoref{fig:nebeneffekteSpharischerSuchradius} verdeutlicht das Problem links und die zunächst implementierte Lösung rechts.

\begin{figure}[h]
\centering
\begin{subfigure}[b]{0.2\linewidth}
    \includegraphics[width=1.0\linewidth]{./img/spharischerSuchradius}
    \caption{Einbeziehen überzähliger Photonen durch sphärischen Suchradius}
    \label{fig:suchradiusZuVielePhotonen}
\end{subfigure}
\hspace{1cm}
\begin{subfigure}[b]{0.2\linewidth}
    \includegraphics[width=1.0\linewidth]{./img/spharischerSuchradiusVerkleinert}
    \caption{Eingrenzung der Photonen durch Einbeziehung der Flächennormale}
    \label{fig:suchradiusZuVielePhotonenKorrigiert}
\end{subfigure}
\caption{Nebeneffekte bei der Nutzung eines sphärischen Suchradius}
\label{fig:nebeneffekteSpharischerSuchradius}
\end{figure}

Da die realisierte Szene keine dünnen Objekte enthält, konnte die Routine deaktiviert werden, sodass sowohl die fließende Kaustik als auch die Beleuchtung der Raumecken realistischer wirken. Um das Problem physikalisch korrekt zu lösen, müssten $r$ weiterere Rekursionsschritte beim Rendern eingeführt werden: Ein Strahl, der eine diffuse Fläche trifft, liest nicht direkt die Farbe aus, sondern sendet $n$ zufällige Strahlen in seiner Hemisphäre aus.  um über die darüber gefundenen Farbwerte zu mitteln. 

\newpage

\section{Vorbereitung und Installation}
Zum Kompilieren und Start des Programms unter Linux werden folgende Pakete vorausgesetzt
\begin{compactitem}
\item gcc
\item make
\item cmake
\item libfreetype6-dev
\end{compactitem}


\subsection{Programmstart}
Das Programm kann in der folgenden Weise gestartet werden. Die hier angegebenen Parameter entsprechen den Standardeinstellungen, wenn das Programm ohne weitere Parameter aufgerufen wird. Die Erklärung der optionalen Parameter als Schlüssel-Wert-Paare befindet sich darunter.

\cd{./momentum a 1 r 1 p 25000 w 800 h 600 c 0}

\begin{center}
\setlength{\extrarowheight}{4pt}
\begin{tabular}{ll}
\toprule Parameter & Funktion \\ 
a & Antialiasing-Faktor (1-$n$) \\
c & Kameraeinstellung zur Betrachtung folgender Szenenausschnitte: \\
& (0) komplette Szene, (1) Kugeln, (2) Spiegelnde Kugel, \\
& (3) Farbausblutung, (4) auf Würfel fallende Kaustik, \\
 &  (5) Kaustik auf dem Boden durch eine gläserne Kugel\\
h & Fensterhöhe \\
p & Anzahl der Photonen pro Lichtquelle \\
r & Multisampling (1-$n$) \\
w & Fensterbreite \\
\bottomrule
\end{tabular}
\end{center}

\newpage

\subsection{Tastenbelegung}
Die Kamera kann mit den Tasten W, A, S, und D frei durch die Szene bewegt werden. Mithilfe der Maus ist bei gedrückter linker Maustaste ein Umschauen möglich. Außerdem können die Photon-Maps und eine Gitternetzdarstellung separat eingeblendet werden. Die Tastenbelegung ist in der folgenden Tabelle aufgelistet.

\begin{center}
\setlength{\extrarowheight}{4pt}
\begin{tabular}{ll}
Funktion & Taste \\ 
\toprule
Wireframe-Modus & \keys{F1} \\
Aktuelle Ansicht rendern & \keys{F2} \\ 
Einblenden des Koordinatensystems & \keys{F3} \\ 
Einblenden propagierter Photonen & \keys{F4} \\ 
Umschaltung Vollbild & \keys{F11} \\
Einblenden bestimmter Photonmaps & \\
\hspace{1cm} direkte Photonen	& \keys{1} \\
\hspace{1cm} indirekte Photonen	& \keys{2}  \\
\hspace{1cm} spekulare Photonen & \keys{3} \\
Umschalten der Kameraperspektive & \keys{C} \\
Ausgeben der Hilfe & \keys{H} \\
Ein- und Ausschalten der direkten Beleuchtung & \keys{L} \\
Beenden des Programms & \keys{Q}, \keys{\esc} \\
Bewegung durch die Szene über & \keys{W}, \keys{A}, \keys{S}, \keys{D} \\
\bottomrule
\end{tabular}
\end{center}

\subsection{Programmnutzung}
Beim Programmstart werden alle Photonen propagiert, der Fortschritt wird dabei in der Konsole ausgegeben. Das Programm startet anschließend in der OpenGL-Ansicht der Szene.

Die Kamera ist frei beweglich oder mit \keys{C} auf eine interessante Perspektive fixierbar. Wird der Renderprozess mittels \keys{F2} gestartet, wird die aktuelle Ansicht mit Photon Maps gerendert. Werden eine Kameraeinstellungen oder die anzuzeigenden Photon Maps geändert, wird automatisch auf die OpenGL-Ansicht zurückgewechselt, woraufhin das Bild neu gerendert werden kann.

\section{Szene}
Zur Repräsentation einer kompletten Szene wurde ein Szenenobjekt implementiert. Es beinhaltet Listen mit allen Objekten (\cd{SceneObjectList}), Lichtquellen (\cd{LightList}) und die zur Szene gehörigen Photon Maps. Durch diese Implementiertung ist es möglich, die gesamte Szene und ihre Eigenschaften konsistent für den Entfurfs- und Raytracing-Modus zu verwenden.


\subsection{Szenenobjekt}
Die physikalischen Objekte der Szene sind konkrete Instanzen eines abstrakten Szenenobjektes. Das abstrakte Szenenobjekt implementiert die allgemeinen Eigenschaftsfelder Objekttyp und Objektmaterial.
\begin{samepage}
Weiterhin werden abstrakte Objektmethoden für

\begin{compactitem}
\item[-] Schnitttest mit einem Strahl
\item[-] Abstand von Strahl zum dichtesten Schnittpunkt mit Objekt
\item[-] Genauen Schnittpunkt eines Strahls im Objekt
\item[-] Normale an bestimmten Punkt
\item[-] Zeichnung der jeweiligen OpenGL-Repräsentation
\end{compactitem}

implementiert.
\end{samepage}

Diese Methoden sind in konkreten Szenenobjekten implementiert und unterscheiden sich aufgrund der Objekttypen (Kugel oder Quader) voneinander. Die Abstraktion ist für die Propagierung und Interaktion der Photonen sowie der Berechnung der Strahlen des Raytracers notwendig.

\subsection{Szenenmanager}
Der statisch und damit einmalig instantiierte Szenenmanager ist die Anlaufstelle für sämtliche Interaktion mit der dargestellten Szene. Einmalig wird die Szene als ebenfalls statisches Objekt initialisiert. Der Szenenmanager erstellt jede Entität der Szene (Kugel, Licht, ...) mit seinen spezifischen Parametern und speichert diese in der Szene. Anschließend startet er die Propagierung der Photonen und speichert die entstandenen Photon-Maps ebenfalls in der Szene. 

Fortan ist der Szenenmanager mit \cd{Scene\& getScene();} in der Lage, einen Zeiger auf die Szene bereitzustellen.

\subsection{Objektmaterial}
Zur Definition der optischen Eigenschaften der sichtbaren Objekte wurde der Typ \cd{Material} definiert. Er enthält alle für die Darstellung relevanten Eigenschaften. Dazu zählen vier Farbeigenschaften für die Darstellung in OpenGL (ambient, diffus, spekular, shininess), drei Zufallswerte zur Bestimmung der Ereignis-Gewichtung im Russian-Roulette und dem Brechungsfaktor, der die Winkeländerung bei der Transmission und spekularen Reflexion bestimmt.
%
%\section{IO}
%Das Programm ist interaktiv bedienbar. Nachdem die Propagierung der Photonen abgeschlossen ist, startet das Programm in eine OpenGL-Repräsentation der Szene, innerhalb derer die Kamera frei beweglich ist. 
%
%Für die Bewegung innerhalb der Szene wurde eine einfache Kamera implementiert, die mit Hilfe der Tastatur und Maus gesteuert werden kann. Die Kamera als Objekt trägt die Eigenschaften ihrer Position \cd{pos}, ihre Blickrichtung \cd{dir}, sowie die Drehwinkel \cd{theta} und \cd{phi}. Bei jedem Zeichnen der Szene werden diese Parameter mittels \cd{gluLookAt()} genutzt.
%
%Die Bewegung durch den Raum mit W, A, S, D wird durch wiederholte Vektoraddition auf den Positionsvektor und den Blickrichtungs-Vektor realisiert. Die Seitwärtsbewegung nutzt das Skalarprodukt beider Vektoren.
%
%Das Umsehen mit der Maus basiert auf der Verschiebung des Blickrichtungs-Vektors auf einer Kugeloberfläche. Die Kamera befindet sich innerhalb einer gedachten Kugel.

\section{Logik}
Die Simulation der fotorealistischen Szene erfolgt durch die Kombination direkter und indirekter Beleuchtung.

\subsection{Direkte Beleuchtung und Raytracing}
Beim Raytracing-Verfahren wird zunächst eine virtuelle Kamera in der Szene positioniert, die eine imaginäre Projektionsfläche aufspannt. Diese Projektionsfläche ist ein Ebenenausschnitt in der dreidimensionalen Szene, der sich aus der Kameraposition, Blickrichtung, Öffnungswinkel, Seitenverhältnis und dem Abstand zur Kamera errechnen lässt. \autoref{fig:RaytracerPrinzip} zeigt das Prinzip in vereinfachter Weise.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{./img/RaytracerPrinzip}
\caption{Grundsätzliches Prinzip eines Raytracers}
\label{fig:RaytracerPrinzip}
\end{figure}

Der Framebuffer, in den das zweidimensionale Bild der Szene gespeichert werden soll, wird auf diese Projektionsfläche abgebildet, indem für jede Pixelposition ein Strahl in die Szene ausgehend vom Augpunkt durch die entsprechende Stelle der Projektionsebene geschossen wird. Anschließend erfolgt ein Schnitttest mit sämtlichen Szenenobjekten, um das Objekt mit dem geringsten Abstand zur Kamera zu bestimmen. Abhängig von den Objekteigenschaften wird die Farbe bestimmt, die in den Framebuffer geschrieben werden soll. Bei transmittierenden Objekten, wie beispielsweise Glas, kann der Strahl gemäß physikalischer Gesetzmäßigkeiten gebrochen oder reflektiert werden, was einen rekursiven Abstieg des Strahls in die Szene zur Folge hat, bis ein diffus reflektierendes Objekt getroffen wird, das die Farbe festlegt. Zusätzlich kann an diesen Objektpunkten ein Schattenstrahl erstellt werden, mit dem sich testen lässt, ob sich zwischen Objekt am Schnittpunkt und der Lichtquelle ein verdeckendes Objekt befindet, sodass der auf dem Framebuffer gezeichnete Punkt weniger stark beleuchtet wird.

Die Intensität der Beleuchtung wird außerdem durch den steigenden Winkel zwischen Schattenstrahl und Oberflächennormale sowie die quadratische Entfernung zur Lichtquelle abgeschwächt. Ist die Lichtquelle weiter vom Objekt entfernt oder steht in einem sehr flachen Winkel zur Oberfläche, erscheint das Objekt dunkler. 

Da die Berechnung der einzelnen Bildpixel unabhängig voneinander erfolgt, kann das Raytracing-Verfahren problemlos parallelisiert werden.

\subsection{Photon Mapping}
Zur Simulation eines indirekten Beleuchtungsmodels wird das Photon Mapping verwendet. 

Dabei werden ausgehend von den Lichtquellen der Szene Photonen verschossen und durch die Szene verfolgt, wobei die Anzahl an Photonen pro Lichtquelle konstant ist. Die Intensität jedes Photons entspricht dabei dem Anteil an der Leuchtkraft der jeweiligen Lichtquelle. \autoref{fig:PhotonPropagierung} zeigt beispielhaft die Propagierung dreier Photonen.

\begin{figure}[h]
\centering
\includegraphics[width=0.5\linewidth]{./img/photonMap}
\caption{Propagierung der Photonen ausgehend von einer Punktlichtquelle.}
\label{fig:PhotonPropagierung}
\end{figure}

Die Wegfindung der Photonen geschieht auf Grundlage des Russian Roulette. Eine Speicherung der propagierten Photonen wird bei einer diffusen Reflexion vorgenommen. Diese werden zunächst in einer linearen Liste gesammelt, aus der nach der Berechnung aller Photonen und ihrer Reflexionen ein KD-Tree erstellt wird.

Die Beleuchtung durch Photonen, die direkt von der Lichtquelle stammen, kann durch das direkte Beleuchtungsmodell abgebildet werden. Letzteres ist performanter, da die Abfragen aus dem KD-Baum entfallen, gleichzeitig nähert sich die Genauigkeit der Beleuchtung durch Photonen erst mit einer unendlichen Photonenanzahl der direkten Beleuchtung an. Insofern wurden die direkten Photonen für das Renderergebnis verworfen.

\subsubsection{KD-Tree}
Ein KD-Tree ist ein $k$-dimensionaler -- hier drei-dimensionaler -- Baum. Die Funktionsweise demonstriert das folgende Beispiel auf \autoref{fig:KDTree}.

\begin{figure}[h]
\centering
\includegraphics[width=0.8\linewidth]{./img/KDTree}
\caption{Prinzip der Speicherung von Photonen in einem KD-Tree}
\label{fig:KDTree}
\end{figure}

Gegeben sei eine Punktemenge im zweidimensionalen Raum. Die Punktemenge wird nun abwechselnd zur x- und y-Achse orthogonale Gerade in zwei gleichgroße Mengen geteilt. Übertragen auf 3D geschieht dann eine Trennung durch Ebenen für x, y, und z.

Dieser Vorgang wird wiederholt, bis maximal ein freier Punkt je Punktesektor vorhanden ist. Da das Hinzufügen eines Punktes zur Punktemenge den Aufbau des KD-Trees stark beeinflussen kann, ist es von Vorteil die Punktemenge vorab zu bestimmen und erst im Anschluss den Baum zu erstellen.

Die so entstandenen Trennungen bilden anschließend die Verzweigungsknoten des binären Baums, dessen Blätter die vorhandenen Punkte repräsentieren. Dabei sind diejenigen Blätter, die im Baum benachbart gespeichert sind, auch im Koordinatensystem räumlich nah beieinander. Diese Eigenschaft kann genutzt werden, um Nearest Neighbour Queries effizient durchzuführen. Dieser Vorteil kann bei der Lichtberechnung der Szene ausgenutzt werden.

\subsubsection{Indirekte Beleuchtung der Szene}
Die indirekte Beleuchtung der Szene sowie die Bestimmung der Farbe eines bestimmten Punktes werden während des Raytracings auf Grundlage der Informationen aus den Photon Maps berechnet.

Dabei wird die Belichtung eines Punktes, der durch den Raytracer getroffen wird, durch die in einem bestimmten Radius umliegenden Photonen bestimmt. \autoref{fig:Raytracing} zeigt beispielhaft die Berechnung für zwei Punkte in der Szene. 

\begin{figure}[h]
\centering
\includegraphics[width=0.4\linewidth]{./img/Raytracing}
\caption{Ermittlung indirekter Helligkeit durch Raytracing und Photonensuche innerhalb eines definierten Suchradius.}
\label{fig:Raytracing}
\end{figure}

Beispielsweise wird der obere Strahl zweifach beim Ein- und Austritt in eine Glaskugel gebrochen und trifft anschließend auf eine diffuse Wand. Die ideal reflektierten Strahlen sind zusätzlich eingezeichnet. Farbe und Helligkeit des Punktes werden dort unter Berücksichtigung der Einschlagrichtung durch die Photonen bestimmt, die sich in dem eingezeichneten Kreis befinden.

\subsubsection{Visualisierung Photon Map}
Die Photonen der Photon Maps können in der OpenGL-Entwurfsdarstellung der Szene visualisiert werden. Diese werden als kurze Linie an der Oberfläche, auf der sie auftreffen, angezeigt. Die Linienrichtung entspricht dabei der Richtung der Herkunft des Photon. Da jedes Photon nur eine sehr geringe Farbinformation mit sich trägt, wird diese für die Anzeige normalisiert.

% Diese werden mit Punktkoordinaten aller Photonen, ihren zugehörigen Farbwerten und einem Vektor für die Einschlagsrichtung gefüllt. Anschließend werden die Vertex-Arrays durch

%\cd{glNormalPointer (GL\_FLOAT, 9 * sizeof(GLfloat), pm->getPhotonNormals();}
%
%an die Grafikkarte übergeben und mit 
%
%\cd{glDrawElements(GL\_POINTS, pm->getTotalPhotonCount(), \\ GL\_UNSIGNED\_INT, pm->getPhotonIndices())}

Mit den Tasten \keys{1} - \keys{3} können die einzelnen Photon Maps ein- und ausgeblendet werden.

\subsection{Vereinfachtes phsyikalisches Modell}
Die Energie eines Photons wurde vereinfacht auf die Lichtfarbe abgebildet und auf die drei Farbkanäle aufgeteilt; deshalb sind Farbe und Intensität im ersten Schritt gleichgesetzt. Jedes Photon übernimmt initial die Farbe (= Energie) der Lichtquelle.

Die Intensität einer Lichtquelle wird anhand ihrer Wattzahl gespeichert. Um die Photonen mit der Intensität auszustatten, werden die einzelnen Farbkanäle mit den Wattzahlen multipliziert. Photonen fliegen durch die Szene und verlieren Farbe bei Interaktion mit farbigen Objekten.
Bei perfekten Spiegeln und perfekten Glaskugeln geht keine Energie verloren. Beim Auslesen der Photonen wird der Farbwert aller gefundenen Photonen aufsummiert und durch die entsprechende Kreisfläche der Suche geteilt.

\subsubsection{Russian Roulette}
Das Propagieren der Strahlen und Photonen geschieht auf Grundlage des Russian Roulettes.

Dabei bestimmt eine Zufallszahl und die vorhandenen Materialeigenschaften, ob ein Photon diffus oder spekular reflektiert, transmittiert oder absorbiert wird, statt alle vier Szenarien zu verfolgen. Die Intensität des Photons bleibt gleich, wodurch auch die so approximierte Gesamtintensität der Szene der Realität angenähert wird. Der folgende Ausschnitt in Pseudo-Code verdeutlicht das Prinzip.

\begin{lstlisting}
traceRay(ray, refractionIndex, color, sceneObjects, SceneObject* self, photonPathSoFar) {
	xi = random([0..1));
	m = self.material;
		if (xi < m.probD) {
			reflectDiffuse();
		} else if (xi < (m.probD + m.probS)) {
			reflectSpecular();
		} else if (xi < (m.probD + m.probS + m.probT)) {
			transmit();
		} else {
			// Absorption, do nothing
		}
	}
}
\end{lstlisting}

%\subsubsection{Darstellung in OpenGL}
%Die Entwurfsdarstellung der Szene wird auf folgende Weise realisiert:
%
%\begin{samepage}
%\begin{compactitem}
%	\item[-] Auslesen der Kameraattribute und setzen der Kamera mittels \cd{gluLookAt(...)}
%	\item[-] Setzen aller Szenenlichter mit jeweiliger Leuchtfarbe
%	\item[-] Iteration über alle Szenenobjekte, dabei
%	\begin{compactitem}
%		\item[-] Setzen der Objektmaterialien
%		\item[-] Setzen der Objektposition mittels affiner Transformationen
%		\item[-] Aufrufen der Zeichenmethode für eine Einheitsrepräsentation des jeweiligen 						Szenenobjektes
%	\end{compactitem}
%\end{compactitem}
%\end{samepage}

\subsubsection{Umschaltung OpenGL / Raytracer}
Durch die zentrale Verwaltung der Szene im SceneManager ist eine konsistente Darstellung der Szene über verschiedene Zeichenmethoden möglich. In diesem Projekt kann zur Laufzeit zwischen der Darstellung der OpenGL- und Raytracer-Ansicht umgeschaltet werden. Diese Funktionalität eignet sich zur schnellen Platzierung der Kamera in der Szene, um anschließend einen zeitintensiven Renderingprozess durch den Raytracer zu initiieren.

\section{Zusammenfassung}
Zur Lichtberechnung in einer virtuellen Szene sind Approximationsverfahren notwendig, da die unendlich wiederholte Reflexion von Lichtstrahlen eine physikalisch korrekte Berechnung des Lichts in endlicher Zeit unmöglich macht.

Das Photon Mapping ist ein Repräsentant eines solchen Verfahrens und ermöglicht es, in virtuellen Szenen Effekte wie Farbausblutung und Kaustiken effizient zu implementieren.

Um allein auf Grundlage des Photon Mappings ein qualitativ hochwertiges Erscheinungsbild der Szene zu erzielen, ist eine Vielzahl an Photonen notwendig. Bei einer Veränderung der Photonenzahl müssen unter Umständen weitere Renderparameter angepasst werden, ansonsten besteht die Gefahr eines fleckigen Erscheinungsbildes der Szene (s. \autoref{sub:Fleckigkeit}) und es existieren negative Seiteneffekte, die bei der Ermittlung der relevanten Photonen entstehen können (s. \autoref{sub:Verdunklung an Kanten}). Um bessere Ergebnisse zu erzielen, wurde die Lichtberechnung mit Hilfe weiterer Verfahren wie dem direkten Beleuchtungsmodell und dem Russian Roulette kombiniert.